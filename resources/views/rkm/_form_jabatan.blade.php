@extends('layouts.iab2')
@section('title', 'Borang Jabatan')
@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div>
    <form action="#" method="POST" enctype="multipart/form-data">
        <div class="mb-3">
            <label class="form-label wajib">Kod Jabatan</label>
            <input type="text" class="form-control"  id="kodjabatan" name="kodjabatan">
        </div>
        <div class="mb-3">
            <label class="form-label wajib">Nama Jabatan</label>
            <input type="text" class="form-control" id="namajabatan" name="namajabatan">
        </div>
        <div class="mb-3">
            <label class="form-label wajib">Kementerian</label>
            <select name="kodKementerian" id="kodKementerian">
                <option value="01">Kementerian Pendidikan</option>
            </select>
        </div>

        <button type="button" onclick="saveJabatan({{$id}})" class="btn btn-primary">Simpan</button>
        <a href="{{route('jabatan.tambah.rkm')}}" class="btn btn-warning">Set Semula</a>
        <a href="{{route('seminar.cari')}}" class="btn btn-danger">Kembali</a>
    </form>
</div>

@endsection

<script>

    function saveJabatan(val){
        // alert('Save Record');

        const xhr = new XMLHttpRequest();
        @if ($id !== '')
        xhr.open("PUT", "/api/jabatans/{{$id}}", true);
        @else
        xhr.open("POST", "/api/jabatans", true);
        @endif
        // xhr.open("POST", "http://projectsaya.test:8083/api/jabatans", true);
        

// Send the proper header information along with the request
xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

xhr.onreadystatechange = () => {
  // Call a function when the state changes.
  if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
    // Request finished. Do processing here.
    location.href="/jabatan/edit/{{$id}}";
  }
};

kodjabatan = document.getElementById('kodjabatan').value;
namajabatan = document.getElementById('namajabatan').value;
kodKementerian = document.getElementById('kodKementerian');
kementerian = kodKementerian.value;
alert(kementerian);

xhr.send("kodjabatan="+kodjabatan+"&namajabatan="+namajabatan+"&kodkementerian="+kementerian);

    }

    //DATA RETRIEVAL - START
    @if ($id !== '')
    const xhttp = new XMLHttpRequest();

    // Define a callback function
    xhttp.onload = function() {
    // Here you can use the Data
    // console.log(typeof this.responseText);
        var data = JSON.parse(this.responseText);
        // console.log(data);
        
        namajabatan = data.data.namajabatan;
        kodjabatan = data.data.kodjabatan;
        
        document.getElementById('kodjabatan').value = kodjabatan;
        document.getElementById('namajabatan').value = namajabatan;
        
    // console.log(namajabatan);
    }

    // Send a request
    xhttp.open("GET", "/api/jabatans/{{$id}}");
    xhttp.send();

    @endif
    //DATA RETRIEVAL - END
</script>
