<?php

namespace App\Http\Controllers;

use App\Models\Agama;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Resources\AgamaCollection;
use App\Http\Resources\AgamaResource;

class AgamaController extends Controller
{
     /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //data dalam bentuk array
        $agama=Agama::all();
        return response()->json(
            //new agamaCollection($agama); //atau
            new AgamaCollection($agama),response::HTTP_OK);

        //var_dump($agama);

          //tukar ke data dalam bentuk json
       return new AgamaCollection($agama);
         
        //nak data kosong(array) ...kalu nak customize guna collection(guna command yg atas)
        //return new agamaResourse($agama);   

    }

    /**
     * Store a newly created resource in storage.
     */
    
     public function store(Request $request)
    {
        $agama=Agama::create($request->only([
            'kodagama','agama','keterangan','editedby'
        ]));
        return new AgamaResource($agama);
    }

    /**
     * Display the specified resource.
     */
    public function show(Agama $agama)
    {
        //
        return new AgamaResource($agama); //$agama panggil data cth 1,2,3
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Agama $agama)
    {
        //jgn letak kalu tak nak update
        $agama->update($request->only([
            'kodagama','agama','keterangan','editedby'
        ]));
        return new AgamaResource($agama);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Agama $agama)
    {
        //delete rekod apa nak 
        $agama->delete();
        //berjaya 
        return response()->json('SUCCESS',Response::HTTP_I_AM_TEAPOT);
        
    }
}
