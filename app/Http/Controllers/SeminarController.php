<?php

namespace App\Http\Controllers;

use App\Models\Seminar;
use App\Models\Reference;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Support\Facades\Storage;

class SeminarController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $req)
    {
        $seminar_name = $req->seminar_name;
        $seminar_cat = $req->seminar_cat;
        
        $mseminar = Seminar::when(!empty($seminar_name),
            function($q) use($seminar_name){
            $q->where('seminar_name','like','%'.$seminar_name.'%');
        })->when(!empty($seminar_cat),
            function($q) use($seminar_cat){
            $q->where('seminar_cat',$seminar_cat);
        });

        if($req->has('cetak')){
            $mseminar = $mseminar->get();
        }else{
            $mseminar = $mseminar->paginate(10);
        }

        $data['mseminar'] = $mseminar;

        $data['category'] = Reference::findByCat('SEMINARCAT');
        
        if($req->has('cetak')){
            
            $data= ['mseminar'=>$data['mseminar']->toArray()];
            $pdf = Pdf::loadView('seminar.senarai_pdf', $data);
            return $pdf->stream();
        }else{
            return view('seminar.senarai',$data);
        }
        
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        // $data['staff_name'] = Auth()->user()->name;
        $data['mseminar'] = new Seminar();
        $data['category'] = Reference::findByCat('SEMINARCAT');
        $data['subcategory'] = Reference::findByCat('SEMINARSUBCAT');
        return view('seminar.borang',$data);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'seminar_name' => 'required',
            'seminar_cat' => 'required',
        ],[
            'seminar_name.required'=>'Sila masukkan Nama Seminar',
            'seminar_cat.required'=>'Sila masukkan Kategori Seminar',
        ]);

        $attachment = $request->seminar_attachment;
        $file_arr = [];
        if($attachment > 0){
            foreach($attachment as $key=>$value){
                $lname = date('YmdHis').rand();
                $ext = $attachment[$key]->extension();
                $filename = $lname.'.'.$ext;
                $oriName = $attachment[$key]->getClientOriginalName();    
                array_push($file_arr,
                ['original_name'=>$oriName,'system_name'=>$filename]);    
                Storage::disk('public')->putFileAs('iab_attachment',$attachment[$key],$filename);
            } 
        }       
        
        $mseminar = new Seminar();
        $mseminar->seminar_name = $request->seminar_name;
        $mseminar->seminar_cat = $request->seminar_cat;
        $mseminar->details = $file_arr;
        $mseminar->save();
        return redirect()->route('seminar.cari');
    }

    /**
     * Display the specified resource.
     */
    public function show(Seminar $seminar)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    // public function edit(Seminar $seminar)
    public function edit($id)
    {
        $mseminar = Seminar::find($id);
        $category = Reference::findByCat('SEMINARCAT');
        $subcategory = Reference::findByCat('SEMINARSUBCAT');
        return view('seminar.borang',compact('mseminar','category','subcategory'));
    }

    /**
     * Update the specified resource in storage.
     */
    // public function update(Request $request, Seminar $seminar)
    public function update(Request $request, $id)
    {
        $mseminar = Seminar::find($id);
        $mseminar->seminar_name = $request->seminar_name;
        $mseminar->seminar_cat = $request->seminar_cat;
        $mseminar->save();
        return redirect()->route('seminar.cari');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        Seminar::find($id)->delete();
        return redirect()->route('seminar.cari');
    }
}
