<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Jabatan>
 */
class JabatanFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
       return[
            'kodjabatan'=>$this->faker->numberBetween(0,1000),
            'namajabatan'=>$this->faker->text(maxNbChars:50),
            'kodkementerian'=>$this->faker->numberBetween(0,1000),
            // 'editedby'=>$this->faker->text(maxNbChars:50),
        ];

        
    }


    
}
