@extends('layouts.iab')
@section('title', 'Senarai Seminar')
@section('content')
<form action="{{route('seminar.cari')}}">
@csrf
    <div class="mb-3">
        <label class="form-label">Nama Seminar</label>
        <input type="text" class="form-control" 
        name="seminar_name" value="{{request()->seminar_name}}">
    </div>    
    <div class="mb-3">
        <label class="form-label">Kategori Seminar</label>
        <select class="form-select" name="seminar_cat">
            @foreach($category as $key=>$value)
            <option value="{{$key}}" 
            {{(request()->seminar_cat == $key ? ' selected ': '')}}>{{$value}}</option>
            @endforeach
        </select>
    </div>
    
    <button type="submit" class="btn btn-primary">Cari</button>
    <a href="{{route('seminar.cari')}}" class="btn btn-warning">Set Semula</a>    
    <a href="{{route('seminar.cari')}}?cetak" class="btn btn-success">Cetak</a> 
    <a href="{{route('seminar.cari')}}?email" class="btn btn-info">Emel</a> 
</form>
<a href="{{route('seminar.daftar')}}" class="btn btn-info">Tambah</a>

<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama Seminar</th>
      <th scope="col">Kategori Seminar</th>
      <th scope="col">Lampiran</th>
      <th scope="col">Tarikh Dicipta</th>
      <th scope="col">Tarikh Kemaskini</th>
      <th scope="col">Tindakan</th>
    </tr>
  </thead>
  <tbody>
        @php 
        $no = $mseminar->firstItem();
        @endphp
       @foreach($mseminar as $item)
       <tr>
        <td>{{$no++}}</td>
        <td>{{$item->seminar_name}}</td>
        <td>{{($item->kategori->descr??'Tiada Maklumat')}}</td>
        <td>
            @if(isset($item->details))
                @foreach($item->details as $m)
                <a href="{{asset('storage/iab_attachment/'.$m['system_name'])}}">
                    {{$m['original_name']}}</a>
                @endforeach
            @endif
        </td>
        <td>{{$item->created_at}}</td>
        <td>{{$item->updated_at}}</td>
        <td width="25%">        
        <form action="{{route('seminar.hapus',$item->id)}}" method="post">
            @csrf
            @method('DELETE')
            <a href="{{route('seminar.edit',$item->id)}}" class="btn btn-success">Sunting</a>
            <input type="submit" 
            onclick="return confirm('Anda pasti untuk menghapuskan rekod ini?')" 
            class="btn btn-danger" value="Hapus">
        </form>
        
        </td>
       </tr>
       @endforeach
  </tbody>
</table>
{{ $mseminar->appends(request()->except('_token'))->links() }}
@endsection
