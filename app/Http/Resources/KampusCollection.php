<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;

class KampusCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @return array<int|string, mixed>
     */
    public function toArray(Request $request): array
    {
        //return parent::toArray($request);
        //return parent::toArray($request);
        return [
            'data'=>$this->collection,  //collection=json refer kat JabatanController
            'version'=>'0.0.1',
            'ownwer'=>'iab',
         ];
    }
}
