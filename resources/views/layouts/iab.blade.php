<!-- resources/views/layouts/app.blade.php -->
 
<html>
    <head>
        <title>IAB Training - @yield('title')</title>
        <link href="{{ asset('/css/app.css') }}" rel="stylesheet" />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
        <base href="https://iab.moe.edu.my/index.php/ms/">
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<meta name="keywords" content="alumni joomla template, university joomla template, joomla page builder">
	<meta name="description" content="Joomla template for alumni and university - JA Alumni built with T3 Framework, fully responsive and integrates JA page Builder">
	<meta name="generator" content="Joomla! - Open Source Content Management">
	<title>Laman Utama - Portal Rasmi Institut Aminuddin Baki</title>
	<link href="/index.php/ms/?format=feed&amp;type=rss" rel="alternate" type="application/rss+xml" title="RSS 2.0">
	<link href="/index.php/ms/?format=feed&amp;type=atom" rel="alternate" type="application/atom+xml" title="Atom 1.0">
	<link href="/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon">
	<link href="https://iab.moe.edu.my/index.php/ms/component/search/?Itemid=101&amp;format=opensearch" rel="search" title="Cari Portal Rasmi Institut Aminuddin Baki" type="application/opensearchdescription+xml">
	<link href="/t3-assets/css/css-8c1a7-92487.css" rel="stylesheet" type="text/css" media="all" attribs="[]">
	<link href="/t3-assets/css/css-4cf7e-92489.css" rel="stylesheet" type="text/css" media="all" attribs="[]">
	<link href="//fonts.googleapis.com/css?family=Frank+Ruhl+Libre:400,500,700|Libre+Franklin:400,500,700" rel="stylesheet" type="text/css">
	<link href="//fonts.googleapis.com/css?family=Roboto:400,500,700,900" rel="stylesheet" type="text/css">
	<link href="/t3-assets/css/css-4fce4-01978.css" rel="stylesheet" type="text/css" media="all" attribs="[]">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
	<link href="/t3-assets/css/css-be9d2-51811.css" rel="stylesheet" type="text/css" media="all" attribs="[]">
	<link href="https://fonts.googleapis.com/css?family=Droid+Sans" rel="stylesheet" type="text/css">
	<link href="/t3-assets/css/css-48f65-41798.css" rel="stylesheet" type="text/css" media="all" attribs="[]">
	<link href="/media/widgetkit/wk-styles-71c4ed45.css" rel="stylesheet" type="text/css" id="wk-styles-css">
	<style type="text/css">
#camera_wrap_451 .camera_pag_ul li img, #camera_wrap_451 .camera_thumbs_cont ul li > img {height:75px;}
#camera_wrap_451 .camera_caption {
	display: block;
	position: absolute;
}
#camera_wrap_451 .camera_caption > div {
	-moz-border-radius: 5px 5px 5px 5px;-webkit-border-radius: 5px 5px 5px 5px;border-radius: 5px 5px 5px 5px;font-family:'Droid Sans';
}
#camera_wrap_451 .camera_caption > div div.camera_caption_title {
	font-size: 1.1em;
}
#camera_wrap_451 .camera_caption > div div.camera_caption_desc {
	font-size: 0.8em;
}

@media screen and (max-width: 480px) {
		#camera_wrap_451 .camera_caption {
			font-size: 0.6em !important;
		}
}
#camera_wrap_407 .camera_pag_ul li img, #camera_wrap_407 .camera_thumbs_cont ul li > img {height:75px;}
#camera_wrap_407 .camera_caption {
	display: block;
	position: absolute;
}
#camera_wrap_407 .camera_caption > div {
	-moz-border-radius: 5px 5px 5px 5px;-webkit-border-radius: 5px 5px 5px 5px;border-radius: 5px 5px 5px 5px;font-family:'Droid Sans';
}
#camera_wrap_407 .camera_caption > div div.camera_caption_title {
	font-size: 1.1em;
}
#camera_wrap_407 .camera_caption > div div.camera_caption_desc {
	font-size: 0.8em;
}

@media screen and (max-width: 480px) {
		#camera_wrap_407 .camera_caption {
			font-size: 0.6em !important;
		}
}
.jj_sl_navigation li a {
			background-color:#33353b;
			text-align:right;
			color:#ffffff !important;
		}
		.jj_sl_navigation .jj_sprite {
			background-image: url(https://iab.moe.edu.my/media/mod_social_slider/icons/sprite-white.png);
		}.jj_sl_navigation { top:150px;
			}.jj_sl_navigation .jj_sl_custom1 a:hover{
			background-color: #000000;
		 }
		 .jj_sl_navigation .jj_sl_custom2 a:hover{
			background-color: #000000;
		 }
		 .jj_sl_navigation .jj_sl_custom3 a:hover{
			background-color: #000000;
		 }
		 .jj_sl_navigation .jj_sl_custom4 a:hover{
			background-color: #000000;
		 }
		 .jj_sl_navigation .jj_sl_custom5 a:hover{
			background-color: #000000;
		 }
				.jj_sl_navigation .jj_sprite_custom.jj_custom1 {
					background-image: url(https://iab.moe.edu.my/media/mod_social_slider/icons/icons8-facebook-old-34.png);
				}
				.jj_sl_navigation .jj_sprite_custom.jj_custom2 {
					background-image: url(https://iab.moe.edu.my/media/mod_social_slider/icons/icons8-twitter-34.png);
				}
				.jj_sl_navigation .jj_sprite_custom.jj_custom3 {
					background-image: url(https://iab.moe.edu.my/media/mod_social_slider/icons/icons8-instagram-34.png);
				}
				.jj_sl_navigation .jj_sprite_custom.jj_custom4 {
					background-image: url(https://iab.moe.edu.my/media/mod_social_slider/icons/icons8-youtube-squared-34.png);
				}
				.jj_sl_navigation .jj_sprite_custom.jj_custom5 {
					background-image: url(https://iab.moe.edu.my/media/mod_social_slider/icons/icon.png);
				}
#camera_wrap_446 .camera_pag_ul li img, #camera_wrap_446 .camera_thumbs_cont ul li > img {height:75px;}
#camera_wrap_446 .camera_caption {
	display: block;
	position: absolute;
}
#camera_wrap_446 .camera_caption > div {
	-moz-border-radius: 5px 5px 5px 5px;-webkit-border-radius: 5px 5px 5px 5px;border-radius: 5px 5px 5px 5px;font-family:'Droid Sans';
}
#camera_wrap_446 .camera_caption > div div.camera_caption_title {
	font-size: 1.1em;
}
#camera_wrap_446 .camera_caption > div div.camera_caption_desc {
	font-size: 0.8em;
}

@media screen and (max-width: 480px) {
		#camera_wrap_446 .camera_caption {
			font-size: 0.6em !important;
		}
}
#camera_wrap_376 .camera_pag_ul li img, #camera_wrap_376 .camera_thumbs_cont ul li > img {height:75px;}
#camera_wrap_376 .camera_caption {
	display: block;
	position: absolute;
}
#camera_wrap_376 .camera_caption > div {
	-moz-border-radius: 5px 5px 5px 5px;-webkit-border-radius: 5px 5px 5px 5px;border-radius: 5px 5px 5px 5px;font-family:'Droid Sans';
}
#camera_wrap_376 .camera_caption > div div.camera_caption_title {
	font-size: 1.1em;
}
#camera_wrap_376 .camera_caption > div div.camera_caption_desc {
	font-size: 0.8em;
}

@media screen and (max-width: 480px) {
		#camera_wrap_376 .camera_caption {
			font-size: 0.6em !important;
		}
}
	</style>
	<script type="text/javascript" async="" src="https://www.googletagmanager.com/gtag/js?id=G-MQ017YP63Q&amp;cx=c&amp;_slc=1"></script><script async="" src="https://www.google-analytics.com/analytics.js"></script><script type="application/json" class="joomla-script-options loaded">{"csrf.token":"041585d7b172a0aba92ea3512740bbfd","system.paths":{"root":"","base":""},"rl_tabs":{"use_hash":1,"reload_iframes":0,"init_timeout":0,"urlscroll":0},"rl_modals":{"class":"modal_link","defaults":{"opacity":"0.8","maxWidth":"95%","maxHeight":"95%","current":"{current} \/ {total}","previous":"previous","next":"next","close":"close","xhrError":"This content failed to load.","imgError":"This image failed to load."},"auto_correct_size":1,"auto_correct_size_delay":0}}</script>
	<script src="/t3-assets/js/js-bc1a5-92488.js" type="text/javascript"></script>
	<script src="/media/tabs/js/script.min.js?v=7.4.0" type="text/javascript"></script>
	<script src="/t3-assets/js/js-85fdc-51698.js" type="text/javascript"></script>
	<script src="/media/modals/js/script.min.js?v=9.13.0" type="text/javascript"></script>
	<script src="/t3-assets/js/js-64565-41799.js" type="text/javascript"></script>
	<script src="/media/widgetkit/uikit2-1bf5db43.js" type="text/javascript"></script>
	<script src="/media/widgetkit/wk-scripts-0e7c0ac4.js" type="text/javascript"></script>
	<script type="text/javascript">

		jQuery(function($) {
			SqueezeBox.initialize({});
			SqueezeBox.assign($('a.modal').get(), {
				parse: 'rel'
			});
		});

		window.jModalClose = function () {
			SqueezeBox.close();
		};
		
		// Add extra modal close functionality for tinyMCE-based editors
		document.onreadystatechange = function () {
			if (document.readyState == 'interactive' && typeof tinyMCE != 'undefined' && tinyMCE)
			{
				if (typeof window.jModalClose_no_tinyMCE === 'undefined')
				{	
					window.jModalClose_no_tinyMCE = typeof(jModalClose) == 'function'  ?  jModalClose  :  false;
					
					jModalClose = function () {
						if (window.jModalClose_no_tinyMCE) window.jModalClose_no_tinyMCE.apply(this, arguments);
						tinyMCE.activeEditor.windowManager.close();
					};
				}
		
				if (typeof window.SqueezeBoxClose_no_tinyMCE === 'undefined')
				{
					if (typeof(SqueezeBox) == 'undefined')  SqueezeBox = {};
					window.SqueezeBoxClose_no_tinyMCE = typeof(SqueezeBox.close) == 'function'  ?  SqueezeBox.close  :  false;
		
					SqueezeBox.close = function () {
						if (window.SqueezeBoxClose_no_tinyMCE)  window.SqueezeBoxClose_no_tinyMCE.apply(this, arguments);
						tinyMCE.activeEditor.windowManager.close();
					};
				}
			}
		};
		
jQuery(window).on('load',  function() {
				new JCaption('img.caption');
			});
function addLoadEvent(func) {
	  var oldonload = window.onload;
	  if (typeof window.onload != 'function') {
	    window.onload = func;
	  } else {
	    window.onload = function() {
	      if (oldonload) {
	        oldonload();
	      }
	      func();
	    }
	  }
	}
		jQuery(document).ready(function(){
			new Slideshowck('#camera_wrap_451', {
				 height: '200',
				 minHeight: '150',
				 pauseOnClick: false,
				 hover: 1,
				 fx: 'random',
				 loader: 'none',
				 pagination: 1,
				 thumbnails: 1,
				 thumbheight: 75,
				 thumbwidth: 100,
				 time: 7000,
				 transPeriod: 1500,
				 alignment: 'topLeft',
				 autoAdvance: 1,
				 mobileAutoAdvance: 1,
				 portrait: 1,
				 barDirection: 'leftToRight',
				 imagePath: '/modules/mod_slideshowck/images/',
				 lightbox: 'mediaboxck',
				 fullpage: 0,
				 mobileimageresolution: '0',
				 navigationHover: true,
					mobileNavHover: true,
					navigation: true,
					playPause: true,
				 barPosition: 'bottom',
				 responsiveCaption: 0,
				 keyboardNavigation: 0,
				 container: ''
		});
}); 

		jQuery(document).ready(function(){
			new Slideshowck('#camera_wrap_407', {
				 height: '150',
				 minHeight: '150',
				 pauseOnClick: false,
				 hover: 1,
				 fx: 'random',
				 loader: 'none',
				 pagination: 1,
				 thumbnails: 1,
				 thumbheight: 75,
				 thumbwidth: 100,
				 time: 7000,
				 transPeriod: 1500,
				 alignment: 'topLeft',
				 autoAdvance: 1,
				 mobileAutoAdvance: 1,
				 portrait: 1,
				 barDirection: 'leftToRight',
				 imagePath: '/modules/mod_slideshowck/images/',
				 lightbox: 'mediaboxck',
				 fullpage: 0,
				 mobileimageresolution: '0',
				 navigationHover: true,
					mobileNavHover: true,
					navigation: true,
					playPause: true,
				 barPosition: 'bottom',
				 responsiveCaption: 0,
				 keyboardNavigation: 0,
				 container: ''
		});
}); 

		jQuery(document).ready(function(){
			new Slideshowck('#camera_wrap_446', {
				 height: '300',
				 minHeight: '150',
				 pauseOnClick: false,
				 hover: 1,
				 fx: 'random',
				 loader: 'none',
				 pagination: 1,
				 thumbnails: 1,
				 thumbheight: 75,
				 thumbwidth: 100,
				 time: 7000,
				 transPeriod: 1500,
				 alignment: 'topLeft',
				 autoAdvance: 1,
				 mobileAutoAdvance: 1,
				 portrait: 1,
				 barDirection: 'leftToRight',
				 imagePath: '/modules/mod_slideshowck/images/',
				 lightbox: 'mediaboxck',
				 fullpage: 0,
				 mobileimageresolution: '0',
				 navigationHover: true,
					mobileNavHover: true,
					navigation: true,
					playPause: true,
				 barPosition: 'bottom',
				 responsiveCaption: 0,
				 keyboardNavigation: 0,
				 container: ''
		});
}); 

		jQuery(document).ready(function(){
			new Slideshowck('#camera_wrap_376', {
				 height: '300',
				 minHeight: '150',
				 pauseOnClick: false,
				 hover: 1,
				 fx: 'random',
				 loader: 'none',
				 pagination: 1,
				 thumbnails: 1,
				 thumbheight: 75,
				 thumbwidth: 100,
				 time: 7000,
				 transPeriod: 1500,
				 alignment: 'topLeft',
				 autoAdvance: 1,
				 mobileAutoAdvance: 1,
				 portrait: 1,
				 barDirection: 'leftToRight',
				 imagePath: '/modules/mod_slideshowck/images/',
				 lightbox: 'mediaboxck',
				 fullpage: 0,
				 mobileimageresolution: '0',
				 navigationHover: true,
					mobileNavHover: true,
					navigation: true,
					playPause: true,
				 barPosition: 'bottom',
				 responsiveCaption: 0,
				 keyboardNavigation: 0,
				 container: ''
		});
}); 

	</script>

	
<!-- META FOR IOS & HANDHELD -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<style type="text/stylesheet">
		@-webkit-viewport   { width: device-width; }
		@-moz-viewport      { width: device-width; }
		@-ms-viewport       { width: device-width; }
		@-o-viewport        { width: device-width; }
		@viewport           { width: device-width; }
	</style>
	<script type="text/javascript">
		//<![CDATA[
		if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
			var msViewportStyle = document.createElement("style");
			msViewportStyle.appendChild(
				document.createTextNode("@-ms-viewport{width:auto!important}")
			);
			document.getElementsByTagName("head")[0].appendChild(msViewportStyle);
		}
		//]]>
	</script>
<meta name="HandheldFriendly" content="true">
<meta name="apple-mobile-web-app-capable" content="YES">
<!-- //META FOR IOS & HANDHELD -->




<!-- Le HTML5 shim and media query for IE8 support -->
<!--[if lt IE 9]>
<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
<script type="text/javascript" src="/plugins/system/t3/base-bs3/js/respond.min.js"></script>
<![endif]-->

<!-- You can add Google Analytics here or use T3 Injection feature -->



<!-- CoalaWeb Google Analytics -->
<script>      
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    
    ga('create', 'UA-123988338-1', 'auto');
    ga('send', 'pageview');
</script>
<!-- End CoalaWeb Google Analytics -->

<style type="text/css">#twojContentSliderId1{ width: 100%; position:relative; padding:0; } #twojContentSliderId1Inner{ width:100%; position:relative; } </style><style type="text/css">.t3-megamenu.animate .animating > .mega-dropdown-menu,.t3-megamenu.animate.slide .animating > .mega-dropdown-menu > div {transition-duration: 400ms !important;-webkit-transition-duration: 400ms !important;}</style><meta http-equiv="origin-trial" content="AymqwRC7u88Y4JPvfIF2F37QKylC04248hLCdJAsh8xgOfe/dVJPV3XS3wLFca1ZMVOtnBfVjaCMTVudWM//5g4AAAB7eyJvcmlnaW4iOiJodHRwczovL3d3dy5nb29nbGV0YWdtYW5hZ2VyLmNvbTo0NDMiLCJmZWF0dXJlIjoiUHJpdmFjeVNhbmRib3hBZHNBUElzIiwiZXhwaXJ5IjoxNjk1MTY3OTk5LCJpc1RoaXJkUGFydHkiOnRydWV9">

    </head>
    <body>
        <!-- @section('sidebar')
            This is the master sidebar.
        @show -->
        <div>
            @include('layouts.topmenu')
        </div>
 
        <div class="container">
            @yield('content')
        </div>
    </body>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
</html>