<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class KementerianCollection extends ResourceCollection // utk array
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        //return parent::toArray($request);
        return [
            'data'=>$this->collection,  //collection=json refer kat JabatanController
            'version'=>'0.0.1',
            'ownwer'=>'iab',
         ];

    }
}
