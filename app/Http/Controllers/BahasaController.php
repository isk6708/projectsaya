<?php

namespace App\Http\Controllers;

use App\Models\Bahasa;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Resources\BahasaResource;
use App\Http\Resources\BahasaCollection;

class BahasaController extends Controller
{
     /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //data dalam bentuk array
        $bahasa=Bahasa::all();
        return response()->json(
            //new bahasaCollection($bahasa); //atau
            new BahasaCollection($bahasa),response::HTTP_OK);

        //var_dump($bahasa);

          //tukar ke data dalam bentuk json
       return new BahasaCollection($bahasa);
         
        //nak data kosong(array) ...kalu nak customize guna collection(guna command yg atas)
        //return new bahasaResourse($bahasa);   

    }

    /**
     * Store a newly created resource in storage.
     */
    
     public function store(Request $request)
    {
        $bahasa=Bahasa::create($request->only([
            'kodbahasa','bahasa'
        ]));
        return new BahasaResource($bahasa);
    }

    /**
     * Display the specified resource.
     */
    public function show(Bahasa $bahasa)
    {
        //
        return new BahasaResource($bahasa); //$bahasa panggil data cth 1,2,3
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, bahasa $bahasa)
    {
        //jgn letak kalu tak nak update
        $bahasa->update($request->only([
            'kodbahasa','bahasa'
        ]));
        return new BahasaResource($bahasa);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Bahasa $bahasa)
    {
        //delete rekod apa nak 
        $bahasa->delete();
        //berjaya 
        return response()->json('SUCCESS',Response::HTTP_I_AM_TEAPOT);
        
    }
}
