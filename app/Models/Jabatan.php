<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jabatan extends Model
{
    use HasFactory;
    //protected $fillable;
    protected $guarded;
    public function kementerians()
    {
        return $this->belongsTo(kementerian::class,'kodkementerian','kodkementerian');
    }
}
