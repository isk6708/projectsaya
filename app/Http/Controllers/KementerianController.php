<?php

namespace App\Http\Controllers;

use App\Models\Kementerian;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Resources\KementerianCollection;
use App\Http\Resources\KementerianResource;

class KementerianController extends Controller
{
     /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //data dalam bentuk array
        $kementerian=Kementerian::all();
        return response()->json(
            //new kementerianCollection($kementerian); //atau
            new KementerianCollection($kementerian),response::HTTP_OK);

        //var_dump($kementerian);

          //tukar ke data dalam bentuk json
       return new KementerianCollection($kementerian);
         
        //nak data kosong(array) ...kalu nak customize guna collection(guna command yg atas)
        //return new kementerianResourse($kementerian);   

    }

    /**
     * Store a newly created resource in storage.
     */
    
     public function store(Request $request)
    {
        $kementerian=Kementerian::create($request->only([
            'kodkementerian','acronym','namakementerian','editedby'
        ]));
        return new KementerianResource($kementerian);
    }

    /**
     * Display the specified resource.
     */
    public function show(Kementerian $kementerian)
    {
        //
        return new KementerianResource($kementerian); //$kementerian panggil data cth 1,2,3
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, kementerian $kementerian)
    {
        //jgn letak kalu tak nak update
        $kementerian->update($request->only([
            'kodkementerian','acronym','namakementerian','editedby'
        ]));
        return new KementerianResource($kementerian);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Kementerian $kementerian)
    {
        //delete rekod apa nak 
        $kementerian->delete();
        //berjaya 
        return response()->json('BERJAYA',response::HTTP_I_AM_TEAPOT);
        
    }
}
