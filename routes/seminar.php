<?php 
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SeminarController;

Route::any('/seminar/cari', [SeminarController::class,'index'])->name('seminar.cari');
Route::get('/seminar/sunting/{id}', [SeminarController::class,'edit'])->name('seminar.edit');
Route::put('/seminar/kemaskini/{id}', [SeminarController::class,'update'])->name('seminar.kemaskini');
Route::get('/seminar/daftar',[SeminarController::class,'create'])->name('seminar.daftar');
Route::post('/seminar/simpan', [SeminarController::class,'store'])->name('seminar.simpan');
Route::delete('/seminar/hapus/{id}', [SeminarController::class,'destroy'])->name('seminar.hapus');