<?php

namespace App\Http\Controllers;

use App\Models\Jabatan;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Resources\JabatanCollection;
use App\Http\Resources\JabatanResource;

//nak check error guna chatgpt

class JabatanController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //data dalam bentuk array
        $jabatan=Jabatan::all();
        return response()->json(
            //new JabatanCollection($jabatan); //atau
            new JabatanCollection($jabatan),response::HTTP_OK);

        //var_dump($jabatan);

          //tukar ke data dalam bentuk json
       return new JabatanCollection($jabatan);
         
        //nak data kosong(array) ...kalu nak customize guna collection(guna command yg atas)
        //return new JabatanResourse($jabatan);   

    }

    /**
     * Store a newly created resource in storage.
     */
    
     public function store(Request $request)
    {
        $jabatan=Jabatan::create($request->only([
            'kodjabatan','namajabatan','kodkementerian'//,'editedby'
        ]));
        return new JabatanResource($jabatan);
    }

    /**
     * Display the specified resource.
     */
    public function show(Jabatan $jabatan)
    {
        //
        return new JabatanResource($jabatan); //$jabatan panggil data cth 1,2,3
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Jabatan $jabatan)
    {
        //jgn letak kalu tak nak update
        $jabatan->update($request->only([
            'kodjabatan','namajabatan','kodkementerian'//,'editedby'
        ]));
        return new JabatanResource($jabatan);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Jabatan $jabatan)
    {
        //delete rekod apa nak 
        $jabatan->delete();
        //berjaya 
        return response()->json('SUCCESS',Response::HTTP_I_AM_TEAPOT);
        
    }
}
