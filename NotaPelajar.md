Module Seminar
- Jadual Seminar
Kategori Seminar p

Route	

Fungsi Selenggara Seminar
 - Carian - index/list
 - Wujud Seminar - create/store
 - Kemaskini Seminar - edit/update
 - Hapus Seminar - destroy/delete

seminars

id - bigint - autoincr
seminar_name varchar(255) - nullable()
seminar_cat varchar(50) - nullable()
details - json - nullable()
created_by bigint() - nullable()
updated_by bigint() - nullable()
timestamps

- Login - Pluggin - (Breeze, Laravel UI) - Laravel 10.x - Sanctum
- CRUD
- PDF/Excel Export
- Upload/Download Dokumen
- Bootstrap - Vue/Angular/NodeJs
- Validation
- Email/Notification
- Pagination - chunking
- Front End Script - javascript/jquery 
- RBAC/ACL - Spatie
- API
- Migration
- Seeding
- JSON Document - optional

DB-
Document Store/JSON - MongoDb, MySQL, SQL Server

Day 3
Controller
Model

20231025 - Rabu - Kuala Selangor



