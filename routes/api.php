<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\JabatanController;
use App\Http\Controllers\KementerianController;
use App\Http\Controllers\BahasaController; 
use App\Http\Controllers\AgamaController; 
use App\Http\Controllers\KampusController; 

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

/*Route::get('/jabatans',[JabatanController::class,'index']);*/

//general utk semua
Route::apiResource('/jabatans',JabatanController::class);
/*tiada arahan limit,guna
Route::resource('/jabatans',JabatanController::class)->only([]'index','store']);*/

Route::apiResource('/kementerians',KementerianController::class);

Route::apiResource('/bahasas',BahasaController::class);

Route::apiResource('/agamas',AgamaController::class);

Route::apiResource('/kampuses',KampusController::class);