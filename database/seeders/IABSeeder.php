<?php

namespace Database\Seeders;

use App\Models\Reference;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class IABSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Reference::where('cat','SEMINARCAT')->delete();
        $arr = [
            ['cat'=>'SEMINARCAT','code'=>'10','descr'=>'Seminar','sort'=>'10'],
            ['cat'=>'SEMINARCAT','code'=>'20','descr'=>'Simposium','sort'=>'20'],
            ['cat'=>'SEMINARCAT','code'=>'30','descr'=>'Persidangan','sort'=>'30'],
            ['cat'=>'SEMINARCAT','code'=>'40','descr'=>'Kolokium','sort'=>'40'],
        ];
        Reference::insert($arr);

        Reference::where('cat','SEMINARSUBCAT')->delete();
        $arr = [
            ['cat'=>'SEMINARSUBCAT','code'=>'1010','descr'=>'Digital','sort'=>'10','param'=>'10'],
            ['cat'=>'SEMINARSUBCAT','code'=>'1020','descr'=>'Kepimpinan','sort'=>'20','param'=>'10'],
            ['cat'=>'SEMINARSUBCAT','code'=>'1030','descr'=>'STEM','sort'=>'30','param'=>'10'],
            ['cat'=>'SEMINARSUBCAT','code'=>'1040','descr'=>'Kolokium','sort'=>'40','param'=>'10'],

            ['cat'=>'SEMINARSUBCAT','code'=>'2010','descr'=>'Digital ICT','sort'=>'10','param'=>'20'],
            ['cat'=>'SEMINARSUBCAT','code'=>'2020','descr'=>'Kepimpinan Pendidikan','sort'=>'20','param'=>'20'],
            ['cat'=>'SEMINARSUBCAT','code'=>'2030','descr'=>'STEM Dalam Pendidikan','sort'=>'30','param'=>'20'],
            ['cat'=>'SEMINARSUBCAT','code'=>'2040','descr'=>'Kolokium Sains','sort'=>'40','param'=>'20'],
        ];
        Reference::insert($arr);
    }
}
