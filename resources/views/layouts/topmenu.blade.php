<nav class="navbar navbar-expand-lg bg-body-tertiary">
  <div class="container-fluid">
    <a class="navbar-brand" href="/">IAB</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="/">Utama</a>
        </li>
        @auth
        <li class="nav-item">
          <a class="nav-link" href="#">Kewangan</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Seminar
          </a>
          <ul class="dropdown-menu">
            <li><a class="dropdown-item" href="{{route('seminar.cari')}}">Urus Seminar</a></li>
            <li><a class="dropdown-item" href="#">Urus Jadual Seminar</a></li>
            <li><a class="dropdown-item" href="#">Urus Peserta Seminar</a></li>
          </ul>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{route('profile.edit')}}">Profil</a>
        </li>
        <li class="nav-item">
            <form action="{{route('logout')}}" method="POST">
                @csrf
                <input type="submit" class="nav-link" value="Log Keluar">
            </form>          
        </li>
        @endauth
        @guest
        <li class="nav-item">
          <a class="nav-link" href="{{route('login')}}">Log Masuk</a>
        </li>        
        <li class="nav-item">
          <a class="nav-link" href="{{route('register')}}">Daftar</a>
        </li>
        @endguest
      </ul>      
    </div>
  </div>
</nav>