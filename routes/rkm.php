<?php

Route::get('/jabatan/tambah',function(){
    $data['id'] = '';
    return view('rkm._form_jabatan',$data);
})->name('jabatan.tambah.rkm');

Route::get('/jabatan/edit/{id}',function($id){
    $data['id'] = $id;
    return view('rkm._form_jabatan',$data);
})->name('jabatan.sunting.rkm');

Route::get('/jabatan/senarai',function(){
    return view('rkm._senarai_jabatan');
})->name('jabatan.senarai.rkm');