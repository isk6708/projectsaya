<?php

use App\Http\Controllers\ProfileController;
use App\Http\Controllers\NoteController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

include 'seminar.php';
include 'rkm.php';
include 'athirah.php';
include 'aliza.php';
include 'awin.php';
include 'rohaima.php';
include 'noriati.php';
// include 'kewangan.php';

Route::get('/', function () {
    // return view('welcome');
    return view('landing_page');
});

Route::get('/dashboard', function () {
    // return view('dashboard');
    return view('iab_dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});


 
Route::resource('/notes', NoteController::class )->middleware('auth');

require __DIR__.'/auth.php';
