<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reference extends Model
{
    use HasFactory;

    public static function findByCat($cat){
        $data = self::where('cat',$cat)
        ->orderBy('sort')->get()
        ->pluck('descr','code');

        return $data->toArray();
    }
}
