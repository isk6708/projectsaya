<?php

namespace App\Http\Controllers;

use App\Models\Kampus;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Resources\KampusCollection;
use App\Http\Resources\KampusResource;

class KampusController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //data dalam bentuk array
        $kampus=Kampus::all();
        return response()->json(
            //new kampusCollection($kampus); //atau
            new KampusCollection($kampus),response::HTTP_OK);

        //var_dump($kampus);

          //tukar ke data dalam bentuk json
       return new KampusCollection($kampus);
         
        //nak data kosong(array) ...kalu nak customize guna collection(guna command yg atas)
        //return new kampusResourse($kampus);   

    }

    /**
     * Store a newly created resource in storage.
     */
    
     public function store(Request $request)
    {
        $kampus=Kampus::create($request->only([
            'kodkampus','namakampus','kodkementerian','editedby'
        ]));
        return new KampusResource($kampus);
    }

    /**
     * Display the specified resource.
     */
    public function show(Kampus $kampus)
    {
        //
        return new KampusResource($kampus); //$kampus panggil data cth 1,2,3
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Kampus $kampus)
    {
        //jgn letak kalu tak nak update
        $kampus->update($request->only([
            'kodkampus','namakampus','kodkementerian','editedby'
        ]));
        return new KampusResource($kampus);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Kampus $kampus)
    {
        //delete rekod apa nak 
        $kampus->delete();
        //berjaya 
        return response()->json('BERJAYA',response::HTTP_I_AM_TEAPOT);
        
    }
}
