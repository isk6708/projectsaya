@extends('layouts.iab')
@section('title', 'IAB Paparan Utama')
@section('content')
<h1>Dashboard IAB</h1>

<div class="t3-wrapper"> <!-- Need this wrapper for off-canvas menu. Remove if you don't use of-canvas -->

  
	<!-- TOPBAR -->
	<div class="wrap t3-topbar ">
    <div class="container">
      <div class="row">
        
				<!-- Top Bar -->
        					<!-- asal <div class="topbar hidden-xs hidden-sm hidden-md col-lg-6"> -->
                     <div class="topbar col-lg-6">
						<ul class="nav nav-pills nav-stacked menu">
<li class="item-338"><a href="https://mail.google.com/a/iab.moe.gov.my" class="" target="_blank" rel="noopener noreferrer">E-mel Staf </a></li><li class="item-339"><a href="https://mail.google.com/a/ilmu.iab.edu.my" class="" target="_blank" rel="noopener noreferrer">E-mel Peserta </a></li><li class="item-710"><a href="/index.php/ms/pages-menu-cawangan" class="">IAB Cawangan </a></li><li class="item-753"><a href="https://sistem.iab.edu.my/insystv2/" class="" target="_blank" rel="noopener noreferrer">InSyst </a></li><li class="item-863"><a href="/index.php/ms/pik-menu-iab-2" class="" target="_blank">PIK</a></li><li class="item-765"><a href="/index.php/ms/spq-iab" class="">SP-Q</a></li></ul>

					</div>
                <!-- // Top Bar -->

        					<!-- asal <div class="topbar-right pull-right col-xs-8 col-md-4"> -->
                  <div class="topbar-right pull-right col-xs-8 col-md-4">
						<!-- Off Canvas -->
						
						<!-- head search -->
													<div class="dropdown nav-search pull-right">
								<a data-toggle="dropdown" href="#" class="dropdown-toggle">
									<i class="fa fa-search"></i>									
								</a>
								<div class="nav-child dropdown-menu container">
									<div class="dropdown-menu-inner">
										<style type="text/css">
/* Corner radius */
.ui-corner-all, .ui-corner-top, .ui-corner-left, .ui-corner-tl { -moz-border-radius-topleft: 6px; -webkit-border-top-left-radius: 6px; -khtml-border-top-left-radius: 6px; border-top-left-radius: 6px; }
.ui-corner-all, .ui-corner-top, .ui-corner-right, .ui-corner-tr { -moz-border-radius-topright: 6px; -webkit-border-top-right-radius: 6px; -khtml-border-top-right-radius: 6px; border-top-right-radius: 6px; }
.ui-corner-all, .ui-corner-bottom, .ui-corner-left, .ui-corner-bl { -moz-border-radius-bottomleft: 6px; -webkit-border-bottom-left-radius: 6px; -khtml-border-bottom-left-radius: 6px; border-bottom-left-radius: 6px; }
.ui-corner-all, .ui-corner-bottom, .ui-corner-right, .ui-corner-br { -moz-border-radius-bottomright: 6px; -webkit-border-bottom-right-radius: 6px; -khtml-border-bottom-right-radius: 6px; border-bottom-right-radius: 6px; }
.ui-widget-content { border:1px solid #dedede;  background-color: #ffffff;}
.ui-widget-header  { border-radius:6px; display:block;border: 1px solid #dddddd; background: #e9e9e9;  font-weight: bold; color:#333333; }
.ui-dialog .ui-dialog-titlebar{ display:none;}
.ui-dialog-titlebar.ui-widget-header.popupjt{ display:block;}
</style>

	<script type="text/javascript">
	jQuery.noConflict();
jQuery( document ).ready(function( jQuery ) {
			jQuery("#popup").dialog({
				autoOpen:false,
				position: { my: "center center", at: "center center", of: window },
				hide: { effect: 'fade', duration:300 }, 
				show: { effect: 'drop',direction: 'up', duration:400 }, 
                modal: false,
				resizable: false,
				title:"Pop Up Title",
				width: "300",
				draggable: false,
				height: "auto",
				closeOnEscape:true,
				open: function(event, ui){
     setTimeout("jQuery('#popup').dialog('close')",5000);
    }
			});
						
			jQuery("#button").on("click", function() {
				jQuery("#popup").dialog("open");
				  
			});
			jQuery(".ui-dialog-titlebar-close").on("click",function(){
				 jQuery("#popup").dialog("close");
				 
		});
jQuery( "#opener" ).click(function() {
               jQuery( "#popup" ).dialog( "open" );
            });
		});
	</script><div class="search">
	<form action="/index.php/ms/" method="post" class="form-inline form-search no-button">
		<input name="searchword" value="" id="mod-search-searchword" maxlength="200" class="form-control search-query" type="search" size="0" placeholder="Carian">		<input type="hidden" name="task" value="search">
		<input type="hidden" name="option" value="com_search">
		<input type="hidden" name="Itemid" value="137">
	</form>
</div>

									</div>
								</div>
							</div>
												<!-- //head search -->

						<!-- topbar right -->
													
<script language="javascript" type="text/javascript">
<!--
defaultSize = 80;
//-->
</script><script type="text/javascript" src="/modules/mod_ppc_fastfont/js/fastfont.js"></script>

<div id="fontsize" style="font-size:16px;line-height:110%;text-align:center;">

<div style="border:none;margin:0px 0px 0px 5px;background:#686868 url(modules/mod_ppc_fastfont/img/20none.png);float:left;height:20px;width:20px;">
<a style="text-decoration: none" href="/index.php" title="Increase size" onclick="changeFontSize(1); return false;" class="larger">
<img style="margin:0; padding:0;" src="/modules/mod_ppc_fastfont/img/20fontincrease.png" alt="Increase size"></a>
</div>
<div style="border:none;margin:0px 0px 0px 5px;background:#686868 url(modules/mod_ppc_fastfont/img/20none.png);float:left;height:20px;width:20px;">
<a style="text-decoration: none" href="/index.php" title="Reset font size to default" onclick="revertStyles(); return false;" class="reset">
<img style="margin:0; padding:0;" src="/modules/mod_ppc_fastfont/img/20fontreset.png" alt="Reset to Default"></a>
</div>
<div style="border:none;margin:0px 0px 0px 5px;letter-spacing:-1px;background:#686868 url(modules/mod_ppc_fastfont/img/20none.png);float:left;height:20px;width:20px;">
<a style="text-decoration: none" href="/index.php" title="Decrease size" onclick="changeFontSize(-1); return false;" class="smaller">
<img style="margin:0; padding:0;" src="/modules/mod_ppc_fastfont/img/20fontdecrease.png" alt="Decrease size"></a>
			</div>	

</div>

												<!-- // topbar right -->
					</div>
              </div>
    </div>
	</div>
	<!-- //TOPBAR -->

	
  
<!-- HEADER -->
<header id="t3-header" class="t3-header affix-top">
	<div class="container">
		<div class="row">

			<!-- LOGO -->
			<div class="col-xs-6 col-sm-3 col-md-3 logo">
				<div class="logo-image">
					<a href="/" title="Portal Rasmi Institut Aminuddin Baki">
													<img class="logo-img" src="/images/banners/banner_iab2023-terkini.png" alt="Portal Rasmi Institut Aminuddin Baki">
																		<span>Portal Rasmi Institut Aminuddin Baki</span>
					</a>
					<small class="site-slogan">Portal Rasmi</small>
				</div>
			</div>
			<!-- //LOGO -->

			<!-- MAIN NAVIGATION -->
			<nav id="t3-mainnav" class="col-xs-6 col-sm-9 col-md-8 navbar navbar-default t3-mainnav pull-right">

					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
					
																				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".t3-navbar-collapse">
								<i class="fa fa-bars"></i>
							</button>
											</div>

											<div class="t3-navbar-collapse navbar-collapse collapse"><ul itemscope="" itemtype="http://www.schema.org/SiteNavigationElement" class="nav navbar-nav level0">
<li itemprop="name" class="current active">
<a itemprop="url" href="/index.php/ms/">Laman Utama </a>

</li>
<li itemprop="name" class="">
<span> Kenali IAB<em class="caret"></em></span>


<ul class="level1 dropdown-menu"><li itemprop="name">
<a itemprop="url" href="/index.php/ms/ttgiab/tentang-iab">Korporat </a>

</li><li itemprop="name">
<a itemprop="url" href="/index.php/ms/ttgiab/carta-organisasi">Carta Organisasi </a>

</li><li itemprop="name">
<a itemprop="url" href="/index.php/ms/ttgiab/men-pependaf">Pejabat Pendaftar </a>

</li><li itemprop="name" class="mega-group">
<span> Kluster Khidmat Profesional</span>


<ul class="level2 dropdown-menu"><li itemprop="name">
<a itemprop="url" href="/index.php/ms/ttgiab/kluster-khidmat-profesional/pusat-pengurusan-dasar-dan-inovasi">Pusat Pengurusan Dasar dan Inovasi </a>

</li><li itemprop="name">
<a itemprop="url" href="/index.php/ms/ttgiab/kluster-khidmat-profesional/pusat-pentaksiran-dan-pembangunan-bakat">Pusat Pentaksiran dan Pembangunan Bakat </a>

</li><li itemprop="name">
<a itemprop="url" href="/index.php/ms/ttgiab/kluster-khidmat-profesional/pusat-pembangunan-dan-pengurusan-teknologi">Pusat Pembangunan dan Pengurusan Teknologi </a>

</li><li itemprop="name">
<a itemprop="url" href="/index.php/ms/ttgiab/kluster-khidmat-profesional/pusat-dokumentasi-dan-sumber-pendidikan">Pusat Dokumentasi dan Sumber Pendidikan </a>

</li></ul></li><li itemprop="name" class="mega-group">
<span> Kluster Khidmat Latihan</span>


<ul class="level2 dropdown-menu"><li itemprop="name">
<a itemprop="url" href="/index.php/ms/ttgiab/kluster-khidmat-latihan/pusat-penyelidikan-dan-penilaian">Pusat Penyelidikan dan Penilaian </a>

</li><li itemprop="name">
<a itemprop="url" href="/index.php/ms/ttgiab/kluster-khidmat-latihan/pusat-pembangunan-kepimpinan-pendidikan">Pusat Pembangunan Kepimpinan Pendidikan </a>

</li><li itemprop="name">
<a itemprop="url" href="/index.php/ms/ttgiab/kluster-khidmat-latihan/pusat-pengurusan-dan-pentadbiran-pendidikan">Pusat Pengurusan dan Pentadbiran Pendidikan </a>

</li><li itemprop="name">
<a itemprop="url" href="/index.php/ms/ttgiab/kluster-khidmat-latihan/pusat-konsultasi-dan-pembangunan-organisasi-pendidikan">Pusat Konsultasi dan Pembangunan Organisasi Pendidikan </a>

</li><li itemprop="name">
<a itemprop="url" href="/index.php/ms/ttgiab/kluster-khidmat-latihan/pusat-pembangunan-komuniti">Pusat Pembangunan Komuniti </a>

</li></ul></li><li itemprop="name" class="mega-group">
<span> IAB Cawangan</span>


<ul class="level2 dropdown-menu"><li itemprop="name">
<a itemprop="url" href="/index.php/ms/ttgiab/iab-cawangan/iab-cawangan-genting-highlands">IAB Cawangan Genting Highlands </a>

</li><li itemprop="name">
<a itemprop="url" href="/index.php/ms/ttgiab/iab-cawangan/iab-cawangan-utara">IAB Cawangan Utara </a>

</li><li itemprop="name">
<a itemprop="url" href="/index.php/ms/ttgiab/iab-cawangan/iab-cawangan-sabah">IAB Cawangan Sabah </a>

</li><li itemprop="name">
<a itemprop="url" href="/index.php/ms/ttgiab/iab-cawangan/iab-cawangan-sarawak-baru">IAB Cawangan Sarawak </a>

</li></ul></li></ul></li>
<li itemprop="name" class="">
<span> Perkhidmatan<em class="caret"></em></span>


<ul class="level1 dropdown-menu"><li itemprop="name" class="mega-group">
<span> Aplikasi Dalam Talian</span>


<ul class="level2 dropdown-menu"><li itemprop="name">
<a itemprop="url" href="https://sistem.iab.edu.my/insystv2/" target="_blank">Sistem Bersepadu IAB (Insyst)</a>

</li><li itemprop="name">
<a itemprop="url" href="https://sistem.iab.edu.my/ekossmik/login.php" target="_blank">Sistem Konsultasi Sehenti Sentral Maya Integrasi Kepimpinan (KoSSMIK)</a>

</li><li itemprop="name">
<a itemprop="url" href="https://sistem.iab.edu.my/insyst/main/" target="_blank">Permohonan Kursus</a>

</li><li itemprop="name">
<a itemprop="url" href="https://sistem.iab.edu.my/insyst/main/" target="_blank">Profil Staf/Peserta</a>

</li><li itemprop="name">
<a itemprop="url" href="https://sistem.iab.edu.my/insystv2/modul-sijil-online/cetak_sijilv2.php" target="_blank">Cetakan Sijil dalam Talian (Tahun Semasa)</a>

</li><li itemprop="name">
<a itemprop="url" href="https://sistem.iab.edu.my/uppv2/modul-sijil-online/cetakan_sijilonline_view.php" target="_blank">Cetakan Sijil dalam Talian (Data Arkib)</a>

</li><li itemprop="name">
<a itemprop="url" href="https://sistem.iab.edu.my/insystv2/login/" target="_blank">i-KOMPAS 2.0</a>

</li></ul></li><li itemprop="name" class="mega-group">
<span> Aplikasi Dalam Talian KPM</span>


<ul class="level2 dropdown-menu"><li itemprop="name">
<a itemprop="url" href="https://tls.moe.gov.my/login.php" target="_blank">Program Kelayakan Profesional Pemimpin Pendidikan Kebangsaan Modul Penggantian Pemimpin Sekolah (NPQEL MPPS)</a>

</li><li itemprop="name">
<a itemprop="url" href="https://sgmy.moe.gov.my/KPMXPERT/index.cfm" target="_blank">Direktori Kepakaran KPM</a>

</li><li itemprop="name">
<a itemprop="url" href="/index.php/ms/pages/aplikasi-dalam-talian-2/direktori-kepakaran-2">Direktori Kepakaran IAB </a>

</li></ul></li><li itemprop="name" class="mega-group">
<span> Instrumen Dalam Talian</span>


<ul class="level2 dropdown-menu"><li itemprop="name">
<a itemprop="url" href="https://leser.iab.edu.my/" target="_blank">Leadership Self Evaluation Rating (LeSER)</a>

</li></ul></li><li itemprop="name" class="mega-group">
<span> Laman Mini/Web</span>


<ul class="level2 dropdown-menu"><li itemprop="name">
<a itemprop="url" href="http://eprints.iab.edu.my/v2/" target="_blank">Repositori</a>

</li><li itemprop="name">
<a itemprop="url" href="http://lib.iab.edu.my/" target="_blank">Portal Pusat Sumber IAB</a>

</li><li itemprop="name">
<a itemprop="url" href="/index.php/ms/pages/menu-ap-web/sp-q-iab">Sistem Pengurusan Kualiti (SP-Q) </a>

</li><li itemprop="name">
<a itemprop="url" href="/index.php/ms/pages/menu-ap-web/master-trainer">Program Master Trainer </a>

</li><li itemprop="name">
<a itemprop="url" href="/index.php/ms/pages/menu-ap-web/kkriab-iab">Kelab Kebajikan dan Rekreasi IAB (KKRIAB) </a>

</li><li itemprop="name">
<a itemprop="url" href="/index.php/ms/pages/menu-ap-web/puspanita-iab">Persatuan Suri dan Anggota Wanita Perkhidmatan Awam Malaysia (PUSPANITA) </a>

</li><li itemprop="name">
<a itemprop="url" href="/index.php/ms/pages/menu-ap-web/penyelidikan-iab-menu">Penyelidikan IAB </a>

</li><li itemprop="name">
<a itemprop="url" href="/index.php/ms/pages/menu-ap-web/mainmenu-jk-keselamatan">JK Keselamatan dan Kesihatan Pekerjaan IAB </a>

</li><li itemprop="name">
<a itemprop="url" href="/index.php/ms/pages/menu-ap-web/mainmenu-propeks">Program Pelonjakan Kepimpinan Sekolah (ProPeKS) </a>

</li></ul></li></ul></li>
<li itemprop="name" class="">
<span> e-Pembelajaran<em class="caret"></em></span>


<ul class="level1 dropdown-menu"><li itemprop="name">
<a itemprop="url" href="http://cpd.iab.edu.my/" target="_blank">e-Pembelajaran Pembangunan Profesionalisme Berterusan (eP@CPD) </a>

</li><li itemprop="name">
<a itemprop="url" href="http://npqel.iab.edu.my/" target="_blank">e-Pembelajaran Program Kelayakan Profesional Pemimpin Pendidikan Kebangsaan (eP@NPQEL)</a>

</li><li itemprop="name">
<a itemprop="url" href="http://lcml.iab.edu.my/" target="_blank">e-Pembelajaran Kursus Kepimpinan Pemimpin Pertengahan (eP@LCML)</a>

</li><li itemprop="name">
<a itemprop="url" href="https://esidang.iab.edu.my/" target="_blank">e-Sidang</a>

</li></ul></li>
<li itemprop="name" class="dropdown">
<a itemprop="url" href="#" data-toggle="dropdown" data-target="#">Hubungi Kami<em class="caret"></em></a>


<ul class="level1 dropdown-menu"><li itemprop="name">
<a itemprop="url" href="/index.php/ms/peta-lokasi-iab-induk">Peta Lokasi IAB Induk</a>

</li><li itemprop="name">
<a itemprop="url" href="/index.php/ms/maklumat-iab-induk">Maklumat IAB Induk dan Cawangan</a>

</li><li itemprop="name">
<a itemprop="url" href="http://sistem2.iab.edu.my/idirektori/" target="_blank">Direktori Staf</a>

</li><li itemprop="name">
<a itemprop="url" href="/index.php/ms/student/bantuan-teknikal">Bantuan Teknikal </a>

</li><li itemprop="name">
<a itemprop="url" href="/index.php/ms/student/maklum-balas-menu-utama">Maklum Balas IAB </a>

</li><li itemprop="name">
<a itemprop="url" href="https://forms.gle/nNgi8yj3j8e1tr38A" target="_blank">Soal Selidik Pelawat Portal Rasmi IAB</a>

</li></ul></li>
<li itemprop="name" class="">
<span> Sumber<em class="caret"></em></span>


<ul class="level1 dropdown-menu"><li itemprop="name">
<a itemprop="url" href="/index.php/ms/sumber/menu-berita-pemberitahuan">Pengumuman </a>

</li><li itemprop="name">
<a itemprop="url" href="/index.php/ms/sumber/menu-sebutharga-tender">Sebut harga/Tender </a>

</li><li itemprop="name">
<a itemprop="url" href="/index.php/ms/sumber/menu-berita-aktiviti">Aktiviti IAB </a>

</li><li itemprop="name">
<a itemprop="url" href="/index.php/ms/sumber/terma-rujukan-tor">Terma Rujukan (TOR) </a>

</li><li itemprop="name">
<a itemprop="url" href="/index.php/ms/sumber/menu-kod-qr">Kod QR Portal IAB </a>

</li><li itemprop="name" class="mega-group">
<span> Terbitan Berkala</span>


<ul class="level2 dropdown-menu"><li itemprop="name">
<a itemprop="url" href="/index.php/ms/sumber/terbitan-berkala/laporan-tahunan">Laporan Tahunan </a>

</li><li itemprop="name">
<a itemprop="url" href="https://iab.moe.edu.my/index.php/ms/program-latihan#program-latihan-2022">Program Latihan</a>

</li><li itemprop="name">
<a itemprop="url" href="/index.php/ms/sumber/terbitan-berkala/jurnal-iab-terkini">Jurnal IAB </a>

</li></ul></li><li itemprop="name" class="mega-group">
<span> Terbitan IAB</span>


<ul class="level2 dropdown-menu"><li itemprop="name">
<a itemprop="url" href="/index.php/ms/sumber/terbitan-iab/buku-terbitan-iab">Buku Terbitan IAB </a>

</li></ul></li><li itemprop="name" class="mega-group">
<span> Muat Turun</span>


<ul class="level2 dropdown-menu"><li itemprop="name">
<a itemprop="url" href="/index.php/ms/sumber/muat-turun/borang-pentadbiran">Borang Pentadbiran </a>

</li><li itemprop="name">
<a itemprop="url" href="/index.php/ms/sumber/muat-turun/manual">Manual Pengguna </a>

</li><li itemprop="name">
<a itemprop="url" href="/index.php/ms/sumber/muat-turun/garis-panduan">Garis Panduan </a>

</li><li itemprop="name">
<a itemprop="url" href="/index.php/ms/sumber/muat-turun/m-pik-iab" target="_blank">Pelan Induk Kecemerlangan (PIK) </a>

</li></ul></li><li itemprop="name" class="mega-group">
<span> Galeri Multimedia</span>


<ul class="level2 dropdown-menu"><li itemprop="name">
<a itemprop="url" href="/index.php/ms/sumber/galeri-multimedia/anugerah-pekerja-cemerlang-apc">Anugerah Pekerja Cemerlang (APC) </a>

</li><li itemprop="name">
<a itemprop="url" href="/index.php/ms/sumber/galeri-multimedia/anugerah-pekerja-terbaik-iab">Anugerah Pekerja Terbaik IAB </a>

</li><li itemprop="name">
<a itemprop="url" href="/index.php/ms/sumber/galeri-multimedia/g-gamber">Galeri Gambar </a>

</li><li itemprop="name">
<a itemprop="url" href="/index.php/ms/sumber/galeri-multimedia/galeri-audio">Galeri Audio </a>

</li><li itemprop="name">
<a itemprop="url" href="/index.php/ms/sumber/galeri-multimedia/galeri-video-main">Galeri Video </a>

</li><li itemprop="name">
<a itemprop="url" href="/index.php/ms/sumber/galeri-multimedia/g-info-grafik">Info grafik </a>

</li></ul></li><li itemprop="name" class="mega-group">
<span> Takwim</span>


<ul class="level2 dropdown-menu"><li itemprop="name">
<a itemprop="url" href="http://sistem.iab.edu.my/insystv2/ekiosk/takwim_iab_tahunan_view.php" target="_blank">Tahunan</a>

</li><li itemprop="name">
<a itemprop="url" href="http://sistem.iab.edu.my/insystv2/ekiosk/takwim_iab_semasa_view.php" target="_blank">Semasa</a>

</li></ul></li><li itemprop="name">
<a itemprop="url" href="/index.php/ms/sumber/kolokiumpcip2021">Kolokium PCIP Tahun 2021 </a>

</li></ul></li>
</ul></div>
					
					<div class="t3-navbar navbar-collapse collapse">
						<div class="t3-megamenu animate zoom" data-duration="400" data-responsive="true">
<ul itemscope="" itemtype="http://www.schema.org/SiteNavigationElement" class="nav navbar-nav level0">
<li itemprop="name" class="current active" data-id="101" data-level="1">
<a itemprop="url" class="" href="/index.php/ms/" data-target="#">Laman Utama </a>

</li>
<li itemprop="name" class="dropdown mega mega-align-center" data-id="312" data-level="1" data-alignsub="center">
<span class=" dropdown-toggle separator" data-target="#" data-toggle="dropdown"> Kenali IAB<em class="caret"></em></span>

<div class="nav-child dropdown-menu mega-dropdown-menu" style="width: 900px" data-width="900"><div class="mega-dropdown-inner">
<div class="row">
<div class="col-xs-3 mega-col-nav" data-width="3"><div class="mega-inner">
<ul itemscope="" itemtype="http://www.schema.org/SiteNavigationElement" class="mega-nav level1">
<li itemprop="name" data-id="280" data-level="2">
<a itemprop="url" class="" href="/index.php/ms/ttgiab/tentang-iab" data-target="#">Korporat </a>

</li>
<li itemprop="name" data-id="315" data-level="2">
<a itemprop="url" class="" href="/index.php/ms/ttgiab/carta-organisasi" data-target="#">Carta Organisasi </a>

</li>
<li itemprop="name" data-id="319" data-level="2">
<a itemprop="url" class="" href="/index.php/ms/ttgiab/men-pependaf" data-target="#">Pejabat Pendaftar </a>

</li>
</ul>
</div></div>
<div class="col-xs-3 mega-col-nav" data-width="3"><div class="mega-inner">
<ul itemscope="" itemtype="http://www.schema.org/SiteNavigationElement" class="mega-nav level1">
<li itemprop="name" class="mega mega-group" data-id="320" data-level="2" data-group="1">
<span class=" dropdown-header mega-group-title separator" data-target="#"> Kluster Khidmat Profesional</span>

<div class="nav-child mega-group-ct"><div class="mega-dropdown-inner">
<div class="row">
<div class="col-xs-12 mega-col-nav" data-width="12"><div class="mega-inner">
<ul itemscope="" itemtype="http://www.schema.org/SiteNavigationElement" class="mega-nav level2">
<li itemprop="name" data-id="323" data-level="3">
<a itemprop="url" class="" href="/index.php/ms/ttgiab/kluster-khidmat-profesional/pusat-pengurusan-dasar-dan-inovasi" data-target="#">Pusat Pengurusan Dasar dan Inovasi </a>

</li>
<li itemprop="name" data-id="325" data-level="3">
<a itemprop="url" class="" href="/index.php/ms/ttgiab/kluster-khidmat-profesional/pusat-pentaksiran-dan-pembangunan-bakat" data-target="#">Pusat Pentaksiran dan Pembangunan Bakat </a>

</li>
<li itemprop="name" data-id="326" data-level="3">
<a itemprop="url" class="" href="/index.php/ms/ttgiab/kluster-khidmat-profesional/pusat-pembangunan-dan-pengurusan-teknologi" data-target="#">Pusat Pembangunan dan Pengurusan Teknologi </a>

</li>
<li itemprop="name" data-id="322" data-level="3">
<a itemprop="url" class="" href="/index.php/ms/ttgiab/kluster-khidmat-profesional/pusat-dokumentasi-dan-sumber-pendidikan" data-target="#">Pusat Dokumentasi dan Sumber Pendidikan </a>

</li>
</ul>
</div></div>
</div>
</div></div>
</li>
</ul>
</div></div>
<div class="col-xs-3 mega-col-nav" data-width="3"><div class="mega-inner">
<ul itemscope="" itemtype="http://www.schema.org/SiteNavigationElement" class="mega-nav level1">
<li itemprop="name" class="mega mega-group" data-id="321" data-level="2" data-group="1">
<span class=" dropdown-header mega-group-title separator" data-target="#"> Kluster Khidmat Latihan</span>

<div class="nav-child mega-group-ct"><div class="mega-dropdown-inner">
<div class="row">
<div class="col-xs-12 mega-col-nav" data-width="12"><div class="mega-inner">
<ul itemscope="" itemtype="http://www.schema.org/SiteNavigationElement" class="mega-nav level2">
<li itemprop="name" data-id="327" data-level="3">
<a itemprop="url" class="" href="/index.php/ms/ttgiab/kluster-khidmat-latihan/pusat-penyelidikan-dan-penilaian" data-target="#">Pusat Penyelidikan dan Penilaian </a>

</li>
<li itemprop="name" data-id="329" data-level="3">
<a itemprop="url" class="" href="/index.php/ms/ttgiab/kluster-khidmat-latihan/pusat-pembangunan-kepimpinan-pendidikan" data-target="#">Pusat Pembangunan Kepimpinan Pendidikan </a>

</li>
<li itemprop="name" data-id="328" data-level="3">
<a itemprop="url" class="" href="/index.php/ms/ttgiab/kluster-khidmat-latihan/pusat-pengurusan-dan-pentadbiran-pendidikan" data-target="#">Pusat Pengurusan dan Pentadbiran Pendidikan </a>

</li>
<li itemprop="name" data-id="330" data-level="3">
<a itemprop="url" class="" href="/index.php/ms/ttgiab/kluster-khidmat-latihan/pusat-konsultasi-dan-pembangunan-organisasi-pendidikan" data-target="#">Pusat Konsultasi dan Pembangunan Organisasi Pendidikan </a>

</li>
<li itemprop="name" data-id="324" data-level="3">
<a itemprop="url" class="" href="/index.php/ms/ttgiab/kluster-khidmat-latihan/pusat-pembangunan-komuniti" data-target="#">Pusat Pembangunan Komuniti </a>

</li>
</ul>
</div></div>
</div>
</div></div>
</li>
</ul>
</div></div>
<div class="col-xs-3 mega-col-nav" data-width="3"><div class="mega-inner">
<ul itemscope="" itemtype="http://www.schema.org/SiteNavigationElement" class="mega-nav level1">
<li itemprop="name" class="mega mega-group" data-id="441" data-level="2" data-group="1">
<span class=" dropdown-header mega-group-title separator" data-target="#"> IAB Cawangan</span>

<div class="nav-child mega-group-ct"><div class="mega-dropdown-inner">
<div class="row">
<div class="col-xs-12 mega-col-nav" data-width="12"><div class="mega-inner">
<ul itemscope="" itemtype="http://www.schema.org/SiteNavigationElement" class="mega-nav level2">
<li itemprop="name" data-id="442" data-level="3">
<a itemprop="url" class="" href="/index.php/ms/ttgiab/iab-cawangan/iab-cawangan-genting-highlands" data-target="#">IAB Cawangan Genting Highlands </a>

</li>
<li itemprop="name" data-id="456" data-level="3">
<a itemprop="url" class="" href="/index.php/ms/ttgiab/iab-cawangan/iab-cawangan-utara" data-target="#">IAB Cawangan Utara </a>

</li>
<li itemprop="name" data-id="457" data-level="3">
<a itemprop="url" class="" href="/index.php/ms/ttgiab/iab-cawangan/iab-cawangan-sabah" data-target="#">IAB Cawangan Sabah </a>

</li>
<li itemprop="name" data-id="458" data-level="3">
<a itemprop="url" class="" href="/index.php/ms/ttgiab/iab-cawangan/iab-cawangan-sarawak-baru" data-target="#">IAB Cawangan Sarawak </a>

</li>
</ul>
</div></div>
</div>
</div></div>
</li>
</ul>
</div></div>
</div>
</div></div>
</li>
<li itemprop="name" class="dropdown mega mega-align-center" data-id="131" data-level="1" data-alignsub="center">
<span class=" dropdown-toggle separator" data-target="#" data-toggle="dropdown"> Perkhidmatan<em class="caret"></em></span>

<div class="nav-child dropdown-menu mega-dropdown-menu" style="width: 1300px" data-width="1300"><div class="mega-dropdown-inner">
<div class="row">
<div class="col-xs-4 mega-col-nav" data-width="4"><div class="mega-inner">
<ul itemscope="" itemtype="http://www.schema.org/SiteNavigationElement" class="mega-nav level1">
<li itemprop="name" class="mega mega-group" data-id="132" data-level="2" data-group="1">
<span class=" dropdown-header mega-group-title separator" data-target="#"> Aplikasi Dalam Talian</span>

<div class="nav-child mega-group-ct"><div class="mega-dropdown-inner">
<div class="row">
<div class="col-xs-12 mega-col-nav" data-width="12"><div class="mega-inner">
<ul itemscope="" itemtype="http://www.schema.org/SiteNavigationElement" class="mega-nav level2">
<li itemprop="name" data-id="415" data-level="3">
<a itemprop="url" class="" href="https://sistem.iab.edu.my/insystv2/" target="_blank" data-target="#">Sistem Bersepadu IAB (Insyst)</a>

</li>
<li itemprop="name" data-id="1128" data-level="3">
<a itemprop="url" class="" href="https://sistem.iab.edu.my/ekossmik/login.php" target="_blank" data-target="#">Sistem Konsultasi Sehenti Sentral Maya Integrasi Kepimpinan (KoSSMIK)</a>

</li>
<li itemprop="name" data-id="416" data-level="3">
<a itemprop="url" class="" href="https://sistem.iab.edu.my/insyst/main/" target="_blank" data-target="#">Permohonan Kursus</a>

</li>
<li itemprop="name" data-id="417" data-level="3">
<a itemprop="url" class="" href="https://sistem.iab.edu.my/insyst/main/" target="_blank" data-target="#">Profil Staf/Peserta</a>

</li>
<li itemprop="name" data-id="879" data-level="3">
<a itemprop="url" class="" href="https://sistem.iab.edu.my/insystv2/modul-sijil-online/cetak_sijilv2.php" target="_blank" data-target="#">Cetakan Sijil dalam Talian (Tahun Semasa)</a>

</li>
<li itemprop="name" data-id="905" data-level="3">
<a itemprop="url" class="" href="https://sistem.iab.edu.my/uppv2/modul-sijil-online/cetakan_sijilonline_view.php" target="_blank" data-target="#">Cetakan Sijil dalam Talian (Data Arkib)</a>

</li>
<li itemprop="name" data-id="1092" data-level="3">
<a itemprop="url" class="" href="https://sistem.iab.edu.my/insystv2/login/" target="_blank" data-target="#">i-KOMPAS 2.0</a>

</li>
</ul>
</div></div>
</div>
</div></div>
</li>
</ul>
</div></div>
<div class="col-xs-4 mega-col-nav" data-width="4"><div class="mega-inner">
<ul itemscope="" itemtype="http://www.schema.org/SiteNavigationElement" class="mega-nav level1">
<li itemprop="name" class="mega mega-group" data-id="880" data-level="2" data-group="1">
<span class=" dropdown-header mega-group-title separator" data-target="#"> Aplikasi Dalam Talian KPM</span>

<div class="nav-child mega-group-ct"><div class="mega-dropdown-inner">
<div class="row">
<div class="col-xs-12 mega-col-nav" data-width="12"><div class="mega-inner">
<ul itemscope="" itemtype="http://www.schema.org/SiteNavigationElement" class="mega-nav level2">
<li itemprop="name" data-id="517" data-level="3">
<a itemprop="url" class="" href="https://tls.moe.gov.my/login.php" target="_blank" data-target="#">Program Kelayakan Profesional Pemimpin Pendidikan Kebangsaan Modul Penggantian Pemimpin Sekolah (NPQEL MPPS)</a>

</li>
<li itemprop="name" data-id="877" data-level="3">
<a itemprop="url" class="" href="https://sgmy.moe.gov.my/KPMXPERT/index.cfm" target="_blank" data-target="#">Direktori Kepakaran KPM</a>

</li>
<li itemprop="name" data-id="878" data-level="3">
<a itemprop="url" class="" href="/index.php/ms/pages/aplikasi-dalam-talian-2/direktori-kepakaran-2" data-target="#">Direktori Kepakaran IAB </a>

</li>
</ul>
</div></div>
</div>
</div></div>
</li>
</ul>
</div></div>
<div class="col-xs-4 mega-col-nav" data-width="4"><div class="mega-inner">
<ul itemscope="" itemtype="http://www.schema.org/SiteNavigationElement" class="mega-nav level1">
<li itemprop="name" class="mega mega-group" data-id="143" data-level="2" data-group="1">
<span class=" dropdown-header mega-group-title separator" data-target="#"> Instrumen Dalam Talian</span>

<div class="nav-child mega-group-ct"><div class="mega-dropdown-inner">
<div class="row">
<div class="col-xs-12 mega-col-nav" data-width="12"><div class="mega-inner">
<ul itemscope="" itemtype="http://www.schema.org/SiteNavigationElement" class="mega-nav level2">
<li itemprop="name" data-id="518" data-level="3">
<a itemprop="url" class="" href="https://leser.iab.edu.my/" target="_blank" data-target="#">Leadership Self Evaluation Rating (LeSER)</a>

</li>
</ul>
</div></div>
</div>
</div></div>
</li>
<li itemprop="name" class="mega mega-group" data-id="439" data-level="2" data-group="1">
<span class=" dropdown-header mega-group-title separator" data-target="#"> Laman Mini/Web</span>

<div class="nav-child mega-group-ct"><div class="mega-dropdown-inner">
<div class="row">
<div class="col-xs-12 mega-col-nav" data-width="12"><div class="mega-inner">
<ul itemscope="" itemtype="http://www.schema.org/SiteNavigationElement" class="mega-nav level2">
<li itemprop="name" data-id="454" data-level="3">
<a itemprop="url" class="" href="http://eprints.iab.edu.my/v2/" target="_blank" data-target="#">Repositori</a>

</li>
<li itemprop="name" data-id="455" data-level="3">
<a itemprop="url" class="" href="http://lib.iab.edu.my/" target="_blank" data-target="#">Portal Pusat Sumber IAB</a>

</li>
<li itemprop="name" data-id="440" data-level="3">
<a itemprop="url" class="" href="/index.php/ms/pages/menu-ap-web/sp-q-iab" data-target="#">Sistem Pengurusan Kualiti (SP-Q) </a>

</li>
<li itemprop="name" data-id="451" data-level="3">
<a itemprop="url" class="" href="/index.php/ms/pages/menu-ap-web/master-trainer" data-target="#">Program Master Trainer </a>

</li>
<li itemprop="name" data-id="452" data-level="3">
<a itemprop="url" class="" href="/index.php/ms/pages/menu-ap-web/kkriab-iab" data-target="#">Kelab Kebajikan dan Rekreasi IAB (KKRIAB) </a>

</li>
<li itemprop="name" data-id="453" data-level="3">
<a itemprop="url" class="" href="/index.php/ms/pages/menu-ap-web/puspanita-iab" data-target="#">Persatuan Suri dan Anggota Wanita Perkhidmatan Awam Malaysia (PUSPANITA) </a>

</li>
<li itemprop="name" data-id="1077" data-level="3">
<a itemprop="url" class="" href="/index.php/ms/pages/menu-ap-web/penyelidikan-iab-menu" data-target="#">Penyelidikan IAB </a>

</li>
<li itemprop="name" data-id="1289" data-level="3">
<a itemprop="url" class="" href="/index.php/ms/pages/menu-ap-web/mainmenu-jk-keselamatan" data-target="#">JK Keselamatan dan Kesihatan Pekerjaan IAB </a>

</li>
<li itemprop="name" data-id="1333" data-level="3">
<a itemprop="url" class="" href="/index.php/ms/pages/menu-ap-web/mainmenu-propeks" data-target="#">Program Pelonjakan Kepimpinan Sekolah (ProPeKS) </a>

</li>
</ul>
</div></div>
</div>
</div></div>
</li>
</ul>
</div></div>
</div>
</div></div>
</li>
<li itemprop="name" class="dropdown mega" data-id="136" data-level="1">
<span class=" dropdown-toggle separator" data-target="#" data-toggle="dropdown"> e-Pembelajaran<em class="caret"></em></span>

<div class="nav-child dropdown-menu mega-dropdown-menu"><div class="mega-dropdown-inner">
<div class="row">
<div class="col-xs-12 mega-col-nav" data-width="12"><div class="mega-inner">
<ul itemscope="" itemtype="http://www.schema.org/SiteNavigationElement" class="mega-nav level1">
<li itemprop="name" data-id="431" data-level="2">
<a itemprop="url" class="" href="http://cpd.iab.edu.my/" target="_blank" data-target="#">e-Pembelajaran Pembangunan Profesionalisme Berterusan (eP@CPD) </a>

</li>
<li itemprop="name" data-id="430" data-level="2">
<a itemprop="url" class="" href="http://npqel.iab.edu.my/" target="_blank" data-target="#">e-Pembelajaran Program Kelayakan Profesional Pemimpin Pendidikan Kebangsaan (eP@NPQEL)</a>

</li>
<li itemprop="name" data-id="520" data-level="2">
<a itemprop="url" class="" href="http://lcml.iab.edu.my/" target="_blank" data-target="#">e-Pembelajaran Kursus Kepimpinan Pemimpin Pertengahan (eP@LCML)</a>

</li>
<li itemprop="name" data-id="661" data-level="2">
<a itemprop="url" class="" href="https://esidang.iab.edu.my/" target="_blank" data-target="#">e-Sidang</a>

</li>
</ul>
</div></div>
</div>
</div></div>
</li>
<li itemprop="name" class="dropdown mega" data-id="204" data-level="1">
<a itemprop="url" class=" dropdown-toggle" href="#" data-target="#" data-toggle="dropdown">Hubungi Kami<em class="caret"></em></a>

<div class="nav-child dropdown-menu mega-dropdown-menu" style="width: 250px" data-width="250"><div class="mega-dropdown-inner">
<div class="row">
<div class="col-xs-12 mega-col-nav" data-width="12"><div class="mega-inner">
<ul itemscope="" itemtype="http://www.schema.org/SiteNavigationElement" class="mega-nav level1">
<li itemprop="name" data-id="721" data-level="2">
<a itemprop="url" class="" href="/index.php/ms/peta-lokasi-iab-induk" data-target="#">Peta Lokasi IAB Induk</a>

</li>
<li itemprop="name" data-id="521" data-level="2">
<a itemprop="url" class="" href="/index.php/ms/maklumat-iab-induk" data-target="#">Maklumat IAB Induk dan Cawangan</a>

</li>
<li itemprop="name" data-id="1297" data-level="2">
<a itemprop="url" class="" href="http://sistem2.iab.edu.my/idirektori/" target="_blank" data-target="#">Direktori Staf</a>

</li>
<li itemprop="name" data-id="523" data-level="2">
<a itemprop="url" class="" href="/index.php/ms/student/bantuan-teknikal" data-target="#">Bantuan Teknikal </a>

</li>
<li itemprop="name" data-id="639" data-level="2">
<a itemprop="url" class="" href="/index.php/ms/student/maklum-balas-menu-utama" data-target="#">Maklum Balas IAB </a>

</li>
<li itemprop="name" data-id="1014" data-level="2">
<a itemprop="url" class="" href="https://forms.gle/nNgi8yj3j8e1tr38A" target="_blank" data-target="#">Soal Selidik Pelawat Portal Rasmi IAB</a>

</li>
</ul>
</div></div>
</div>
</div></div>
</li>
<li itemprop="name" class="dropdown mega mega-align-center" data-id="288" data-level="1" data-alignsub="center">
<span class=" dropdown-toggle separator" data-target="#" data-toggle="dropdown"> Sumber<em class="caret"></em></span>

<div class="nav-child dropdown-menu mega-dropdown-menu" style="width: 800px" data-width="800"><div class="mega-dropdown-inner">
<div class="row">
<div class="col-xs-4 mega-col-nav" data-width="4"><div class="mega-inner">
<ul itemscope="" itemtype="http://www.schema.org/SiteNavigationElement" class="mega-nav level1">
<li itemprop="name" data-id="625" data-level="2">
<a itemprop="url" class="" href="/index.php/ms/sumber/menu-berita-pemberitahuan" data-target="#">Pengumuman </a>

</li>
<li itemprop="name" data-id="780" data-level="2">
<a itemprop="url" class="" href="/index.php/ms/sumber/menu-sebutharga-tender" data-target="#">Sebut harga/Tender </a>

</li>
<li itemprop="name" data-id="662" data-level="2">
<a itemprop="url" class="" href="/index.php/ms/sumber/menu-berita-aktiviti" data-target="#">Aktiviti IAB </a>

</li>
<li itemprop="name" data-id="512" data-level="2">
<a itemprop="url" class="" href="/index.php/ms/sumber/terma-rujukan-tor" data-target="#">Terma Rujukan (TOR) </a>

</li>
<li itemprop="name" data-id="664" data-level="2">
<a itemprop="url" class="" href="/index.php/ms/sumber/menu-kod-qr" data-target="#">Kod QR Portal IAB </a>

</li>
<li itemprop="name" class="mega mega-group" data-id="289" data-level="2" data-group="1">
<span class=" dropdown-header mega-group-title separator" data-target="#"> Terbitan Berkala</span>

<div class="nav-child mega-group-ct"><div class="mega-dropdown-inner">
<div class="row">
<div class="col-xs-12 mega-col-nav" data-width="12"><div class="mega-inner">
<ul itemscope="" itemtype="http://www.schema.org/SiteNavigationElement" class="mega-nav level2">
<li itemprop="name" data-id="446" data-level="3">
<a itemprop="url" class="" href="/index.php/ms/sumber/terbitan-berkala/laporan-tahunan" data-target="#">Laporan Tahunan </a>

</li>
<li itemprop="name" data-id="447" data-level="3">
<a itemprop="url" class="" href="https://iab.moe.edu.my/index.php/ms/program-latihan#program-latihan-2022" data-target="#">Program Latihan</a>

</li>
<li itemprop="name" data-id="448" data-level="3">
<a itemprop="url" class="" href="/index.php/ms/sumber/terbitan-berkala/jurnal-iab-terkini" data-target="#">Jurnal IAB </a>

</li>
</ul>
</div></div>
</div>
</div></div>
</li>
</ul>
</div></div>
<div class="col-xs-4 mega-col-nav" data-width="4"><div class="mega-inner">
<ul itemscope="" itemtype="http://www.schema.org/SiteNavigationElement" class="mega-nav level1">
<li itemprop="name" class="mega mega-group" data-id="294" data-level="2" data-group="1">
<span class=" dropdown-header mega-group-title separator" data-target="#"> Terbitan IAB</span>

<div class="nav-child mega-group-ct"><div class="mega-dropdown-inner">
<div class="row">
<div class="col-xs-12 mega-col-nav" data-width="12"><div class="mega-inner">
<ul itemscope="" itemtype="http://www.schema.org/SiteNavigationElement" class="mega-nav level2">
<li itemprop="name" data-id="450" data-level="3">
<a itemprop="url" class="" href="/index.php/ms/sumber/terbitan-iab/buku-terbitan-iab" data-target="#">Buku Terbitan IAB </a>

</li>
</ul>
</div></div>
</div>
</div></div>
</li>
<li itemprop="name" class="mega mega-group" data-id="496" data-level="2" data-group="1">
<span class=" dropdown-header mega-group-title separator" data-target="#"> Muat Turun</span>

<div class="nav-child mega-group-ct"><div class="mega-dropdown-inner">
<div class="row">
<div class="col-xs-12 mega-col-nav" data-width="12"><div class="mega-inner">
<ul itemscope="" itemtype="http://www.schema.org/SiteNavigationElement" class="mega-nav level2">
<li itemprop="name" data-id="498" data-level="3">
<a itemprop="url" class="" href="/index.php/ms/sumber/muat-turun/borang-pentadbiran" data-target="#">Borang Pentadbiran </a>

</li>
<li itemprop="name" data-id="499" data-level="3">
<a itemprop="url" class="" href="/index.php/ms/sumber/muat-turun/manual" data-target="#">Manual Pengguna </a>

</li>
<li itemprop="name" data-id="502" data-level="3">
<a itemprop="url" class="" href="/index.php/ms/sumber/muat-turun/garis-panduan" data-target="#">Garis Panduan </a>

</li>
<li itemprop="name" data-id="734" data-level="3">
<a itemprop="url" class="" href="/index.php/ms/sumber/muat-turun/m-pik-iab" target="_blank" data-target="#">Pelan Induk Kecemerlangan (PIK) </a>

</li>
</ul>
</div></div>
</div>
</div></div>
</li>
</ul>
</div></div>
<div class="col-xs-4 mega-col-nav" data-width="4"><div class="mega-inner">
<ul itemscope="" itemtype="http://www.schema.org/SiteNavigationElement" class="mega-nav level1">
<li itemprop="name" class="mega mega-group" data-id="505" data-level="2" data-group="1">
<span class=" dropdown-header mega-group-title separator" data-target="#"> Galeri Multimedia</span>

<div class="nav-child mega-group-ct"><div class="mega-dropdown-inner">
<div class="row">
<div class="col-xs-12 mega-col-nav" data-width="12"><div class="mega-inner">
<ul itemscope="" itemtype="http://www.schema.org/SiteNavigationElement" class="mega-nav level2">
<li itemprop="name" data-id="1098" data-level="3">
<a itemprop="url" class="" href="/index.php/ms/sumber/galeri-multimedia/anugerah-pekerja-cemerlang-apc" data-target="#">Anugerah Pekerja Cemerlang (APC) </a>

</li>
<li itemprop="name" data-id="1096" data-level="3">
<a itemprop="url" class="" href="/index.php/ms/sumber/galeri-multimedia/anugerah-pekerja-terbaik-iab" data-target="#">Anugerah Pekerja Terbaik IAB </a>

</li>
<li itemprop="name" data-id="791" data-level="3">
<a itemprop="url" class="" href="/index.php/ms/sumber/galeri-multimedia/g-gamber" data-target="#">Galeri Gambar </a>

</li>
<li itemprop="name" data-id="511" data-level="3">
<a itemprop="url" class="" href="/index.php/ms/sumber/galeri-multimedia/galeri-audio" data-target="#">Galeri Audio </a>

</li>
<li itemprop="name" data-id="509" data-level="3">
<a itemprop="url" class="" href="/index.php/ms/sumber/galeri-multimedia/galeri-video-main" data-target="#">Galeri Video </a>

</li>
<li itemprop="name" data-id="798" data-level="3">
<a itemprop="url" class="" href="/index.php/ms/sumber/galeri-multimedia/g-info-grafik" data-target="#">Info grafik </a>

</li>
</ul>
</div></div>
</div>
</div></div>
</li>
<li itemprop="name" class="mega mega-group" data-id="513" data-level="2" data-group="1">
<span class=" dropdown-header mega-group-title separator" data-target="#"> Takwim</span>

<div class="nav-child mega-group-ct"><div class="mega-dropdown-inner">
<div class="row">
<div class="col-xs-12 mega-col-nav" data-width="12"><div class="mega-inner">
<ul itemscope="" itemtype="http://www.schema.org/SiteNavigationElement" class="mega-nav level2">
<li itemprop="name" data-id="514" data-level="3">
<a itemprop="url" class="" href="http://sistem.iab.edu.my/insystv2/ekiosk/takwim_iab_tahunan_view.php" target="_blank" data-target="#">Tahunan</a>

</li>
<li itemprop="name" data-id="515" data-level="3">
<a itemprop="url" class="" href="http://sistem.iab.edu.my/insystv2/ekiosk/takwim_iab_semasa_view.php" target="_blank" data-target="#">Semasa</a>

</li>
</ul>
</div></div>
</div>
</div></div>
</li>
<li itemprop="name" data-id="1121" data-level="2">
<a itemprop="url" class="" href="/index.php/ms/sumber/kolokiumpcip2021" data-target="#">Kolokium PCIP Tahun 2021 </a>

</li>
</ul>
</div></div>
</div>
</div></div>
</li>
</ul>
</div>

					</div>

			</nav>
			<!-- //MAIN NAVIGATION -->

		</div>
	</div>
</header>
<!-- //HEADER -->


  


  
	<!-- masthead -->
	<div class="wrap t3-masthead ">
			<div class="moduletable"><div id="twojContentSliderId1" class=" squareArrows" style="max-height: none; overflow: visible;"><div id="twojContentSliderId1Inner" class="revslider-initialised tp-simpleresponsive hovered" style="max-height: none; height: 201px;"><ul class="tp-revslider-mainul" style="display: block; overflow: hidden; width: 100%; height: 100%; max-height: none;"><li data-transition="slotzoom-horizontal" data-masterspeed="300" data-slotamount="7" class="tp-revslider-slidesli active-revslide" style="width: 100%; height: 100%; overflow: hidden; z-index: 18; visibility: hidden; opacity: 0;">
		<div class="slotholder" style="width:100%;height:100%;" data-duration="undefined" data-zoomstart="undefined" data-zoomend="undefined" data-rotationstart="undefined" data-rotationend="undefined" data-ease="undefined" data-bgpositionend="undefined" data-bgposition="undefined" data-kenburns="undefined" data-easeme="undefined" data-bgfit="contain" data-bgfitend="undefined" data-owidth="undefined" data-oheight="undefined"><div class="tp-bgimg defaultimg" data-lazyload="https://iab.moe.edu.my/images/iklan/2023/Salam-Maulidur-Rasul.png" data-bgfit="contain" data-bgposition="center center" data-bgrepeat="no-repeat" data-lazydone="undefined" src="https://iab.moe.edu.my/images/iklan/2023/Salam-Maulidur-Rasul.png" data-src="https://iab.moe.edu.my/components/com_twojtoolbox/plugins/slideshow/1002/images/dummy.png" style="background-color: rgba(0, 0, 0, 0); background-repeat: no-repeat; background-size: contain; background-position: center center; width: 100%; height: 100%; background-image: url(&quot;https://iab.moe.edu.my/images/iklan/2023/Salam-Maulidur-Rasul.png&quot;); opacity: 0; visibility: hidden;"></div></div>
</li>
<li data-transition="zoomin" data-masterspeed="300" data-slotamount="7" class="tp-revslider-slidesli active-revslide" style="width: 100%; height: 100%; overflow: hidden; z-index: 18; visibility: hidden; opacity: 0;">
		<div class="slotholder" style="width:100%;height:100%;" data-duration="undefined" data-zoomstart="undefined" data-zoomend="undefined" data-rotationstart="undefined" data-rotationend="undefined" data-ease="undefined" data-bgpositionend="undefined" data-bgposition="undefined" data-kenburns="undefined" data-easeme="undefined" data-bgfit="contain" data-bgfitend="undefined" data-owidth="undefined" data-oheight="undefined"><div class="tp-bgimg defaultimg" data-lazyload="https://iab.moe.edu.my/images/iklan/2023/WEBINAR-HG-2023-1920--540px.png" data-bgfit="contain" data-bgposition="center center" data-bgrepeat="no-repeat" data-lazydone="undefined" src="https://iab.moe.edu.my/images/iklan/2023/WEBINAR-HG-2023-1920--540px.png" data-src="https://iab.moe.edu.my/components/com_twojtoolbox/plugins/slideshow/1002/images/dummy.png" style="background-color: rgba(0, 0, 0, 0); background-repeat: no-repeat; background-size: contain; background-position: center center; width: 100%; height: 100%; background-image: url(&quot;https://iab.moe.edu.my/images/iklan/2023/WEBINAR-HG-2023-1920--540px.png&quot;); opacity: 1; visibility: inherit;"></div></div>
</li>
<li data-transition="fade" data-masterspeed="300" data-slotamount="7" class="tp-revslider-slidesli active-revslide current-sr-slide-visible" style="width: 100%; height: 100%; overflow: hidden; z-index: 20; visibility: inherit; opacity: 1;">
		<div class="slotholder" style="width:100%;height:100%;" data-duration="undefined" data-zoomstart="undefined" data-zoomend="undefined" data-rotationstart="undefined" data-rotationend="undefined" data-ease="undefined" data-bgpositionend="undefined" data-bgposition="undefined" data-kenburns="undefined" data-easeme="undefined" data-bgfit="contain" data-bgfitend="undefined" data-owidth="undefined" data-oheight="undefined"><div class="tp-bgimg defaultimg" data-lazyload="https://iab.moe.edu.my/images/iklan/2023/Pro2023.png" data-bgfit="contain" data-bgposition="center center" data-bgrepeat="no-repeat" data-lazydone="undefined" src="https://iab.moe.edu.my/images/iklan/2023/Pro2023.png" data-src="https://iab.moe.edu.my/components/com_twojtoolbox/plugins/slideshow/1002/images/dummy.png" style="background-color: rgba(0, 0, 0, 0); background-repeat: no-repeat; background-size: contain; background-position: center center; width: 100%; height: 100%; background-image: url(&quot;https://iab.moe.edu.my/images/iklan/2023/Pro2023.png&quot;); visibility: inherit; opacity: 1;"></div></div>
</li>
<li data-transition="slotslide-horizontal" data-masterspeed="300" data-slotamount="7" class="tp-revslider-slidesli" style="width: 100%; height: 100%; overflow: hidden;">
		<div class="slotholder" style="width:100%;height:100%;" data-duration="undefined" data-zoomstart="undefined" data-zoomend="undefined" data-rotationstart="undefined" data-rotationend="undefined" data-ease="undefined" data-bgpositionend="undefined" data-bgposition="undefined" data-kenburns="undefined" data-easeme="undefined" data-bgfit="contain" data-bgfitend="undefined" data-owidth="undefined" data-oheight="undefined"><div class="tp-bgimg defaultimg" data-lazyload="https://iab.moe.edu.my/images/iklan/2022/BAnner-TAhniah-SIRIM-terkini.png" data-bgfit="contain" data-bgposition="center center" data-bgrepeat="no-repeat" data-lazydone="undefined" src="https://iab.moe.edu.my/components/com_twojtoolbox/plugins/slideshow/1002/images/dummy.png" data-src="https://iab.moe.edu.my/components/com_twojtoolbox/plugins/slideshow/1002/images/dummy.png" style="background-color:rgba(0, 0, 0, 0);background-repeat:no-repeat;background-image:url(https://iab.moe.edu.my/components/com_twojtoolbox/plugins/slideshow/1002/images/dummy.png);background-size:contain;background-position:center center;width:100%;height:100%;"></div></div>
</li>
<li data-transition="boxslide" data-masterspeed="300" data-link="/images/pengurus_kandungan/master_trainer/Promosi/Promosi PMT 2023 .pdf" data-target="_blank" data-slotamount="7" class="tp-revslider-slidesli" style="width: 100%; height: 100%; overflow: hidden;">
		<div class="slotholder" style="width:100%;height:100%;" data-duration="undefined" data-zoomstart="undefined" data-zoomend="undefined" data-rotationstart="undefined" data-rotationend="undefined" data-ease="undefined" data-bgpositionend="undefined" data-bgposition="undefined" data-kenburns="undefined" data-easeme="undefined" data-bgfit="contain" data-bgfitend="undefined" data-owidth="undefined" data-oheight="undefined"><div class="tp-bgimg defaultimg" data-lazyload="https://iab.moe.edu.my/images/iklan/2023/070223-Backdrop-Promosi-PMT-Portal-IAB-terkini.png" data-bgfit="contain" data-bgposition="center center" data-bgrepeat="no-repeat" data-lazydone="undefined" src="https://iab.moe.edu.my/components/com_twojtoolbox/plugins/slideshow/1002/images/dummy.png" data-src="https://iab.moe.edu.my/components/com_twojtoolbox/plugins/slideshow/1002/images/dummy.png" style="background-color:rgba(0, 0, 0, 0);background-repeat:no-repeat;background-image:url(https://iab.moe.edu.my/components/com_twojtoolbox/plugins/slideshow/1002/images/dummy.png);background-size:contain;background-position:center center;width:100%;height:100%;"></div></div>
<div class="tp-caption sft slidelink" style="width:100%;height:100%;z-index:60;" data-x="center" data-y="center" data-linktoslide="no" data-start="0"><a style="width:100%;height:100%;display:block" target="_blank" href="/images/pengurus_kandungan/master_trainer/Promosi/Promosi PMT 2023 .pdf"><span style="width:100%;height:100%;display:block"></span></a></div></li>
<li data-transition="slotfade-horizontal" data-masterspeed="300" data-link="https://iab.moe.edu.my/images/pemberitahuan/2022/Slaid%20NBLIAB%20_Portal%20IAB_checkedTP_penambahbaika%2027.6.22%20(1).pdf" data-target="_blank" data-slotamount="7" class="tp-revslider-slidesli" style="width: 100%; height: 100%; overflow: hidden;">
		<div class="slotholder" style="width:100%;height:100%;" data-duration="undefined" data-zoomstart="undefined" data-zoomend="undefined" data-rotationstart="undefined" data-rotationend="undefined" data-ease="undefined" data-bgpositionend="undefined" data-bgposition="undefined" data-kenburns="undefined" data-easeme="undefined" data-bgfit="contain" data-bgfitend="undefined" data-owidth="undefined" data-oheight="undefined"><div class="tp-bgimg defaultimg" data-lazyload="https://iab.moe.edu.my/images/iklan/2022/template-banner_NBLIAB-edited.png" data-bgfit="contain" data-bgposition="center center" data-bgrepeat="no-repeat" data-lazydone="undefined" src="https://iab.moe.edu.my/components/com_twojtoolbox/plugins/slideshow/1002/images/dummy.png" data-src="https://iab.moe.edu.my/components/com_twojtoolbox/plugins/slideshow/1002/images/dummy.png" style="background-color:rgba(0, 0, 0, 0);background-repeat:no-repeat;background-image:url(https://iab.moe.edu.my/components/com_twojtoolbox/plugins/slideshow/1002/images/dummy.png);background-size:contain;background-position:center center;width:100%;height:100%;"></div></div>
<div class="tp-caption sft slidelink" style="width:100%;height:100%;z-index:60;" data-x="center" data-y="center" data-linktoslide="no" data-start="0"><a style="width:100%;height:100%;display:block" target="_blank" href="https://iab.moe.edu.my/images/pemberitahuan/2022/Slaid%20NBLIAB%20_Portal%20IAB_checkedTP_penambahbaika%2027.6.22%20(1).pdf"><span style="width:100%;height:100%;display:block"></span></a></div></li>
<li data-transition="fade" data-masterspeed="300" data-slotamount="7" class="tp-revslider-slidesli" style="width: 100%; height: 100%; overflow: hidden;">
		<div class="slotholder" style="width:100%;height:100%;" data-duration="undefined" data-zoomstart="undefined" data-zoomend="undefined" data-rotationstart="undefined" data-rotationend="undefined" data-ease="undefined" data-bgpositionend="undefined" data-bgposition="undefined" data-kenburns="undefined" data-easeme="undefined" data-bgfit="contain" data-bgfitend="undefined" data-owidth="undefined" data-oheight="undefined"><div class="tp-bgimg defaultimg" data-lazyload="https://iab.moe.edu.my/images/iklan/2022/TatacaraPengurusanPakej17Mei22.png" data-bgfit="contain" data-bgposition="center center" data-bgrepeat="no-repeat" data-lazydone="undefined" src="https://iab.moe.edu.my/components/com_twojtoolbox/plugins/slideshow/1002/images/dummy.png" data-src="https://iab.moe.edu.my/components/com_twojtoolbox/plugins/slideshow/1002/images/dummy.png" style="background-color:rgba(0, 0, 0, 0);background-repeat:no-repeat;background-image:url(https://iab.moe.edu.my/components/com_twojtoolbox/plugins/slideshow/1002/images/dummy.png);background-size:contain;background-position:center center;width:100%;height:100%;"></div></div>
</li>
<li data-transition="zoomin" data-masterspeed="300" data-slotamount="7" class="tp-revslider-slidesli" style="width: 100%; height: 100%; overflow: hidden;">
		<div class="slotholder" style="width:100%;height:100%;" data-duration="undefined" data-zoomstart="undefined" data-zoomend="undefined" data-rotationstart="undefined" data-rotationend="undefined" data-ease="undefined" data-bgpositionend="undefined" data-bgposition="undefined" data-kenburns="undefined" data-easeme="undefined" data-bgfit="contain" data-bgfitend="undefined" data-owidth="undefined" data-oheight="undefined"><div class="tp-bgimg defaultimg" data-lazyload="https://iab.moe.edu.my/images/iklan/2022/HebahanMedsos22.png" data-bgfit="contain" data-bgposition="center center" data-bgrepeat="no-repeat" data-lazydone="undefined" src="https://iab.moe.edu.my/components/com_twojtoolbox/plugins/slideshow/1002/images/dummy.png" data-src="https://iab.moe.edu.my/components/com_twojtoolbox/plugins/slideshow/1002/images/dummy.png" style="background-color:rgba(0, 0, 0, 0);background-repeat:no-repeat;background-image:url(https://iab.moe.edu.my/components/com_twojtoolbox/plugins/slideshow/1002/images/dummy.png);background-size:contain;background-position:center center;width:100%;height:100%;"></div></div>
</li>
<li data-transition="slotfade-horizontal" data-masterspeed="300" data-slotamount="7" class="tp-revslider-slidesli" style="width: 100%; height: 100%; overflow: hidden;">
		<div class="slotholder" style="width:100%;height:100%;" data-duration="undefined" data-zoomstart="undefined" data-zoomend="undefined" data-rotationstart="undefined" data-rotationend="undefined" data-ease="undefined" data-bgpositionend="undefined" data-bgposition="undefined" data-kenburns="undefined" data-easeme="undefined" data-bgfit="contain" data-bgfitend="undefined" data-owidth="undefined" data-oheight="undefined"><div class="tp-bgimg defaultimg" data-lazyload="https://iab.moe.edu.my/images/iklan/2021/INFO BANNER KPO.jpg" data-bgfit="contain" data-bgposition="center center" data-bgrepeat="no-repeat" data-lazydone="undefined" src="https://iab.moe.edu.my/components/com_twojtoolbox/plugins/slideshow/1002/images/dummy.png" data-src="https://iab.moe.edu.my/components/com_twojtoolbox/plugins/slideshow/1002/images/dummy.png" style="background-color:rgba(0, 0, 0, 0);background-repeat:no-repeat;background-image:url(https://iab.moe.edu.my/components/com_twojtoolbox/plugins/slideshow/1002/images/dummy.png);background-size:contain;background-position:center center;width:100%;height:100%;"></div></div>
</li>
<li data-transition="fade" data-masterspeed="300" data-slotamount="7" class="tp-revslider-slidesli" style="width: 100%; height: 100%; overflow: hidden;">
		<div class="slotholder" style="width:100%;height:100%;" data-duration="undefined" data-zoomstart="undefined" data-zoomend="undefined" data-rotationstart="undefined" data-rotationend="undefined" data-ease="undefined" data-bgpositionend="undefined" data-bgposition="undefined" data-kenburns="undefined" data-easeme="undefined" data-bgfit="contain" data-bgfitend="undefined" data-owidth="undefined" data-oheight="undefined"><div class="tp-bgimg defaultimg" data-lazyload="https://iab.moe.edu.my/images/iklan/2022/BANNER-PROMOSI-KoSSMIK-JUN-2022-logo-baru.png" data-bgfit="contain" data-bgposition="center center" data-bgrepeat="no-repeat" data-lazydone="undefined" src="https://iab.moe.edu.my/components/com_twojtoolbox/plugins/slideshow/1002/images/dummy.png" data-src="https://iab.moe.edu.my/components/com_twojtoolbox/plugins/slideshow/1002/images/dummy.png" style="background-color:rgba(0, 0, 0, 0);background-repeat:no-repeat;background-image:url(https://iab.moe.edu.my/components/com_twojtoolbox/plugins/slideshow/1002/images/dummy.png);background-size:contain;background-position:center center;width:100%;height:100%;"></div></div>
</li>
<li data-transition="curtain-2" data-masterspeed="300" data-link="http://iab.moe.edu.my/index.php/ms/pages/aplikasi-dalam-talian-2/direktori-kepakaran-2" data-slotamount="7" class="tp-revslider-slidesli" style="width: 100%; height: 100%; overflow: hidden; visibility: hidden; opacity: 0; z-index: 18;">
		<div class="slotholder" style="width:100%;height:100%;" data-duration="undefined" data-zoomstart="undefined" data-zoomend="undefined" data-rotationstart="undefined" data-rotationend="undefined" data-ease="undefined" data-bgpositionend="undefined" data-bgposition="undefined" data-kenburns="undefined" data-easeme="undefined" data-bgfit="contain" data-bgfitend="undefined" data-owidth="undefined" data-oheight="undefined"><div class="tp-bgimg defaultimg" data-lazyload="https://iab.moe.edu.my/images/iklan/2022/Sijil22.png" data-bgfit="contain" data-bgposition="center center" data-bgrepeat="no-repeat" data-lazydone="undefined" src="https://iab.moe.edu.my/components/com_twojtoolbox/plugins/slideshow/1002/images/dummy.png" data-src="https://iab.moe.edu.my/components/com_twojtoolbox/plugins/slideshow/1002/images/dummy.png" style="background-color: rgba(0, 0, 0, 0); background-repeat: no-repeat; background-image: url(&quot;https://iab.moe.edu.my/components/com_twojtoolbox/plugins/slideshow/1002/images/dummy.png&quot;); background-size: contain; background-position: center center; width: 100%; height: 100%; opacity: 0;"></div></div>
<div class="tp-caption sft slidelink" style="width:100%;height:100%;z-index:60;" data-x="center" data-y="center" data-linktoslide="no" data-start="0"><a style="width:100%;height:100%;display:block" target="_self" href="http://iab.moe.edu.my/index.php/ms/pages/aplikasi-dalam-talian-2/direktori-kepakaran-2"><span style="width:100%;height:100%;display:block"></span></a></div></li>
</ul><div class="tp-loader spinner0" style="display: none;"><div class="dot1"></div><div class="dot2"></div><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div><div class="tp-bannertimer" style="width: 0%; transform: matrix(1, 0, 0, 1, 0, 0); visibility: hidden;"></div></div><div class="tp-bullets simplebullets square" style="bottom: 10px; left: 10px;"><div class="bullet first"></div><div class="bullet"></div><div class="bullet selected"></div><div class="bullet"></div><div class="bullet"></div><div class="bullet"></div><div class="bullet"></div><div class="bullet"></div><div class="bullet"></div><div class="bullet"></div><div class="bullet last"></div><div class="tpclear"></div></div><div style="position: absolute; margin-top: -8px; left: 10px; top: 100px;" class="tp-leftarrow tparrows default square"><div class="tp-arr-allwrapper"><div class="tp-arr-iwrapper"><div class="tp-arr-imgholder" style="visibility: inherit; opacity: 1; background-image: url(&quot;https://iab.moe.edu.my/images/iklan/2023/WEBINAR-HG-2023-1920--540px.png&quot;);"></div><div class="tp-arr-imgholder2"></div><div class="tp-arr-titleholder"></div><div class="tp-arr-subtitleholder"></div></div></div></div><div style="position: absolute; margin-top: -8px; right: 10px; top: 100px;" class="tp-rightarrow tparrows default square"><div class="tp-arr-allwrapper"><div class="tp-arr-iwrapper"><div class="tp-arr-imgholder" style="visibility: inherit; opacity: 1; background-image: url(&quot;https://iab.moe.edu.my/components/com_twojtoolbox/plugins/slideshow/1002/images/dummy.png&quot;);"></div><div class="tp-arr-imgholder2"></div><div class="tp-arr-titleholder"></div><div class="tp-arr-subtitleholder"></div></div></div></div></div><script type="text/javascript">emsajax("head").append("<style  type='text/css'>#twojContentSliderId1{ width: 100%; position:relative; padding:0; } #twojContentSliderId1Inner{ width:100%; position:relative; } </style>");emsajax(document).ready(function(){emsajax("#twojContentSliderId1Inner").revolution({ startwidth:  1920 ,startheight: 540 ,fullWidth:  'on' ,forceFullWidth:  'off' ,autoHeight:   'on' ,shadow: 0 ,hideTimerBar: "on" ,delay: 9000 ,onHoverStop: 'on' ,touchenabled: 'on' ,keyboardNavigation: 'on' ,hideArrowsOnMobile: 'off' ,navigationArrows: 'solo' ,soloArrowLeftHalign:  'left' ,soloArrowLeftValign:  'center' ,soloArrowLeftHOffset:  10 ,soloArrowLeftVOffset:  10 ,soloArrowRightHalign:  'right' ,soloArrowRightValign:  'center' ,soloArrowRightHOffset:  10 ,soloArrowRightVOffset:  10 ,navigationType: 'bullet' ,hideBulletsOnMobile: 'on' ,navigationStyle: 'square' ,navigationHAlign:  'left' ,navigationVAlign:  'bottom' ,navigationHOffset:  10 ,navigationVOffset:  10});});</script></div>
	</div>
	<!-- //masthead -->


	
	<!-- SECTIONS -->
	<div class="wrap t3-sections ">
					<div class="moduletable spotlight-bottom">
						
<div class="acm-spotlight ">
	<div class="container">	<div class="row">
		<div class="col col-xs-12 col-sm-12 col-md-4 col-lg-4">
		<div class="module-wrap">
			

<div class="custom">
	

<!-- START: Tabs -->
<div class="rl_tabs nn_tabs outline_handles outline_content top align_left has_effects" role="presentation">
<!--googleoff: index-->
<a id="rl_tabs-scrollto_1" class="anchor rl_tabs-scroll nn_tabs-scroll"></a>
<ul class="nav nav-tabs" id="set-rl_tabs-1" role="tablist">
<li class="rl_tabs-tab nn_tabs-tab nav-item active" role="heading"><a href="#pengumuman" class="rl_tabs-toggle nn_tabs-toggle nav-link" id="tab-pengumuman" data-toggle="tab" data-id="pengumuman" role="tab" aria-controls="pengumuman" aria-selected="true" style="height: auto;"><span class="rl_tabs-toggle-inner nn_tabs-toggle-inner">Pengumuman</span></a></li>
<li class="rl_tabs-tab nn_tabs-tab nav-item" role="heading"><a href="#sebut-harga-tender" class="rl_tabs-toggle nn_tabs-toggle nav-link" id="tab-sebut-harga-tender" data-toggle="tab" data-id="sebut-harga-tender" role="tab" aria-controls="sebut-harga-tender" aria-selected="false" style="height: auto;"><span class="rl_tabs-toggle-inner nn_tabs-toggle-inner">Sebut harga/Tender</span></a></li>
</ul>
<!--googleon: index-->
<div class="tab-content">
<div class="tab-pane rl_tabs-pane nn_tabs-pane active" id="pengumuman" role="tabpanel" aria-labelledby="tab-pengumuman" aria-hidden="false">
<h2 class="rl_tabs-title nn_tabs-title"><a id="anchor-pengumuman" class="anchor"></a>Pengumuman</h2>
<!-- START: Modules Anywhere --><div class="latestnews recently-list ">
          
      <div class="item clearfix">
            <div class="pull-left item-image">
      <a href="/index.php/ms/sumber/menu-berita-pemberitahuan/1644-webinar-kepimpinan-pendidikan-human-governance-dalam-pengurusan-pembelajaran-murid-pada-29-september-2023"><img src="/images/pemberitahuan/2023/WEBINAR-HG-2023-Instagram-Post-Square.png" alt="" itemprop="thumbnailUrl"></a>
    </div>
        <div class="content-item">
          <h4>
            <a href="/index.php/ms/sumber/menu-berita-pemberitahuan/1644-webinar-kepimpinan-pendidikan-human-governance-dalam-pengurusan-pembelajaran-murid-pada-29-september-2023" itemprop="url">Webinar Kepimpinan Pendidikan: Human Governance Dalam Pengurusan Pembelajaran Murid Pada 29 September 2023 </a>
          </h4>
          <div class="content-meta">
            <span datetime="2023-09-27T10:17:47+08:00" itemprop="dateModified">
                27 September 2023            </span>
          </div>
        </div>
      </div>
          
      <div class="item clearfix">
                <div class="content-item">
          <h4>
            <a href="/index.php/ms/sumber/menu-berita-pemberitahuan/1627-kemudahan-rujukan-perpustakaan-aminuddin-baki-institut-aminuddin-baki-induk-iabi-terkini" itemprop="url">Kemudahan Rujukan Perpustakaan, Aminuddin Baki Institut Aminuddin Baki Induk (IABI)</a>
          </h4>
          <div class="content-meta">
            <span datetime="2023-07-18T15:36:51+08:00" itemprop="dateModified">
                18 Julai 2023            </span>
          </div>
        </div>
      </div>
          
      <div class="item clearfix">
            <div class="pull-left item-image">
      <a href="/index.php/ms/sumber/menu-berita-pemberitahuan/1626-adiwarna-bahasa-edisi-1-tahun-2023"><img src="/images/pengurus_kandungan/pkomp/adiwarna/adiwarnabil12023.png" alt="" itemprop="thumbnailUrl"></a>
    </div>
        <div class="content-item">
          <h4>
            <a href="/index.php/ms/sumber/menu-berita-pemberitahuan/1626-adiwarna-bahasa-edisi-1-tahun-2023" itemprop="url">Hebahan Adiwarna Bahasa Bilangan 1 Tahun 2023</a>
          </h4>
          <div class="content-meta">
            <span datetime="2023-07-17T11:57:26+08:00" itemprop="dateModified">
                17 Julai 2023            </span>
          </div>
        </div>
      </div>
          
      <div class="item clearfix">
                <div class="content-item">
          <h4>
            <a href="/index.php/ms/sumber/menu-berita-pemberitahuan/1611-program-pelonjakan-kepimpinan-sekolah-propeks" itemprop="url">Program Pelonjakan Kepimpinan Sekolah (ProPeKS)</a>
          </h4>
          <div class="content-meta">
            <span datetime="2023-06-23T17:07:01+08:00" itemprop="dateModified">
                23 Jun 2023            </span>
          </div>
        </div>
      </div>
          
      <div class="item clearfix">
                <div class="content-item">
          <h4>
            <a href="/index.php/ms/sumber/menu-berita-pemberitahuan/1515-adiwarna-bahasa-edisi-2tahun-2023" itemprop="url">Adiwarna Bahasa Bilangan 2 Tahun 2022</a>
          </h4>
          <div class="content-meta">
            <span datetime="2022-12-30T12:17:55+08:00" itemprop="dateModified">
                30 Disember 2022            </span>
          </div>
        </div>
      </div>
    </div>
<!-- END: Modules Anywhere -->
<p style="text-align: right;"><span style="font-size: 8pt;"><strong><a href="/index.php/ms/sumber/menu-berita-pemberitahuan">Baca Selanjutnya &gt;&gt;</a></strong></span></p>
<p></p>


</div>
<div class="tab-pane rl_tabs-pane nn_tabs-pane" id="sebut-harga-tender" role="tabpanel" aria-labelledby="tab-sebut-harga-tender" aria-hidden="true">
<h2 class="rl_tabs-title nn_tabs-title"><a id="anchor-sebut-harga-tender" class="anchor"></a>Sebut harga/Tender</h2>
<!-- START: Modules Anywhere --><div class="latestnews recently-list ">
    </div>
<!-- END: Modules Anywhere -->
<p style="text-align: right;"><strong><span style="font-size: 8pt;"><a href="/index.php/ms/sumber/menu-sebutharga-tender">Baca Selanjutnya &gt;&gt;</a></span></strong></p>
</div></div></div><!-- END: Tabs -->
<p></p></div>
		<div class="moduletable">
							<h3>IAB Tag (Pautan Pantas)</h3>
						<div>
<canvas width="650" height="390" id="myCanvas1">
	<p>This Browser is not good enough to show HTML5 canvas. Switch to a better browser (Chrome, Firefox, IE9, Safari etc) to view the contect of this module properly</p>
	<ul id="taglist1" style="display: none;">
	<li><a href="http://sistem.iab.edu.my/insystv2/" target="_blank" style="color:#B20000; font-size:24pt;">InSYST</a></li>
<li><a href="http://sistem.iab.edu.my/insyst/main/" target="_blank" style="color:#B20059; font-size:20pt;">Permohonan Kursus</a></li>
<li><a href="http://sistem.iab.edu.my//insyst/main/" target="_blank" style="color:#A000B2; font-size:18pt;">Profil</a></li>
<li><a href="http://moe.spab.gov.my/eApps/system/index.do" target="_blank" style="color:#7900B2; font-size:16pt;">Sistem Pengurusan Aduan Awam (SISPAA)</a></li>
<li><a href="http://sistem.iab.edu.my/idirektori/" target="_blank" style="color:#5000B2; font-size:14pt;">Direktori Staf</a></li>
<li><a href="http://sistem.iab.edu.my/sppb/" target="_blank" style="color:#3BB2AB; font-size:14pt;">Sistem Permohonan Pensyarah Baharu (SPPB)</a></li>
<li><a href="https://tls.moe.gov.my/login.php" target="_blank" style="color:#3BB23B; font-size:14pt;">NPQEL MPPS</a></li>
<li><a href="http://esidang.iab.edu.my/" target="_blank" style="color:#B2AB3B; font-size:14pt;">eSidang</a></li>
<li><a href="http://cpd.iab.edu.my/" target="_blank" style="color:#B27D3B; font-size:14pt;">eP@CPD</a></li>
<li><a href="/index.php/ms/ttgiab/iab-cawangan/iab-cawangan-utara" target="_blank" style="color:#000000; font-size:14pt;">IABCU</a></li>
<li><a href="/index.php/ms/ttgiab/iab-cawangan/iab-cawangan-genting-highlands" target="_blank" style="color:#000000; font-size:14pt;">IABCGH</a></li>
<li><a href="/index.php/ms/ttgiab/iab-cawangan/iab-cawangan-sabah" target="_blank" style="color:#000000; font-size:12pt;">IABCSBH</a></li>
<li><a href="/index.php/ms/ttgiab/iab-cawangan/iab-cawangan-sarawak" target="_blank" style="color:#000000; font-size:12pt;">IABCSWK</a></li>
<li><a href="http://lcml.iab.edu.my" target="_blank" style="color:#000000; font-size:12pt;">eP@LCML</a></li>
<li><a href="http://npqel.iab.edu.my/" target="_blank" style="color:#000000; font-size:12pt;">eP@NPQEL</a></li>
<li><a href="http://sistem.iab.edu.my/insystv2/" target="_blank" style="color:#000000; font-size:12pt;">i-Gerak</a></li>
	</ul>
</canvas>
	<div style="clear: both"></div>
</div>
<script type="text/javascript">
var oopts1 = {	
	textFont: '"Comic Sans MS", cursive, sans-serif',	
	// shape and speed parameters
	shape : "sphere",
		maxSpeed : 0.015,
	minSpeed : 0.01,
		reverse : true,
	
	// shadow and color parameters
	outlineColour : "#505099",
	outlineThickness : 1,
	shadow : "#000000",
	shadowBlur : 2,
	minBrightness : 0.3,
	zoom : 1.2,
	stretchX : 1,
	stretchY : 1,
	wheelZoom : true,
	initial : [0.1,0.1] // delete this line for stopping initial rotation
};

function srz_tag_resize_canvas1(){
	var newcanvwidth = document.getElementById('myCanvas1').parentNode.clientWidth;

	var newcanvheight = (newcanvwidth * 60)/100;
	console.log(newcanvheight);
	document.getElementById('myCanvas1').width = newcanvwidth;
	document.getElementById('myCanvas1').height = newcanvheight;
}
srz_tag_resize_canvas1();
window.addEventListener('resize', srz_tag_resize_canvas1);

if(window.jQuery){
	jQuery(document).ready(function() {
		TagCanvas.textColour = null;
		TagCanvas.weight = true;
		try {
			TagCanvas.Start('myCanvas1', 'taglist1' ,oopts1);
		} catch(e) {
			// something went wrong, hide the canvas container
			document.getElementById('myCanvasContainer').style.display = 'none';
		}
	});
}
else if(!window.MooTools){
	addLoadEvent(function() {
		TagCanvas.textColour = null;
		TagCanvas.weight = true;
		try {
			TagCanvas.Start('myCanvas1', 'taglist1' ,oopts1);
		} catch(e) {
			// something went wrong, hide the canvas container
			document.getElementById('myCanvasContainer').style.display = 'none';
		}
	});
}
else{
	window.addEvent('domready',function() {
		TagCanvas.textColour = null;
		TagCanvas.weight = true;
		try {
			TagCanvas.Start('myCanvas1', 'taglist1' ,oopts1);
		} catch(e) {
			// something went wrong, hide the canvas container
			document.getElementById('myCanvasContainer').style.display = 'none';
		}
	});
}
</script>		</div>
			</div>
	</div>
		<div class="col col-xs-12 col-sm-12 col-md-4 col-lg-4">
		<div class="module-wrap">
					<div class="moduletable">
							<h3>Berita IAB</h3>
						<div class="category-module category-events owl-carousel owl-theme" style="opacity: 1; display: block;">
			<div class="owl-wrapper-outer"><div class="owl-wrapper" style="width: 7968px; left: 0px; display: block; transition: all 0ms ease 0s; transform: translate3d(0px, 0px, 0px);"><div class="owl-item" style="width: 664px;"><div class="event-item">
						
							<div class="intro-image">
					<img src="/images/pemberitahuan/2023/randau_30Ogos_medsos.jpeg">
				</div>
			
							<h3 class="event-date">
					30 Ogos 2023				</h3>
			
							<h4 class="event-title ">
					<a href="/index.php/ms/events/1640-randau-kepemimpinan-iab-cawangan-sarawak-siri-7-2023">
						Randau Kepemimpinan IAB Cawangan Sarawak Siri 7/2023					</a>
				</h4>
			
			
							<div class="event-info">
											<span><i class="fa fa-clock-o"></i> 2:30 pm - 5:30 pm</span>
					
											<span class="intro-ctm-address">
							<i class="fa fa-map-marker"></i>Secara Dalam Talian (Youtube)						</span>
					
				</div>
			
			<!-- more info -->
			<div class="more-info">
				
				
				
				
							</div>
		</div></div><div class="owl-item" style="width: 664px;"><div class="event-item">
						
							<div class="intro-image">
					<img src="/images/pemberitahuan/2023/sabahmedos_3ogos.jpeg">
				</div>
			
							<h3 class="event-date">
					03 Ogos 2023				</h3>
			
							<h4 class="event-title ">
					<a href="/index.php/ms/events/1635-program-hari-bertemu-pelanggan-institut-aminuddin-baki-2023-bagi-zon-sabah-kementerian-pendidikan-malaysia-3-5-ogos-2023">
						Program Hari Bertemu Pelanggan Institut Aminuddin Baki 2023 bagi Zon Sabah Kementerian Pendidikan Malaysia (3 - 5 Ogos 2023)					</a>
				</h4>
			
			
							<div class="event-info">
											<span><i class="fa fa-clock-o"></i> 8:00 am - 5:00 pm</span>
					
											<span class="intro-ctm-address">
							<i class="fa fa-map-marker"></i>Sabah						</span>
					
				</div>
			
			<!-- more info -->
			<div class="more-info">
				
				
				
				
							</div>
		</div></div><div class="owl-item" style="width: 664px;"><div class="event-item">
						
							<div class="intro-image">
					<img src="/images/pemberitahuan/2023/randauterkini28julaimedsos.jpeg">
				</div>
			
							<h3 class="event-date">
					28 Julai 2023				</h3>
			
							<h4 class="event-title ">
					<a href="/index.php/ms/events/1634-randau-kepemimpinan-iab-cawangan-sarawak-siri-6-2023-coaching-apa-dan-bagaimana">
						Randau Kepemimpinan IAB Cawangan Sarawak Siri 6/2023 - 'Coaching': Apa dan bagaimana?					</a>
				</h4>
			
			
							<div class="event-info">
											<span><i class="fa fa-clock-o"></i> 2:30 pm - 4:30 pm</span>
					
											<span class="intro-ctm-address">
							<i class="fa fa-map-marker"></i>Secara dalam talian						</span>
					
				</div>
			
			<!-- more info -->
			<div class="more-info">
				
				
				
				
							</div>
		</div></div><div class="owl-item" style="width: 664px;"><div class="event-item">
						
							<div class="intro-image">
					<img src="/images/pemberitahuan/2023/posterSNmedsos.jpeg">
				</div>
			
							<h3 class="event-date">
					24 Julai 2023				</h3>
			
							<h4 class="event-title ">
					<a href="/index.php/ms/events/1628-seminar-nasional-pengurusan-dan-kepimpinan-pendidikan-ke-30-institut-aminuddin-baki">
						Seminar Nasional Pengurusan dan Kepimpinan Pendidikan ke-30 Institut Aminuddin Baki 					</a>
				</h4>
			
			
							<div class="event-info">
											<span><i class="fa fa-clock-o"></i> 8:00 am - 5:00 pm</span>
					
											<span class="intro-ctm-address">
							<i class="fa fa-map-marker"></i>Auditorium Perdana, Institut Aminuddin Baki, Bandar Enstek						</span>
					
				</div>
			
			<!-- more info -->
			<div class="more-info">
				
				
				
				
							</div>
		</div></div><div class="owl-item" style="width: 664px;"><div class="event-item">
						
							<div class="intro-image">
					<img src="/images/pemberitahuan/2023/RKS523_27Jun2023medsos.jpg">
				</div>
			
							<h3 class="event-date">
					27 Jun 2023				</h3>
			
							<h4 class="event-title ">
					<a href="/index.php/ms/events/1621-randau-kepemimpinan-iab-cawangan-sarawak-siri-5-2023">
						Randau Kepemimpinan IAB Cawangan Sarawak Siri 5/2023					</a>
				</h4>
			
			
							<div class="event-info">
											<span><i class="fa fa-clock-o"></i> 9:30 am - 1:00 pm</span>
					
					
				</div>
			
			<!-- more info -->
			<div class="more-info">
				
				
				
				
							</div>
		</div></div><div class="owl-item" style="width: 664px;"><div class="event-item">
						
							<div class="intro-image">
					<img src="/images/iklan/2023/webinar16junjumaat.png">
				</div>
			
							<h3 class="event-date">
					13 Jun 2023				</h3>
			
							<h4 class="event-title ">
					<a href="/index.php/ms/events/1620-13jun2023">
						Webinar Penilaian Bersepadu Pegawai Perkhidmatan Pendidikan					</a>
				</h4>
			
			
							<div class="event-info">
											<span><i class="fa fa-clock-o"></i> 2:15 pm - 5:00 pm</span>
					
					
				</div>
			
			<!-- more info -->
			<div class="more-info">
				
				
				
				
							</div>
		</div></div></div></div>
			
			
			
			
			
	<div class="owl-controls clickable"><div class="owl-buttons"><div class="owl-prev"><i class="fa fa-angle-left"></i></div><div class="owl-next"><i class="fa fa-angle-right"></i></div></div></div></div>

<script>
(function($){
  jQuery(document).ready(function($) {
    $(".category-events").owlCarousel({
      items: 1,
      singleItem : true,
      itemsScaleUp : true,
      navigation : true,
      navigationText : ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
      pagination: false,
      merge: false,
      mergeFit: true,
      slideBy: 1,
      autoplay: true,
    });
  });
})(jQuery);
</script>
		</div>
			<div class="moduletable">
						

<div class="custom">
	<p style="text-align: right;"><span style="font-size: 8pt;"><strong><a href="/index.php/ms/sumber/menu-berita-aktiviti">Baca Selanjutnya &gt;&gt;</a></strong></span></p></div>
		</div>
			<div class="moduletable">
						

<div class="custom">
	<p style="text-align: right;"><span style="font-size: 8pt;"><strong><a href="/index.php/ms/sumber/galeri-multimedia/anugerah-pekerja-cemerlang-apc">Selanjutnya &gt;&gt;</a></strong></span></p></div>
		</div>
			<div class="moduletable">
							<h3>Anugerah Pekerja Terbaik IAB</h3>
						
		<!--[if lte IE 7]>
		<link href="/modules/mod_slideshowck/themes/default/css/camera_ie.css" rel="stylesheet" type="text/css" />
		<![endif]-->
		<!--[if IE 8]>
		<link href="/modules/mod_slideshowck/themes/default/css/camera_ie8.css" rel="stylesheet" type="text/css" />
		<![endif]--><!-- debut Slideshow CK -->
<div class="slideshowck camera_wrap camera_burgundy_skin" id="camera_wrap_451" style="width: 400px; display: block; height: 200px; margin-bottom: 63px;"><div class="camera_fakehover"><div class="camera_src camerastarted camerasliding">
			<div data-thumb="/images/anugerah/2023/Feb23/th/feb10_th.png" data-src="/images/anugerah/2023/Feb23/feb10.png" data-link="https://iab.moe.edu.my/index.php/ms/sumber/galeri-multimedia/anugerah-pekerja-terbaik-iab#galleryd97bf53bdd-1" data-target="_blank">
														
									</div>
		<div data-thumb="/images/anugerah/2023/Jan23/th/Jan10_th.PNG" data-src="/images/anugerah/2023/Jan23/Jan10.PNG" data-link="https://iab.moe.edu.my/index.php/ms/sumber/galeri-multimedia/anugerah-pekerja-terbaik-iab#gallery25aefb479e-1" data-target="_blank">
														
									</div>
</div><div class="camera_target"><div class="cameraCont"><div class="cameraSlide cameraSlide_0" style="visibility: visible; z-index: 1; margin-left: 0px; margin-top: 0px; display: none;"><img src="/images/anugerah/2023/Feb23/feb10.png" class="imgLoaded" style="visibility: visible; height: 200px; margin-left: 0px; margin-right: 0px; margin-top: 0px; position: absolute; width: 355.556px;" data-alignment="" data-portrait="" width="1920" height="1080" alt="Februari 2023"><div class="camerarelative" style="width: 400px; height: 200px;"></div></div><div class="cameraSlide cameraSlide_1 cameracurrent cameranext" style="display: block; z-index: 999; margin-left: 0px; margin-top: 0px;"><img src="/images/anugerah/2023/Jan23/Jan10.PNG" class="imgLoaded" style="visibility: visible; height: 200px; margin-left: 0px; margin-right: 0px; margin-top: 0px; position: absolute; width: 355.556px;" data-alignment="" data-portrait="" width="1280" height="720" alt="Januari 2023"><div class="camerarelative" style="width: 400px; height: 200px;"></div></div><div class="cameraSlide cameraSlide_2 cameranext" style="z-index: 1; display: none;"><div class="camerarelative" style="width: 400px; height: 200px;"></div></div><div class="cameraappended" style="display: block; overflow: hidden; position: absolute; z-index: 1000; height: 0px; left: 0px; top: 0px; width: 0px; margin-left: 68px; margin-top: 51px; opacity: 1;"><div class="cameraSlide cameraSlide_0" style="visibility: visible; z-index: 1; margin-left: 0px; margin-top: 0px; display: block; height: 200px; width: 400px;"><img src="/images/anugerah/2023/Feb23/feb10.png" class="imgLoaded" style="visibility: visible; height: 200px; margin-left: 0px; margin-right: 0px; margin-top: 0px; position: absolute; width: 355.556px;" data-alignment="" data-portrait="" width="1920" height="1080" alt="Februari 2023"><div class="camerarelative" style="width: 400px; height: 200px;"></div></div></div><div class="cameraappended" style="display: block; overflow: hidden; position: absolute; z-index: 1000; height: 0.0526125px; left: 67px; top: 0px; width: 0.07015px; margin-left: 67.9298px; margin-top: 50.9474px; opacity: 1;"><div class="cameraSlide cameraSlide_0" style="visibility: visible; z-index: 1; margin-left: -67px; margin-top: 0px; display: block; height: 200px; width: 400px;"><img src="/images/anugerah/2023/Feb23/feb10.png" class="imgLoaded" style="visibility: visible; height: 200px; margin-left: 0px; margin-right: 0px; margin-top: 0px; position: absolute; width: 355.556px;" data-alignment="" data-portrait="" width="1920" height="1080" alt="Februari 2023"><div class="camerarelative" style="width: 400px; height: 200px;"></div></div></div><div class="cameraappended" style="display: block; overflow: hidden; position: absolute; z-index: 1000; height: 3.5054px; left: 134px; top: 0px; width: 4.67387px; margin-left: 63.3261px; margin-top: 47.4946px; opacity: 1;"><div class="cameraSlide cameraSlide_0" style="visibility: visible; z-index: 1; margin-left: -134px; margin-top: 0px; display: block; height: 200px; width: 400px;"><img src="/images/anugerah/2023/Feb23/feb10.png" class="imgLoaded" style="visibility: visible; height: 200px; margin-left: 0px; margin-right: 0px; margin-top: 0px; position: absolute; width: 355.556px;" data-alignment="" data-portrait="" width="1920" height="1080" alt="Februari 2023"><div class="camerarelative" style="width: 400px; height: 200px;"></div></div></div><div class="cameraappended" style="display: block; overflow: hidden; position: absolute; z-index: 1000; height: 33.1371px; left: 201px; top: 0px; width: 44.1828px; margin-left: 23.8172px; margin-top: 17.8629px; opacity: 1;"><div class="cameraSlide cameraSlide_0" style="visibility: visible; z-index: 1; margin-left: -201px; margin-top: 0px; display: block; height: 200px; width: 400px;"><img src="/images/anugerah/2023/Feb23/feb10.png" class="imgLoaded" style="visibility: visible; height: 200px; margin-left: 0px; margin-right: 0px; margin-top: 0px; position: absolute; width: 355.556px;" data-alignment="" data-portrait="" width="1920" height="1080" alt="Februari 2023"><div class="camerarelative" style="width: 400px; height: 200px;"></div></div></div><div class="cameraappended" style="display: block; overflow: hidden; position: absolute; z-index: 1000; height: 49.9716px; left: 268px; top: 0px; width: 65.649px; margin-left: 1.35099px; margin-top: 1.02836px; opacity: 1;"><div class="cameraSlide cameraSlide_0" style="visibility: visible; z-index: 1; margin-left: -268px; margin-top: 0px; display: block; height: 200px; width: 400px;"><img src="/images/anugerah/2023/Feb23/feb10.png" class="imgLoaded" style="visibility: visible; height: 200px; margin-left: 0px; margin-right: 0px; margin-top: 0px; position: absolute; width: 355.556px;" data-alignment="" data-portrait="" width="1920" height="1080" alt="Februari 2023"><div class="camerarelative" style="width: 400px; height: 200px;"></div></div></div><div class="cameraappended" style="display: block; overflow: hidden; position: absolute; z-index: 1000; height: 50.9938px; left: 334px; top: 0px; width: 66.9919px; margin-left: 0.0081005px; margin-top: 0.006166px; opacity: 1;"><div class="cameraSlide cameraSlide_0" style="visibility: visible; z-index: 1; margin-left: -334px; margin-top: 0px; display: block; height: 200px; width: 400px;"><img src="/images/anugerah/2023/Feb23/feb10.png" class="imgLoaded" style="visibility: visible; height: 200px; margin-left: 0px; margin-right: 0px; margin-top: 0px; position: absolute; width: 355.556px;" data-alignment="" data-portrait="" width="1920" height="1080" alt="Februari 2023"><div class="camerarelative" style="width: 400px; height: 200px;"></div></div></div><div class="cameraappended" style="display: block; overflow: hidden; position: absolute; z-index: 1000; height: 0.0526125px; left: 0px; top: 50px; width: 0.07015px; margin-left: 67.9298px; margin-top: 50.9474px; opacity: 1;"><div class="cameraSlide cameraSlide_0" style="visibility: visible; z-index: 1; margin-left: 0px; margin-top: -50px; display: block; height: 200px; width: 400px;"><img src="/images/anugerah/2023/Feb23/feb10.png" class="imgLoaded" style="visibility: visible; height: 200px; margin-left: 0px; margin-right: 0px; margin-top: 0px; position: absolute; width: 355.556px;" data-alignment="" data-portrait="" width="1920" height="1080" alt="Februari 2023"><div class="camerarelative" style="width: 400px; height: 200px;"></div></div></div><div class="cameraappended" style="display: block; overflow: hidden; position: absolute; z-index: 1000; height: 3.5054px; left: 67px; top: 50px; width: 4.67387px; margin-left: 63.3261px; margin-top: 47.4946px; opacity: 1;"><div class="cameraSlide cameraSlide_0" style="visibility: visible; z-index: 1; margin-left: -67px; margin-top: -50px; display: block; height: 200px; width: 400px;"><img src="/images/anugerah/2023/Feb23/feb10.png" class="imgLoaded" style="visibility: visible; height: 200px; margin-left: 0px; margin-right: 0px; margin-top: 0px; position: absolute; width: 355.556px;" data-alignment="" data-portrait="" width="1920" height="1080" alt="Februari 2023"><div class="camerarelative" style="width: 400px; height: 200px;"></div></div></div><div class="cameraappended" style="display: block; overflow: hidden; position: absolute; z-index: 1000; height: 33.1371px; left: 134px; top: 50px; width: 44.1828px; margin-left: 23.8172px; margin-top: 17.8629px; opacity: 1;"><div class="cameraSlide cameraSlide_0" style="visibility: visible; z-index: 1; margin-left: -134px; margin-top: -50px; display: block; height: 200px; width: 400px;"><img src="/images/anugerah/2023/Feb23/feb10.png" class="imgLoaded" style="visibility: visible; height: 200px; margin-left: 0px; margin-right: 0px; margin-top: 0px; position: absolute; width: 355.556px;" data-alignment="" data-portrait="" width="1920" height="1080" alt="Februari 2023"><div class="camerarelative" style="width: 400px; height: 200px;"></div></div></div><div class="cameraappended" style="display: block; overflow: hidden; position: absolute; z-index: 1000; height: 49.9716px; left: 201px; top: 50px; width: 66.6288px; margin-left: 1.37115px; margin-top: 1.02836px; opacity: 1;"><div class="cameraSlide cameraSlide_0" style="visibility: visible; z-index: 1; margin-left: -201px; margin-top: -50px; display: block; height: 200px; width: 400px;"><img src="/images/anugerah/2023/Feb23/feb10.png" class="imgLoaded" style="visibility: visible; height: 200px; margin-left: 0px; margin-right: 0px; margin-top: 0px; position: absolute; width: 355.556px;" data-alignment="" data-portrait="" width="1920" height="1080" alt="Februari 2023"><div class="camerarelative" style="width: 400px; height: 200px;"></div></div></div><div class="cameraappended" style="display: block; overflow: hidden; position: absolute; z-index: 1000; height: 50.9938px; left: 268px; top: 50px; width: 66.9919px; margin-left: 0.0081005px; margin-top: 0.006166px; opacity: 1;"><div class="cameraSlide cameraSlide_0" style="visibility: visible; z-index: 1; margin-left: -268px; margin-top: -50px; display: block; height: 200px; width: 400px;"><img src="/images/anugerah/2023/Feb23/feb10.png" class="imgLoaded" style="visibility: visible; height: 200px; margin-left: 0px; margin-right: 0px; margin-top: 0px; position: absolute; width: 355.556px;" data-alignment="" data-portrait="" width="1920" height="1080" alt="Februari 2023"><div class="camerarelative" style="width: 400px; height: 200px;"></div></div></div><div class="cameraappended cameraeased" style="display: block; overflow: hidden; position: absolute; z-index: 1000; height: 51px; left: 334px; top: 50px; width: 67px; margin-left: 0px; margin-top: 0px; opacity: 1;"><div class="cameraSlide cameraSlide_0" style="visibility: visible; z-index: 1; margin-left: -334px; margin-top: -50px; display: block; height: 200px; width: 400px;"><img src="/images/anugerah/2023/Feb23/feb10.png" class="imgLoaded" style="visibility: visible; height: 200px; margin-left: 0px; margin-right: 0px; margin-top: 0px; position: absolute; width: 355.556px;" data-alignment="" data-portrait="" width="1920" height="1080" alt="Februari 2023"><div class="camerarelative" style="width: 400px; height: 200px;"></div></div></div><div class="cameraappended" style="display: block; overflow: hidden; position: absolute; z-index: 1000; height: 3.5054px; left: 0px; top: 100px; width: 4.67387px; margin-left: 63.3261px; margin-top: 47.4946px; opacity: 1;"><div class="cameraSlide cameraSlide_0" style="visibility: visible; z-index: 1; margin-left: 0px; margin-top: -100px; display: block; height: 200px; width: 400px;"><img src="/images/anugerah/2023/Feb23/feb10.png" class="imgLoaded" style="visibility: visible; height: 200px; margin-left: 0px; margin-right: 0px; margin-top: 0px; position: absolute; width: 355.556px;" data-alignment="" data-portrait="" width="1920" height="1080" alt="Februari 2023"><div class="camerarelative" style="width: 400px; height: 200px;"></div></div></div><div class="cameraappended" style="display: block; overflow: hidden; position: absolute; z-index: 1000; height: 33.1371px; left: 67px; top: 100px; width: 44.1828px; margin-left: 23.8172px; margin-top: 17.8629px; opacity: 1;"><div class="cameraSlide cameraSlide_0" style="visibility: visible; z-index: 1; margin-left: -67px; margin-top: -100px; display: block; height: 200px; width: 400px;"><img src="/images/anugerah/2023/Feb23/feb10.png" class="imgLoaded" style="visibility: visible; height: 200px; margin-left: 0px; margin-right: 0px; margin-top: 0px; position: absolute; width: 355.556px;" data-alignment="" data-portrait="" width="1920" height="1080" alt="Februari 2023"><div class="camerarelative" style="width: 400px; height: 200px;"></div></div></div><div class="cameraappended" style="display: block; overflow: hidden; position: absolute; z-index: 1000; height: 49.9716px; left: 134px; top: 100px; width: 66.6288px; margin-left: 1.37115px; margin-top: 1.02836px; opacity: 1;"><div class="cameraSlide cameraSlide_0" style="visibility: visible; z-index: 1; margin-left: -134px; margin-top: -100px; display: block; height: 200px; width: 400px;"><img src="/images/anugerah/2023/Feb23/feb10.png" class="imgLoaded" style="visibility: visible; height: 200px; margin-left: 0px; margin-right: 0px; margin-top: 0px; position: absolute; width: 355.556px;" data-alignment="" data-portrait="" width="1920" height="1080" alt="Februari 2023"><div class="camerarelative" style="width: 400px; height: 200px;"></div></div></div><div class="cameraappended" style="display: block; overflow: hidden; position: absolute; z-index: 1000; height: 50.9938px; left: 201px; top: 100px; width: 67.9918px; margin-left: 0.0082214px; margin-top: 0.006166px; opacity: 1;"><div class="cameraSlide cameraSlide_0" style="visibility: visible; z-index: 1; margin-left: -201px; margin-top: -100px; display: block; height: 200px; width: 400px;"><img src="/images/anugerah/2023/Feb23/feb10.png" class="imgLoaded" style="visibility: visible; height: 200px; margin-left: 0px; margin-right: 0px; margin-top: 0px; position: absolute; width: 355.556px;" data-alignment="" data-portrait="" width="1920" height="1080" alt="Februari 2023"><div class="camerarelative" style="width: 400px; height: 200px;"></div></div></div><div class="cameraappended cameraeased" style="display: block; overflow: hidden; position: absolute; z-index: 1000; height: 51px; left: 268px; top: 100px; width: 67px; margin-left: 0px; margin-top: 0px; opacity: 1;"><div class="cameraSlide cameraSlide_0" style="visibility: visible; z-index: 1; margin-left: -268px; margin-top: -100px; display: block; height: 200px; width: 400px;"><img src="/images/anugerah/2023/Feb23/feb10.png" class="imgLoaded" style="visibility: visible; height: 200px; margin-left: 0px; margin-right: 0px; margin-top: 0px; position: absolute; width: 355.556px;" data-alignment="" data-portrait="" width="1920" height="1080" alt="Februari 2023"><div class="camerarelative" style="width: 400px; height: 200px;"></div></div></div><div class="cameraappended cameraeased" style="display: block; overflow: hidden; position: absolute; z-index: 1000; height: 51px; left: 334px; top: 100px; width: 67px; margin-left: 0px; margin-top: 0px; opacity: 1;"><div class="cameraSlide cameraSlide_0" style="visibility: visible; z-index: 1; margin-left: -334px; margin-top: -100px; display: block; height: 200px; width: 400px;"><img src="/images/anugerah/2023/Feb23/feb10.png" class="imgLoaded" style="visibility: visible; height: 200px; margin-left: 0px; margin-right: 0px; margin-top: 0px; position: absolute; width: 355.556px;" data-alignment="" data-portrait="" width="1920" height="1080" alt="Februari 2023"><div class="camerarelative" style="width: 400px; height: 200px;"></div></div></div><div class="cameraappended" style="display: block; overflow: hidden; position: absolute; z-index: 1000; height: 33.1371px; left: 0px; top: 150px; width: 44.1828px; margin-left: 23.8172px; margin-top: 17.8629px; opacity: 1;"><div class="cameraSlide cameraSlide_0" style="visibility: visible; z-index: 1; margin-left: 0px; margin-top: -150px; display: block; height: 200px; width: 400px;"><img src="/images/anugerah/2023/Feb23/feb10.png" class="imgLoaded" style="visibility: visible; height: 200px; margin-left: 0px; margin-right: 0px; margin-top: 0px; position: absolute; width: 355.556px;" data-alignment="" data-portrait="" width="1920" height="1080" alt="Februari 2023"><div class="camerarelative" style="width: 400px; height: 200px;"></div></div></div><div class="cameraappended" style="display: block; overflow: hidden; position: absolute; z-index: 1000; height: 49.9716px; left: 67px; top: 150px; width: 66.6288px; margin-left: 1.37115px; margin-top: 1.02836px; opacity: 1;"><div class="cameraSlide cameraSlide_0" style="visibility: visible; z-index: 1; margin-left: -67px; margin-top: -150px; display: block; height: 200px; width: 400px;"><img src="/images/anugerah/2023/Feb23/feb10.png" class="imgLoaded" style="visibility: visible; height: 200px; margin-left: 0px; margin-right: 0px; margin-top: 0px; position: absolute; width: 355.556px;" data-alignment="" data-portrait="" width="1920" height="1080" alt="Februari 2023"><div class="camerarelative" style="width: 400px; height: 200px;"></div></div></div><div class="cameraappended" style="display: block; overflow: hidden; position: absolute; z-index: 1000; height: 50.9938px; left: 134px; top: 150px; width: 67.9918px; margin-left: 0.0082214px; margin-top: 0.006166px; opacity: 1;"><div class="cameraSlide cameraSlide_0" style="visibility: visible; z-index: 1; margin-left: -134px; margin-top: -150px; display: block; height: 200px; width: 400px;"><img src="/images/anugerah/2023/Feb23/feb10.png" class="imgLoaded" style="visibility: visible; height: 200px; margin-left: 0px; margin-right: 0px; margin-top: 0px; position: absolute; width: 355.556px;" data-alignment="" data-portrait="" width="1920" height="1080" alt="Februari 2023"><div class="camerarelative" style="width: 400px; height: 200px;"></div></div></div><div class="cameraappended cameraeased" style="display: block; overflow: hidden; position: absolute; z-index: 1000; height: 51px; left: 201px; top: 150px; width: 68px; margin-left: 0px; margin-top: 0px; opacity: 1;"><div class="cameraSlide cameraSlide_0" style="visibility: visible; z-index: 1; margin-left: -201px; margin-top: -150px; display: block; height: 200px; width: 400px;"><img src="/images/anugerah/2023/Feb23/feb10.png" class="imgLoaded" style="visibility: visible; height: 200px; margin-left: 0px; margin-right: 0px; margin-top: 0px; position: absolute; width: 355.556px;" data-alignment="" data-portrait="" width="1920" height="1080" alt="Februari 2023"><div class="camerarelative" style="width: 400px; height: 200px;"></div></div></div><div class="cameraappended cameraeased" style="display: block; overflow: hidden; position: absolute; z-index: 1000; height: 51px; left: 268px; top: 150px; width: 67px; margin-left: 0px; margin-top: 0px; opacity: 1;"><div class="cameraSlide cameraSlide_0" style="visibility: visible; z-index: 1; margin-left: -268px; margin-top: -150px; display: block; height: 200px; width: 400px;"><img src="/images/anugerah/2023/Feb23/feb10.png" class="imgLoaded" style="visibility: visible; height: 200px; margin-left: 0px; margin-right: 0px; margin-top: 0px; position: absolute; width: 355.556px;" data-alignment="" data-portrait="" width="1920" height="1080" alt="Februari 2023"><div class="camerarelative" style="width: 400px; height: 200px;"></div></div></div><div class="cameraappended cameraeased" style="display: block; overflow: hidden; position: absolute; z-index: 1000; height: 51px; left: 334px; top: 150px; width: 67px; margin-left: 0px; margin-top: 0px; opacity: 1;"><div class="cameraSlide cameraSlide_0" style="visibility: visible; z-index: 1; margin-left: -334px; margin-top: -150px; display: block; height: 200px; width: 400px;"><img src="/images/anugerah/2023/Feb23/feb10.png" class="imgLoaded" style="visibility: visible; height: 200px; margin-left: 0px; margin-right: 0px; margin-top: 0px; position: absolute; width: 355.556px;" data-alignment="" data-portrait="" width="1920" height="1080" alt="Februari 2023"><div class="camerarelative" style="width: 400px; height: 200px;"></div></div></div></div></div><div class="camera_overlayer"></div><div class="camera_target_content"><div class="cameraContents"><div class="cameraContent" style="display: none;"><a class="camera_link" href="https://iab.moe.edu.my/index.php/ms/sumber/galeri-multimedia/anugerah-pekerja-terbaik-iab#galleryd97bf53bdd-1" target="_blank"></a><div class="camera_caption moveFromLeft" style="visibility: hidden; left: 0px; right: auto;"><div>
					<div class="camera_caption_title">
						Februari 2023											</div>
										<div class="camera_caption_desc">
																	</div>
									</div></div></div><div class="cameraContent cameracurrent" style="display: none;"><a class="camera_link" href="https://iab.moe.edu.my/index.php/ms/sumber/galeri-multimedia/anugerah-pekerja-terbaik-iab#gallery25aefb479e-1" target="_blank"></a><div class="camera_caption moveFromLeft" style="visibility: visible; left: 0px; right: auto;"><div>
					<div class="camera_caption_title">
						Januari 2023											</div>
										<div class="camera_caption_desc">
																	</div>
									</div></div></div></div></div><div class="camera_bar" style="display: none; top: auto; height: 7px;"><span class="camera_bar_cont" style="opacity: 0.8; position: absolute; inset: 0px; background-color: rgb(34, 34, 34);"><span id="pie_camera_wrap_451" style="opacity: 0; position: absolute; background-color: rgb(238, 238, 238); inset: 2px 0px; display: none;"></span></span></div><div class="camera_commands" style="opacity: 0;"><div class="camera_play" tabindex="0" aria-label="Start the slideshow" style="display: none;"></div><div class="camera_stop" tabindex="0" aria-label="Pause the slideshow" style="display: block;"></div></div><div class="camera_prev" tabindex="0" style="opacity: 0;"><span></span></div><div class="camera_next" tabindex="0" style="opacity: 0;"><span></span></div></div><div class="camera_thumbs_cont" style="visibility: visible;"></div><div class="camera_pag"><ul class="camera_pag_ul"><li class="pag_nav_0 cameracurrent" style="position:relative; z-index:1002" tabindex="0" aria-label="Show slide 1"><span><span>0</span></span><img src="/images/anugerah/2023/Feb23/th/feb10_th.png" class="camera_thumb" style="position: absolute; opacity: 0;"><div class="thumb_arrow" style="opacity: 0;"></div></li><li class="pag_nav_1" style="position:relative; z-index:1002" tabindex="0" aria-label="Show slide 2"><span><span>1</span></span><img src="/images/anugerah/2023/Jan23/th/Jan10_th.PNG" class="camera_thumb" style="position: absolute; opacity: 0;"><div class="thumb_arrow" style="opacity: 0;"></div></li></ul></div><div class="camera_loader" style="display: none;"></div></div>
<div style="clear:both;"></div>
<!-- fin Slideshow CK -->
		</div>
			<div class="moduletable">
							<h3>Kerjasama IAB</h3>
						
		<!--[if lte IE 7]>
		<link href="/modules/mod_slideshowck/themes/default/css/camera_ie.css" rel="stylesheet" type="text/css" />
		<![endif]-->
		<!--[if IE 8]>
		<link href="/modules/mod_slideshowck/themes/default/css/camera_ie8.css" rel="stylesheet" type="text/css" />
		<![endif]--><!-- debut Slideshow CK -->
<div class="slideshowck camera_wrap camera_burgundy_skin" id="camera_wrap_407" style="display: block; height: 150px; margin-bottom: 63px;"><div class="camera_fakehover"><div class="camera_src camerastarted">
			<div data-thumb="/images/iklanberkala/infografik/kerjasama/th/semeo_th.jpeg" data-src="/images/iklanberkala/infografik/kerjasama/semeo.jpeg" data-link="https://www.seameo-innotech.org/iknow/" data-target="_blank">
														
									</div>
		<div data-thumb="/images/iklanberkala/infografik/kerjasama/th/Slide1_th.png" data-src="/images/iklanberkala/infografik/kerjasama/Slide1.png" data-link="http://iab.moe.edu.my/index.php/ms/sumber/galeri-multimedia/g-info-grafik#gallery21f4aa45f7-1" data-target="_blank">
								</div>
		<div data-thumb="/images/iklanberkala/infografik/kerjasama/th/Slide2_th.png" data-src="/images/iklanberkala/infografik/kerjasama/Slide2.png" data-link="http://iab.moe.edu.my/index.php/ms/sumber/galeri-multimedia/g-info-grafik#gallery21f4aa45f7-2" data-target="_parent">
								</div>
		<div data-thumb="/images/iklanberkala/infografik/kerjasama/th/Slide3_th.png" data-src="/images/iklanberkala/infografik/kerjasama/Slide3.png" data-link="http://iab.moe.edu.my/index.php/ms/sumber/galeri-multimedia/g-info-grafik#gallery21f4aa45f7-3" data-target="_parent">
								</div>
		<div data-thumb="/images/iklanberkala/infografik/kerjasama/th/Slide4_th.png" data-src="/images/iklanberkala/infografik/kerjasama/Slide4.png" data-link="http://iab.moe.edu.my/index.php/ms/sumber/galeri-multimedia/g-info-grafik#gallery21f4aa45f7-4" data-target="_parent">
								</div>
</div><div class="camera_target"><div class="cameraCont"><div class="cameraSlide cameraSlide_0 cameracurrent" style="visibility: visible; z-index: 999; display: block;"><img src="/images/iklanberkala/infografik/kerjasama/semeo.jpeg" class="imgLoaded" style="visibility: visible; height: 150px; margin-left: 0px; margin-right: 0px; margin-top: 0px; position: absolute; width: 337.5px;" data-alignment="" data-portrait="" width="900" height="400" alt="SEAMEO INNOTECH"><div class="camerarelative" style="width: 664px; height: 150px;"></div></div><div class="cameraSlide cameraSlide_1 cameranext" style="display: none; z-index: 1;"><img src="/images/iklanberkala/infografik/kerjasama/Slide1.png" class="imgLoaded" style="visibility: visible; height: 150px; margin-left: 0px; margin-right: 0px; margin-top: 0px; position: absolute; width: 337.5px;" data-alignment="" data-portrait="" width="900" height="400"><div class="camerarelative" style="width: 664px; height: 150px;"></div></div><div class="cameraSlide cameraSlide_2" style="display: none; z-index: 1;"><img src="/images/iklanberkala/infografik/kerjasama/Slide2.png" class="imgLoaded" style="visibility: visible; height: 150px; margin-left: 0px; margin-right: 0px; margin-top: 0px; position: absolute; width: 337.5px;" data-alignment="" data-portrait="" width="900" height="400"><div class="camerarelative" style="width: 664px; height: 150px;"></div></div><div class="cameraSlide cameraSlide_3" style="display: none; z-index: 1;"><img src="/images/iklanberkala/infografik/kerjasama/Slide3.png" class="imgLoaded" style="visibility: visible; margin-left: 0px; margin-right: 0px; margin-top: 0px; position: absolute; width: 337.5px; height: 150px;" data-alignment="" data-portrait="" width="900" height="400"><div class="camerarelative" style="width: 664px; height: 150px;"></div></div><div class="cameraSlide cameraSlide_4" style="display: none; z-index: 1;"><img src="/images/iklanberkala/infografik/kerjasama/Slide4.png" class="imgLoaded" style="visibility: visible; height: 150px; margin-left: 0px; margin-right: 0px; margin-top: 0px; position: absolute; width: 337.5px;" data-alignment="" data-portrait="" width="900" height="400"><div class="camerarelative" style="width: 664px; height: 150px;"></div></div><div class="cameraSlide cameraSlide_5 cameranext" style="z-index: 1; display: none;"><div class="camerarelative" style="width: 664px; height: 150px;"></div></div></div></div><div class="camera_overlayer"></div><div class="camera_target_content"><div class="cameraContents"><div class="cameraContent cameracurrent" style="display: block;"><a class="camera_link" href="https://www.seameo-innotech.org/iknow/" target="_blank"></a><div class="camera_caption moveFromLeft" style="visibility: visible; left: 0px; right: auto;"><div>
					<div class="camera_caption_title">
						SEAMEO INNOTECH											</div>
										<div class="camera_caption_desc">
																	</div>
									</div></div></div><div class="cameraContent" style="display: none;"><a class="camera_link" href="http://iab.moe.edu.my/index.php/ms/sumber/galeri-multimedia/g-info-grafik#gallery21f4aa45f7-1" target="_blank"></a></div><div class="cameraContent" style="display: none;"><a class="camera_link" href="http://iab.moe.edu.my/index.php/ms/sumber/galeri-multimedia/g-info-grafik#gallery21f4aa45f7-2" target="_parent"></a></div><div class="cameraContent" style="display: none;"><a class="camera_link" href="http://iab.moe.edu.my/index.php/ms/sumber/galeri-multimedia/g-info-grafik#gallery21f4aa45f7-3" target="_parent"></a></div><div class="cameraContent" style="display: none;"><a class="camera_link" href="http://iab.moe.edu.my/index.php/ms/sumber/galeri-multimedia/g-info-grafik#gallery21f4aa45f7-4" target="_parent"></a></div></div></div><div class="camera_bar" style="display: none; top: auto; height: 7px;"><span class="camera_bar_cont" style="opacity: 0.8; position: absolute; inset: 0px; background-color: rgb(34, 34, 34);"><span id="pie_camera_wrap_407" style="opacity: 0.8; position: absolute; background-color: rgb(238, 238, 238); inset: 2px 0px; display: none;"></span></span></div><div class="camera_commands" style="opacity: 0;"><div class="camera_play" tabindex="0" aria-label="Start the slideshow" style="display: none;"></div><div class="camera_stop" tabindex="0" aria-label="Pause the slideshow" style="display: block;"></div></div><div class="camera_prev" tabindex="0" style="opacity: 0;"><span></span></div><div class="camera_next" tabindex="0" style="opacity: 0;"><span></span></div></div><div class="camera_thumbs_cont" style="visibility: visible;"></div><div class="camera_pag"><ul class="camera_pag_ul"><li class="pag_nav_0 cameracurrent" style="position:relative; z-index:1002" tabindex="0" aria-label="Show slide 1"><span><span>0</span></span><img src="/images/iklanberkala/infografik/kerjasama/th/semeo_th.jpeg" class="camera_thumb" style="position: absolute; opacity: 0;"><div class="thumb_arrow" style="opacity: 0;"></div></li><li class="pag_nav_1" style="position:relative; z-index:1002" tabindex="0" aria-label="Show slide 2"><span><span>1</span></span><img src="/images/iklanberkala/infografik/kerjasama/th/Slide1_th.png" class="camera_thumb" style="position: absolute; opacity: 0;"><div class="thumb_arrow" style="opacity: 0;"></div></li><li class="pag_nav_2" style="position:relative; z-index:1002" tabindex="0" aria-label="Show slide 3"><span><span>2</span></span><img src="/images/iklanberkala/infografik/kerjasama/th/Slide2_th.png" class="camera_thumb" style="position: absolute; opacity: 0;"><div class="thumb_arrow" style="opacity: 0;"></div></li><li class="pag_nav_3" style="position:relative; z-index:1002" tabindex="0" aria-label="Show slide 4"><span><span>3</span></span><img src="/images/iklanberkala/infografik/kerjasama/th/Slide3_th.png" class="camera_thumb" style="position: absolute; opacity: 0;"><div class="thumb_arrow" style="opacity: 0;"></div></li><li class="pag_nav_4" style="position:relative; z-index:1002" tabindex="0" aria-label="Show slide 5"><span><span>4</span></span><img src="/images/iklanberkala/infografik/kerjasama/th/Slide4_th.png" class="camera_thumb" style="position: absolute; opacity: 0;"><div class="thumb_arrow" style="opacity: 0;"></div></li></ul></div><div class="camera_loader" style="display: none;"></div></div>
<div style="clear:both;"></div>
<!-- fin Slideshow CK -->
		</div>
			</div>
	</div>
		<div class="col col-xs-12 col-sm-12 col-md-4 col-lg-4">
		<div class="module-wrap">
			<ul class="jj_sl_navigation jj_transition right"><li class="jj_sl_custom1"><a href="https://www.facebook.com/InstitutAminuddinBaki" target="_blank"><span class="jj_social_text">Facebook IAB</span><span class="jj_sprite_custom jj_custom1"></span></a></li><li class="jj_sl_custom2"><a href="https://twitter.com/iabkpm" target="_blank"><span class="jj_social_text">Twitter IAB</span><span class="jj_sprite_custom jj_custom2"></span></a></li><li class="jj_sl_custom3"><a href="https://www.instagram.com/iabkpm/" target="_blank"><span class="jj_social_text">Instagram IAB</span><span class="jj_sprite_custom jj_custom3"></span></a></li><li class="jj_sl_custom4"><a href="https://www.youtube.com/channel/UC5hGRQw6FbZDqkuM2eufPqg" target="_blank"><span class="jj_social_text">YouTube IAB</span><span class="jj_sprite_custom jj_custom4"></span></a></li></ul>		<div class="moduletable">
							<h3>Takwim IAB</h3>
						

<div class="custom">
	<center>
<p class="example-button clearfix" style="max-width: 100%; height: auto;"><a class="btn btn-primary" href="http://sistem.iab.edu.my/insystv2/ekiosk/takwim_iab_tahunan_view.php" target="_blank" rel="noopener"> TAHUNAN </a>&nbsp; &nbsp; &nbsp;<a class="btn btn-primary" href="http://sistem.iab.edu.my/insystv2/ekiosk/takwim_iab_semasa_view.php" target="_blank" rel="noopener">SEMASA</a></p>
</center>
<div>&nbsp;</div></div>
		</div>
			<div class="moduletable">
							<h3>Terbitan Berkala IAB</h3>
						

<div class="custom">
	<center>
<p class="example-button clearfix" style="max-width: 100%; height: auto;"><a class="btn btn-primary" href="/index.php/ms/program-latihan" target="_blank" rel="noopener"> PROGRAM LATIHAN </a>&nbsp; &nbsp; &nbsp;</p>
<p><a class="btn btn-primary" href="/index.php/ms/sumber/terbitan-berkala/laporan-tahunan" target="_blank" rel="noopener">LAPORAN TAHUNAN</a>&nbsp; &nbsp; &nbsp;&nbsp;<a class="btn btn-primary" href="/index.php/ms/jurnal-iab" target="_blank" rel="noopener" jurnal-iab="">JURNAL</a></p>
</center>
<div>&nbsp;</div></div>
		</div>
			<div class="moduletable">
							<h3>Buku Terbitan IAB</h3>
						
		<!--[if lte IE 7]>
		<link href="/modules/mod_slideshowck/themes/default/css/camera_ie.css" rel="stylesheet" type="text/css" />
		<![endif]-->
		<!--[if IE 8]>
		<link href="/modules/mod_slideshowck/themes/default/css/camera_ie8.css" rel="stylesheet" type="text/css" />
		<![endif]--><!-- debut Slideshow CK -->
<div class="slideshowck camera_wrap camera_burgundy_skin" id="camera_wrap_446" style="display: block; height: 300px; margin-bottom: 63px;"><div class="camera_fakehover"><div class="camera_src camerastarted">
			<div data-rel="lightbox" data-thumb="/images/coverbuku/th/LCML_th.png" data-src="/images/coverbuku/LCML.png" data-link="https://drive.google.com/file/d/1qsXWBns8kFdNgJmacOf66AUL26Q8RJc1/view" data-target="lightbox">
														
									</div>
		<div data-thumb="/images/coverbuku/th/budayanaratif_th.png" data-src="/images/coverbuku/budayanaratif.png" data-link="https://drive.google.com/file/d/1e9JZ0QObW7Vb01BI312BrCCU0iAVBIIB/view" data-target="_parent">
														
									</div>
		<div data-rel="lightbox" data-thumb="/images/coverbuku/th/bicara-pemimpin_th.png" data-src="/images/coverbuku/bicara-pemimpin.png" data-link="https://drive.google.com/file/d/1s_dzyI42EKJnFWsnwGxDns3_tCqVY3cs/view" data-target="lightbox">
														
									</div>
		<div data-rel="lightbox" data-thumb="/images/coverbuku/th/SN2021_th.png" data-src="/images/coverbuku/SN2021.png" data-link="https://drive.google.com/file/d/16DjM-Ggd9OCoWljCEKjYYcRSwgv9WJXS/view" data-target="lightbox">
														
									</div>
		<div data-rel="lightbox" data-thumb="/images/coverbuku/th/jendela-ilmu_th.png" data-src="/images/coverbuku/jendela-ilmu.png" data-link="https://drive.google.com/file/d/147AtxS-wvEpxk-loRT7w1HzflRJf6xig/view" data-target="lightbox">
														
									</div>
		<div data-rel="lightbox" data-thumb="/images/coverbuku/th/kompilasi-amalan-terbaik_th.png" data-src="/images/coverbuku/kompilasi-amalan-terbaik.png" data-link="https://drive.google.com/file/d/12fS3Ae3yr4vJPMLBdWlheYKh3Z5c9pCu/view" data-target="lightbox">
														
									</div>
		<div data-rel="lightbox" data-thumb="/images/coverbuku/th/protokoldanetiket_nov_th.jpg" data-src="/images/coverbuku/protokoldanetiket_nov.jpg" data-link="https://iab.moe.edu.my/bahanportal/penerbitan/2021/IAB%20Book%20Protokol%20dan%20Etiket%20Sosial%20Teks%20FA.pdf" data-target="lightbox">
														
									</div>
</div><div class="camera_target"><div class="cameraCont"><div class="cameraSlide cameraSlide_0" style="visibility: visible; z-index: 1; display: none;"><img src="/images/coverbuku/LCML.png" class="imgLoaded" style="visibility: visible; height: 300px; margin-left: 0px; margin-right: 0px; margin-top: 0px; position: absolute; width: 213.035px;" data-alignment="" data-portrait="" width="365" height="514" alt="PEMBANGUNAN  PEMIMPIN PERTENGAHAN (PANDUAN UNTUK  KURSUS KEPIMPINAN PEMIMPIN  PERTENGAHAN)  Leadership Course For Middle Leaders  (LCML)"><div class="camerarelative" style="width: 664px; height: 300px;"></div></div><div class="cameraSlide cameraSlide_1" style="display: none; z-index: 1;"><img src="/images/coverbuku/budayanaratif.png" class="imgLoaded" style="visibility: visible; height: 300px; margin-left: 0px; margin-right: 0px; margin-top: 0px; position: absolute; width: 223.541px;" data-alignment="" data-portrait="" width="383" height="514" alt="Komuniti Pembelajaran Profesional"><div class="camerarelative" style="width: 664px; height: 300px;"></div></div><div class="cameraSlide cameraSlide_2" style="display: none; z-index: 1;"><img src="/images/coverbuku/bicara-pemimpin.png" class="imgLoaded" style="visibility: visible; height: 300px; margin-left: 0px; margin-right: 0px; margin-top: 0px; position: absolute; width: 211.364px;" data-alignment="" data-portrait="" width="434" height="616" alt="Bicara Pemimpin"><div class="camerarelative" style="width: 664px; height: 300px;"></div></div><div class="cameraSlide cameraSlide_3" style="display: none; z-index: 1;"><img src="/images/coverbuku/SN2021.png" class="imgLoaded" style="visibility: visible; height: 300px; margin-left: 0px; margin-right: 0px; margin-top: 0px; position: absolute; width: 209.276px;" data-alignment="" data-portrait="" width="376" height="539" alt="Seminar Nasional 2021"><div class="camerarelative" style="width: 664px; height: 300px;"></div></div><div class="cameraSlide cameraSlide_4" style="display: none; z-index: 1; margin-left: 0px; margin-top: 0px;"><img src="/images/coverbuku/jendela-ilmu.png" class="imgLoaded" style="visibility: visible; height: 300px; margin-left: 0px; margin-right: 0px; margin-top: 0px; position: absolute; width: 207.948px;" data-alignment="" data-portrait="" width="375" height="541" alt="Jendela Ilmu"><div class="camerarelative" style="width: 664px; height: 300px;"></div></div><div class="cameraSlide cameraSlide_5" style="display: none; z-index: 1;"><img src="/images/coverbuku/kompilasi-amalan-terbaik.png" class="imgLoaded" style="visibility: visible; height: 300px; margin-left: 0px; margin-right: 0px; margin-top: 0px; position: absolute; width: 210.44px;" data-alignment="" data-portrait="" width="430" height="613" alt="Kompilasi Amalan Terbaik"><div class="camerarelative" style="width: 664px; height: 300px;"></div></div><div class="cameraSlide cameraSlide_6 cameracurrent" style="display: block; z-index: 999;"><img src="/images/coverbuku/protokoldanetiket_nov.jpg" class="imgLoaded" style="visibility: visible; height: 300px; margin-left: 0px; margin-right: 0px; margin-top: 0px; position: absolute; width: 206.356px;" data-alignment="" data-portrait="" width="487" height="708" alt="Protokol dan Etiket Sosial"><div class="camerarelative" style="width: 664px; height: 300px;"></div></div><div class="cameraSlide cameraSlide_7 cameranext" style="z-index: 1; display: none;"><div class="camerarelative" style="width: 664px; height: 300px;"></div></div></div></div><div class="camera_overlayer"></div><div class="camera_target_content"><div class="cameraContents"><div class="cameraContent" style="display: none;"><a class="camera_link" rel="lightbox" href="https://drive.google.com/file/d/1qsXWBns8kFdNgJmacOf66AUL26Q8RJc1/view" target="lightbox"></a><div class="camera_caption moveFromLeft" style="visibility: hidden; left: 0px; right: auto;"><div>
					<div class="camera_caption_title">
						PEMBANGUNAN  PEMIMPIN PERTENGAHAN (PANDUAN UNTUK  KURSUS KEPIMPINAN PEMIMPIN  PERTENGAHAN)  Leadership Course For Middle Leaders  (LCML)											</div>
										<div class="camera_caption_desc">
																	</div>
									</div></div></div><div class="cameraContent" style="display: none;"><a class="camera_link" href="https://drive.google.com/file/d/1e9JZ0QObW7Vb01BI312BrCCU0iAVBIIB/view" target="_parent"></a><div class="camera_caption moveFromLeft" style="visibility: hidden; left: 0px; right: auto;"><div>
					<div class="camera_caption_title">
						Komuniti Pembelajaran Profesional											</div>
										<div class="camera_caption_desc">
																	</div>
									</div></div></div><div class="cameraContent" style="display: none;"><a class="camera_link" rel="lightbox" href="https://drive.google.com/file/d/1s_dzyI42EKJnFWsnwGxDns3_tCqVY3cs/view" target="lightbox"></a><div class="camera_caption moveFromLeft" style="visibility: hidden; left: 0px; right: auto;"><div>
					<div class="camera_caption_title">
						Bicara Pemimpin											</div>
										<div class="camera_caption_desc">
																	</div>
									</div></div></div><div class="cameraContent" style="display: none;"><a class="camera_link" rel="lightbox" href="https://drive.google.com/file/d/16DjM-Ggd9OCoWljCEKjYYcRSwgv9WJXS/view" target="lightbox"></a><div class="camera_caption moveFromLeft" style="visibility: hidden; left: 0px; right: auto;"><div>
					<div class="camera_caption_title">
						Seminar Nasional 2021											</div>
										<div class="camera_caption_desc">
																	</div>
									</div></div></div><div class="cameraContent" style="display: none;"><a class="camera_link" rel="lightbox" href="https://drive.google.com/file/d/147AtxS-wvEpxk-loRT7w1HzflRJf6xig/view" target="lightbox"></a><div class="camera_caption moveFromLeft" style="visibility: hidden; left: 0px; right: auto;"><div>
					<div class="camera_caption_title">
						Jendela Ilmu											</div>
										<div class="camera_caption_desc">
																	</div>
									</div></div></div><div class="cameraContent" style="display: none;"><a class="camera_link" rel="lightbox" href="https://drive.google.com/file/d/12fS3Ae3yr4vJPMLBdWlheYKh3Z5c9pCu/view" target="lightbox"></a><div class="camera_caption moveFromLeft" style="visibility: hidden; left: 0px; right: auto;"><div>
					<div class="camera_caption_title">
						Kompilasi Amalan Terbaik											</div>
										<div class="camera_caption_desc">
																	</div>
									</div></div></div><div class="cameraContent cameracurrent" style="display: block;"><a class="camera_link" rel="lightbox" href="https://iab.moe.edu.my/bahanportal/penerbitan/2021/IAB%20Book%20Protokol%20dan%20Etiket%20Sosial%20Teks%20FA.pdf" target="lightbox"></a><div class="camera_caption moveFromLeft" style="visibility: visible; left: 0px; right: auto;"><div>
					<div class="camera_caption_title">
						Protokol dan Etiket Sosial											</div>
										<div class="camera_caption_desc">
																	</div>
									</div></div></div></div></div><div class="camera_bar" style="display: none; top: auto; height: 7px;"><span class="camera_bar_cont" style="opacity: 0.8; position: absolute; inset: 0px; background-color: rgb(34, 34, 34);"><span id="pie_camera_wrap_446" style="opacity: 0.8; position: absolute; background-color: rgb(238, 238, 238); inset: 2px 0px; display: none;"></span></span></div><div class="camera_commands" style="opacity: 0;"><div class="camera_play" tabindex="0" aria-label="Start the slideshow" style="display: none;"></div><div class="camera_stop" tabindex="0" aria-label="Pause the slideshow" style="display: block;"></div></div><div class="camera_prev" tabindex="0" style="opacity: 0;"><span></span></div><div class="camera_next" tabindex="0" style="opacity: 0;"><span></span></div></div><div class="camera_thumbs_cont" style="visibility: visible;"></div><div class="camera_pag"><ul class="camera_pag_ul"><li class="pag_nav_0" style="position:relative; z-index:1002" tabindex="0" aria-label="Show slide 1"><span><span>0</span></span><img src="/images/coverbuku/th/LCML_th.png" class="camera_thumb" style="position: absolute; opacity: 0;"><div class="thumb_arrow" style="opacity: 0;"></div></li><li class="pag_nav_1" style="position:relative; z-index:1002" tabindex="0" aria-label="Show slide 2"><span><span>1</span></span><img src="/images/coverbuku/th/budayanaratif_th.png" class="camera_thumb" style="position: absolute; opacity: 0;"><div class="thumb_arrow" style="opacity: 0;"></div></li><li class="pag_nav_2" style="position:relative; z-index:1002" tabindex="0" aria-label="Show slide 3"><span><span>2</span></span><img src="/images/coverbuku/th/bicara-pemimpin_th.png" class="camera_thumb" style="position: absolute; opacity: 0;"><div class="thumb_arrow" style="opacity: 0;"></div></li><li class="pag_nav_3" style="position:relative; z-index:1002" tabindex="0" aria-label="Show slide 4"><span><span>3</span></span><img src="/images/coverbuku/th/SN2021_th.png" class="camera_thumb" style="position: absolute; opacity: 0;"><div class="thumb_arrow" style="opacity: 0;"></div></li><li class="pag_nav_4" style="position:relative; z-index:1002" tabindex="0" aria-label="Show slide 5"><span><span>4</span></span><img src="/images/coverbuku/th/jendela-ilmu_th.png" class="camera_thumb" style="position: absolute; opacity: 0;"><div class="thumb_arrow" style="opacity: 0;"></div></li><li class="pag_nav_5" style="position:relative; z-index:1002" tabindex="0" aria-label="Show slide 6"><span><span>5</span></span><img src="/images/coverbuku/th/kompilasi-amalan-terbaik_th.png" class="camera_thumb" style="position: absolute; opacity: 0;"><div class="thumb_arrow" style="opacity: 0;"></div></li><li class="pag_nav_6 cameracurrent" style="position:relative; z-index:1002" tabindex="0" aria-label="Show slide 7"><span><span>6</span></span><img src="/images/coverbuku/th/protokoldanetiket_nov_th.jpg" class="camera_thumb" style="position: absolute; opacity: 0;"><div class="thumb_arrow" style="opacity: 0;"></div></li></ul></div><div class="camera_loader" style="display: none;"></div></div>
<div style="clear:both;"></div>
<!-- fin Slideshow CK -->
		</div>
			<div class="moduletable">
						

<div class="custom">
	<p style="text-align: right;"><span style="font-size: 8pt;"><strong><a href="/index.php/ms/sumber/terbitan-iab/buku-terbitan-iab">Lagi Buku Terbitan IAB &gt;&gt;</a></strong></span></p></div>
		</div>
			<div class="moduletable">
							<h3>Info Grafik</h3>
						
		<!--[if lte IE 7]>
		<link href="/modules/mod_slideshowck/themes/default/css/camera_ie.css" rel="stylesheet" type="text/css" />
		<![endif]-->
		<!--[if IE 8]>
		<link href="/modules/mod_slideshowck/themes/default/css/camera_ie8.css" rel="stylesheet" type="text/css" />
		<![endif]--><!-- debut Slideshow CK -->
<div class="slideshowck camera_wrap camera_burgundy_skin" id="camera_wrap_376" style="display: block; height: 300px; margin-bottom: 63px;"><div class="camera_fakehover"><div class="camera_src camerastarted">
			<div data-thumb="/images/iklanberkala/infografik/2023/th/ketetapanlatihan_th.png" data-src="/images/iklanberkala/infografik/2023/ketetapanlatihan.png" data-link="https://iab.moe.edu.my/index.php/ms/sumber/galeri-multimedia/g-info-grafik#gallery59cdce47ab-6" data-target="_parent">
														
									</div>
		<div data-thumb="/images/iklanberkala/infografik/2023/th/kategori-latihan_th.png" data-src="/images/iklanberkala/infografik/2023/kategori-latihan.png" data-link="https://iab.moe.edu.my/index.php/ms/sumber/galeri-multimedia/g-info-grafik#gallery59cdce47ab-5" data-target="_parent">
														
									</div>
		<div data-thumb="/images/iklanberkala/infografik/2023/th/infolatihan_th.png" data-src="/images/iklanberkala/infografik/2023/infolatihan.png" data-link="https://iab.moe.edu.my/index.php/ms/sumber/galeri-multimedia/g-info-grafik#gallery59cdce47ab-4" data-target="_parent">
														
									</div>
		<div data-thumb="/images/iklanberkala/infografik/2023/th/epsa_th.png" data-src="/images/iklanberkala/infografik/2023/epsa.png" data-link="https://iab.moe.edu.my/index.php/ms/sumber/galeri-multimedia/g-info-grafik#gallery59cdce47ab-3" data-target="_parent">
														
									</div>
		<div data-thumb="/images/iklanberkala/infografik/2023/th/logo-malaysia-madani_th.jpg" data-src="/images/iklanberkala/infografik/2023/logo-malaysia-madani.jpg" data-link="https://iab.moe.edu.my/index.php/ms/sumber/galeri-multimedia/g-info-grafik#gallery59cdce47ab-7" data-target="_parent">
														
									</div>
		<div data-thumb="/images/iklanberkala/infografik/2023/th/penilaian-hrmis-fasa-1_th.jpg" data-src="/images/iklanberkala/infografik/2023/penilaian-hrmis-fasa-1.jpg" data-link="https://iab.moe.edu.my/index.php/ms/sumber/galeri-multimedia/g-info-grafik#gallery59cdce47ab-8" data-target="_parent">
														
									</div>
</div><div class="camera_target"><div class="cameraCont"><div class="cameraSlide cameraSlide_0" style="visibility: visible; z-index: 1; display: none;"><img src="/images/iklanberkala/infografik/2023/ketetapanlatihan.png" class="imgLoaded" style="visibility: visible; height: 300px; margin-left: 0px; margin-right: 0px; margin-top: 0px; position: absolute; width: 212.071px;" data-alignment="" data-portrait="" width="1587" height="2245" alt="Ketetapan Latihan Tahun 2023"><div class="camerarelative" style="width: 664px; height: 300px;"></div></div><div class="cameraSlide cameraSlide_1" style="display: none; z-index: 1;"><img src="/images/iklanberkala/infografik/2023/kategori-latihan.png" class="imgLoaded" style="visibility: visible; height: 300px; margin-left: 0px; margin-right: 0px; margin-top: 0px; position: absolute; width: 212.071px;" data-alignment="" data-portrait="" width="1587" height="2245" alt="Kategori Latihan"><div class="camerarelative" style="width: 664px; height: 300px;"></div></div><div class="cameraSlide cameraSlide_2" style="display: none; z-index: 1;"><img src="/images/iklanberkala/infografik/2023/infolatihan.png" class="imgLoaded" style="visibility: visible; height: 300px; margin-left: 0px; margin-right: 0px; margin-top: 0px; position: absolute; width: 212.1px;" data-alignment="" data-portrait="" width="1414" height="2000" alt="Info Latihan"><div class="camerarelative" style="width: 664px; height: 300px;"></div></div><div class="cameraSlide cameraSlide_3" style="display: none; z-index: 1;"><img src="/images/iklanberkala/infografik/2023/epsa.png" class="imgLoaded" style="visibility: visible; height: 300px; margin-left: 0px; margin-right: 0px; margin-top: 0px; position: absolute; width: 212.1px;" data-alignment="" data-portrait="" width="1414" height="2000" alt="EPSA"><div class="camerarelative" style="width: 664px; height: 300px;"></div></div><div class="cameraSlide cameraSlide_4" style="display: none; z-index: 1;"><img src="/images/iklanberkala/infografik/2023/logo-malaysia-madani.jpg" class="imgLoaded" style="visibility: visible; height: 300px; margin-left: 0px; margin-right: 0px; margin-top: 0px; position: absolute; width: 225px;" data-alignment="" data-portrait="" width="756" height="1008" alt="Rasional Logo Malaysia Madani"><div class="camerarelative" style="width: 664px; height: 300px;"></div></div><div class="cameraSlide cameraSlide_5 cameracurrent" style="display: block; z-index: 999;"><img src="/images/iklanberkala/infografik/2023/penilaian-hrmis-fasa-1.jpg" class="imgLoaded" style="visibility: visible; height: 300px; margin-left: 0px; margin-right: 0px; margin-top: 0px; position: absolute; width: 453.768px;" data-alignment="" data-portrait="" width="5359" height="3543" alt="Pelaksanaan Fasa Pertama Penilaian Sasaran Kerja Utama (SKU)"><div class="camerarelative" style="width: 664px; height: 300px;"></div></div><div class="cameraSlide cameraSlide_6 cameranext" style="z-index: 1; display: none;"><div class="camerarelative" style="width: 664px; height: 300px;"></div></div></div></div><div class="camera_overlayer"></div><div class="camera_target_content"><div class="cameraContents"><div class="cameraContent" style="display: none;"><a class="camera_link" href="https://iab.moe.edu.my/index.php/ms/sumber/galeri-multimedia/g-info-grafik#gallery59cdce47ab-6" target="_parent"></a><div class="camera_caption moveFromLeft" style="visibility: hidden; left: 0px; right: auto;"><div>
					<div class="camera_caption_title">
						Ketetapan Latihan Tahun 2023											</div>
										<div class="camera_caption_desc">
																	</div>
									</div></div></div><div class="cameraContent" style="display: none;"><a class="camera_link" href="https://iab.moe.edu.my/index.php/ms/sumber/galeri-multimedia/g-info-grafik#gallery59cdce47ab-5" target="_parent"></a><div class="camera_caption moveFromLeft" style="visibility: hidden; left: 0px; right: auto;"><div>
					<div class="camera_caption_title">
						Kategori Latihan											</div>
										<div class="camera_caption_desc">
																	</div>
									</div></div></div><div class="cameraContent" style="display: none;"><a class="camera_link" href="https://iab.moe.edu.my/index.php/ms/sumber/galeri-multimedia/g-info-grafik#gallery59cdce47ab-4" target="_parent"></a><div class="camera_caption moveFromLeft" style="visibility: hidden; left: 0px; right: auto;"><div>
					<div class="camera_caption_title">
						Info Latihan											</div>
										<div class="camera_caption_desc">
																	</div>
									</div></div></div><div class="cameraContent" style="display: none;"><a class="camera_link" href="https://iab.moe.edu.my/index.php/ms/sumber/galeri-multimedia/g-info-grafik#gallery59cdce47ab-3" target="_parent"></a><div class="camera_caption moveFromLeft" style="visibility: hidden; left: 0px; right: auto;"><div>
					<div class="camera_caption_title">
						EPSA											</div>
										<div class="camera_caption_desc">
																	</div>
									</div></div></div><div class="cameraContent" style="display: none;"><a class="camera_link" href="https://iab.moe.edu.my/index.php/ms/sumber/galeri-multimedia/g-info-grafik#gallery59cdce47ab-7" target="_parent"></a><div class="camera_caption moveFromLeft" style="visibility: hidden; left: 0px; right: auto;"><div>
					<div class="camera_caption_title">
						Rasional Logo Malaysia Madani											</div>
										<div class="camera_caption_desc">
																	</div>
									</div></div></div><div class="cameraContent cameracurrent" style="display: block;"><a class="camera_link" href="https://iab.moe.edu.my/index.php/ms/sumber/galeri-multimedia/g-info-grafik#gallery59cdce47ab-8" target="_parent"></a><div class="camera_caption moveFromLeft" style="visibility: visible; left: 0px; right: auto;"><div>
					<div class="camera_caption_title">
						Pelaksanaan Fasa Pertama Penilaian Sasaran Kerja Utama (SKU)											</div>
										<div class="camera_caption_desc">
						Melalui HRMIS Tahun 2023											</div>
									</div></div></div></div></div><div class="camera_bar" style="display: none; top: auto; height: 7px;"><span class="camera_bar_cont" style="opacity: 0.8; position: absolute; inset: 0px; background-color: rgb(34, 34, 34);"><span id="pie_camera_wrap_376" style="opacity: 0.8; position: absolute; background-color: rgb(238, 238, 238); inset: 2px 0px; display: none;"></span></span></div><div class="camera_commands" style="opacity: 0;"><div class="camera_play" tabindex="0" aria-label="Start the slideshow" style="display: none;"></div><div class="camera_stop" tabindex="0" aria-label="Pause the slideshow" style="display: block;"></div></div><div class="camera_prev" tabindex="0" style="opacity: 0;"><span></span></div><div class="camera_next" tabindex="0" style="opacity: 0;"><span></span></div></div><div class="camera_thumbs_cont" style="visibility: visible;"></div><div class="camera_pag"><ul class="camera_pag_ul"><li class="pag_nav_0" style="position:relative; z-index:1002" tabindex="0" aria-label="Show slide 1"><span><span>0</span></span><img src="/images/iklanberkala/infografik/2023/th/ketetapanlatihan_th.png" class="camera_thumb" style="position: absolute; opacity: 0;"><div class="thumb_arrow" style="opacity: 0;"></div></li><li class="pag_nav_1" style="position:relative; z-index:1002" tabindex="0" aria-label="Show slide 2"><span><span>1</span></span><img src="/images/iklanberkala/infografik/2023/th/kategori-latihan_th.png" class="camera_thumb" style="position: absolute; opacity: 0;"><div class="thumb_arrow" style="opacity: 0;"></div></li><li class="pag_nav_2" style="position:relative; z-index:1002" tabindex="0" aria-label="Show slide 3"><span><span>2</span></span><img src="/images/iklanberkala/infografik/2023/th/infolatihan_th.png" class="camera_thumb" style="position: absolute; opacity: 0;"><div class="thumb_arrow" style="opacity: 0;"></div></li><li class="pag_nav_3" style="position:relative; z-index:1002" tabindex="0" aria-label="Show slide 4"><span><span>3</span></span><img src="/images/iklanberkala/infografik/2023/th/epsa_th.png" class="camera_thumb" style="position: absolute; opacity: 0;"><div class="thumb_arrow" style="opacity: 0;"></div></li><li class="pag_nav_4" style="position:relative; z-index:1002" tabindex="0" aria-label="Show slide 5"><span><span>4</span></span><img src="/images/iklanberkala/infografik/2023/th/logo-malaysia-madani_th.jpg" class="camera_thumb" style="position: absolute; opacity: 0;"><div class="thumb_arrow" style="opacity: 0;"></div></li><li class="pag_nav_5 cameracurrent" style="position:relative; z-index:1002" tabindex="0" aria-label="Show slide 6"><span><span>5</span></span><img src="/images/iklanberkala/infografik/2023/th/penilaian-hrmis-fasa-1_th.jpg" class="camera_thumb" style="position: absolute; opacity: 0;"><div class="thumb_arrow" style="opacity: 0;"></div></li></ul></div><div class="camera_loader" style="display: none;"></div></div>
<div style="clear:both;"></div>
<!-- fin Slideshow CK -->
		</div>
			<div class="moduletable">
						

<div class="custom">
	<p style="text-align: right;"><span style="font-size: 8pt;"><strong><a href="/index.php/ms/sumber/galeri-multimedia/g-info-grafik">Lagi Info Grafik &gt;&gt;</a></strong></span></p></div>
		</div>
			<div class="moduletable">
							<h3>Pencapaian dan Pensijilan</h3>
						

<div class="custom">
	<div style="text-align: left;"><img style="max-width: 30%; height: auto;" src="/images/iab/QMSDSM_Color-01.png" alt="QMSDSM Color 01" height="auto"><img style="max-width: 30%; height: auto;" src="/images/iab/QMSIQNET_Color-01.png" alt="QMSIQNET Color 01" height="auto"><img style="max-width: 30%; height: auto;" src="/images/iab/QMSUKAS_Color-01.png" alt="QMSUKAS Color 01" height="auto"></div>
<p><span style="font-size: 8pt;"><strong><a href="/index.php/ms/18-about-us/288-a-pencapaian-iab"></a></strong></span></p></div>
		</div>
			</div>
	</div>
		</div>
	</div></div>		</div>
	
	</div>
	<!-- //SECTIONS -->


  

  
	<!-- JA MAP -->
	<div class="wrap t3-ja-map ">
			

<div class="custom">
	<p style="text-align: center;"><a href="https://www.malaysia.gov.my/public/cms/" target="_blank" rel="noopener"><img src="/images/pautan/jata.png" alt="jata" width="40" height="31"></a>&nbsp;&nbsp;<a href="http://www.mscmalaysia.my/" target="_blank" rel="noopener"><img src="/images/pautan/msc.png" alt="msc" width="43" height="36"></a>&nbsp;&nbsp;<a title="Jabatan Perkhidmatan Awam" href="https://www.jpa.gov.my/" target="_blank" rel="noopener"><img src="/images/pautan/jpa.png" alt="jpa" width="29" height="36"></a>&nbsp;&nbsp;<a href="http://moe.gov.my" target="_blank" rel="noopener"><img src="/images/pautan/Logo_Jata_KPM_01_JPG.jpg" alt="Logo Jata KPM 01 JPG" width="90" height="39"></a>&nbsp;<a title="Lembaga Hasil Dalam Negeri" href="http://www.hasil.gov.my/" target="_blank" rel="noopener"><img src="/images/pautan/lhdn.png" alt="LHDN" width="46" height="32"></a><span style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 8.25px; font-style: normal; font-weight: bold; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; background-color: #ffffff; float: none;">&nbsp; &nbsp;</span><a style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 8.25px; font-style: normal; font-weight: bold; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; background-color: #ffffff;" title="MAMPU" href="http://www.mampu.gov.my" target="_blank" rel="noopener"><img src="/images/pautan/mampu.png" alt="mampu" width="67" height="16"></a><span style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 8.25px; font-style: normal; font-weight: bold; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; background-color: #ffffff; float: none;">&nbsp; <a title="MyGovUC" href="http://www.1govuc.gov.my/" target="_blank" rel="noopener"><img src="/images/pautanpantas/MY_GOV_UC_small.png" alt="MY GOV UC small" width="62" height="19"> &nbsp;</a></span><span style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 8.25px; font-style: normal; font-weight: bold; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; background-color: #ffffff; float: none;"></span><a style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 8.25px; font-style: normal; font-weight: bold; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; background-color: #ffffff;" title="Kementerian Kesihatan Malaysia" href="http://www.moh.gov.my/" target="_blank" rel="noopener"><img src="/images/pautan/kemkes.png" alt="kemkes" width="34" height="33"></a><span style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 8.25px; font-style: normal; font-weight: bold; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; background-color: #ffffff; float: none;"></span><a style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 8.25px; font-style: normal; font-weight: bold; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; background-color: #ffffff;" title="Kementerian Kewangan Malaysia" href="http://www.treasury.gov.my/" target="_blank" rel="noopener"><img src="/images/pautan/MOF.png" alt="" width="44" height="24"></a><span style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 8.25px; font-style: normal; font-weight: bold; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; background-color: #ffffff; float: none;">&nbsp;&nbsp;&nbsp;</span><a style="font-family: Verdana, Arial, Helvetica, sans-serif; font-style: normal; font-weight: bold; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; background-color: #ffffff; font-size: 0.75em;" title="Puspanita KPM" href="https://puspanita.moe.gov.my/" target="_blank" rel="noopener"><img src="/images/pautan/pus.png" alt="" width="31" height="31"></a><span style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-style: normal; font-weight: bold; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; background-color: #ffffff; font-size: 0.75em;"></span><a style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 8.25px; font-style: normal; font-weight: bold; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; background-color: #ffffff;" title="Perpustakaan Negara Malaysia" href="http://www.pnm.gov.my/" target="_blank" rel="noopener"><img src="/images/pautan/PNM.png" alt="PNM" width="26" height="28"></a><span style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-style: normal; font-weight: bold; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; background-color: #ffffff; font-size: 0.75em;"></span><span style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 8.25px; font-style: normal; font-weight: bold; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; background-color: #ffffff; float: none;">&nbsp;&nbsp;</span><a style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 8.25px; font-style: normal; font-weight: bold; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; background-color: #ffffff;" title="EDUWEBTV" href="http://www.eduwebtv.com" target="_blank" rel="noopener"><img src="/images/pautan/eduweb.png" alt="" width="69" height="19"></a><span style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 8.25px; font-style: normal; font-weight: bold; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; background-color: #ffffff; float: none;">&nbsp;&nbsp;</span><span style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-style: normal; font-weight: bold; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; background-color: #ffffff; font-size: 0.75em;">&nbsp;<a title="SPL KPM" href="https://splkpm.moe.gov.my/mn_pengumuman/index.cfm" target="_blank" rel="noopener"><img src="/images/pautan/splkpm.PNG" alt="" width="66" height="28"></a>&nbsp;&nbsp;</span><a style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 0.75em;" title="HRMIS" href="http://www.eghrmis.gov.my/" target="_blank" rel="noopener"><img src="/images/pautan/hrmis.png" alt="hrmis" width="43" height="43"></a><span style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 0.75em;"></span><a style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 0.75em;" title="Penyata Gaji" href="https://epenyatagaji-laporan.anm.gov.my/Layouts/Login/Login.aspx" target="_blank" rel="noopener"><img src="/images/pautan/epenyata.jpg" alt="epenyata" width="83" height="31"></a><span style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 0.75em;">&nbsp;&nbsp;</span><a style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 0.75em;" title="MyEG" href="https://www.myeg.com.my/" target="_blank" rel="noopener"><img src="/images/pautan/myeg.png" alt="myeg" width="61" height="29"></a><span style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 0.75em;">&nbsp;&nbsp;</span><a style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 0.75em;" title="e-Perolehan" href="http://home.eperolehan.gov.my/v2/index.php?lang=bm" target="_blank" rel="noopener"><img src="/images/pautan/ePlogo.png" alt="ePlogo" width="105" height="29"></a><span style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 0.75em;">&nbsp;&nbsp;</span><a style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 0.75em;" title="e-Maklum" href="http://emaklumweb.anm.gov.my/action/home" target="_blank" rel="noopener"><img src="/images/pautan/emaklum.jpg" alt="emaklum" width="85" height="26"></a><span style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 0.75em;">&nbsp;&nbsp;</span><a style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 0.75em;" title="Sumber Elektronik Perputakaan Negara Malaysia" href="http://www.pnmdigital.gov.my/" target="_blank" rel="noopener"><img src="/images/pautan/PNM.png" alt="" width="31" height="32"></a><span style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 0.75em;">&nbsp;&nbsp;</span><a style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 0.75em;" title="Kursus Online INTAN" href="http://www.online-itims.intan.my/itims/" target="_blank" rel="noopener"><img src="/images/pautan/intan.png" alt="intan" width="29" height="42"></a><span style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 0.75em;">&nbsp;&nbsp;</span><a style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 0.75em;" title="Jabatan Akauntan Negara Malaysia" href="http://www.anm.gov.my/index.php" target="_blank" rel="noopener"><img src="/images/pautan/logoanm.png" alt="logoanm" width="42" height="42"></a>&nbsp;&nbsp;<a title="Sistem Guru Malaysia" href="https://sgmy.moe.gov.my/INDEX.CFM" target="_blank" rel="noopener"><img src="/images/pautanpantas/sgm2.JPG" alt="sgm2" width="75" height="29"></a><span style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 0.75em;"></span></p></div>

	</div>
	<!-- // JA MAP -->


  

  
  
<!-- BACK TOP TOP BUTTON -->
<div id="back-to-top" data-spy="affix" data-offset-top="200" class="back-to-top hidden-xs hidden-sm affix-top">
  <button class="btn btn-primary" title="Back to Top"><i class="fa fa-long-arrow-up" aria-hidden="true"></i></button>
</div>

<script type="text/javascript">
(function($) {
  // Back to top
  $('#back-to-top').on('click', function(){
    $("html, body").animate({scrollTop: 0}, 500);
    return false;
  });
})(jQuery);
</script>
<!-- BACK TO TOP BUTTON -->

<!-- FOOTER -->
<footer id="t3-footer" class="wrap t3-footer">
	
			<!-- FOOT NAVIGATION -->
		<div class="container footnav-1">
				<!-- SPOTLIGHT -->
	<div class="t3-spotlight t3-footnav-1  row">
					<div class=" col-lg-3 col-md-3 col-sm-3 col-xs-6">
								<div class="t3-module module " id="Mod220"><div class="module-inner"><h3 class="module-title "><span>IAB Induk</span></h3><div class="module-ct">

<div class="custom">
	<p>Institut Aminuddin Baki</p>
<p>Kementerian Pendidikan Malaysia</p>
<p>Kompleks Pendidikan Nilai</p>
<p>71760 Bandar Enstek</p>
<p>Negeri Sembilan</p>
<p>No.Tel:+606-7979200</p>
<p>E-mel: iab@iab.moe.gov.my</p>
<p><a title="Facebook IAB" href="https://www.facebook.com/InstitutAminuddinBaki" target="_blank" rel="noopener"><img style="max-width: 20%; height: auto;" src="/images/iab/if_facebook_386622.png" alt="if facebook 386622" width="16" height="16"></a>&nbsp; &nbsp;<a title="Twitter IAB" href="https://twitter.com/iabkpm" target="_blank" rel="noopener"><img style="max-width: 20%; height: auto;" src="/images/iab/if_twitter_386736.png" alt="if twitter 386736" width="16" height="16"></a>&nbsp; &nbsp;<a title="Instagram IAB" href="https://www.instagram.com/iabkpm/" target="_blank" rel="noopener"><img style="max-width: 20%; height: auto;" src="/images/ikon/Logo-Instagram.png" alt="Logo Instagram" width="16" height="16"></a>&nbsp;&nbsp;<a title="YouTube IAB" href="https://www.youtube.com/channel/UC5hGRQw6FbZDqkuM2eufPqg" target="_self"><img style="max-width: 20%; height: auto;" src="/images/ikon/icons8-youtube-squared-16.png" alt="icons8 youtube squared 16" width="20" height="20"></a>&nbsp;&nbsp;</p></div>
</div></div></div>
							</div>
					<div class=" col-lg-3 col-md-3 col-sm-3 col-xs-6">
								<div class="t3-module module " id="Mod222"><div class="module-inner"><h3 class="module-title "><span>Notis</span></h3><div class="module-ct"><ul class="nav nav-pills nav-stacked menu">
<li class="item-418"><a href="/index.php/ms/hak-cipta" class="">Hak Cipta</a></li><li class="item-419"><a href="/index.php/ms/penafian" class="">Penafian</a></li><li class="item-420"><a href="/index.php/ms/dasar-keselamatan" class="">Dasar Keselamatan</a></li><li class="item-422"><a href="/index.php/ms/dasar-privasi" class="">Dasar Privasi</a></li><li class="item-526"><a href="/index.php/ms/pautan" class="">Pautan</a></li><li class="item-423"><a href="/index.php/ms/soalan-lazim" class="">Soalan Lazim</a></li><li class="item-424"><a href="/index.php/ms/peta-laman" class="">Peta Laman</a></li></ul>
</div></div></div>
							</div>
					<div class=" col-lg-3 col-md-3 col-sm-3 col-xs-6">
								<div class="t3-module module " id="Mod286"><div class="module-inner"><h3 class="module-title "><span>Artikel Popular</span></h3><div class="module-ct"><ul class="mostread">
	<li itemscope="" itemtype="https://schema.org/Article">
		<a href="/index.php/ms/2-uncategorised/620-cetakan-sijil-dlm-talian" itemprop="url">
			<span itemprop="name">
				Cetakan Sijil Dalam Talian			</span>
		</a>
	</li>
	<li itemscope="" itemtype="https://schema.org/Article">
		<a href="/index.php/ms/ttgiab/iab-cawangan/iab-cawangan-genting-highlands" itemprop="url">
			<span itemprop="name">
				IAB Cawangan Genting Highlands			</span>
		</a>
	</li>
	<li itemscope="" itemtype="https://schema.org/Article">
		<a href="/index.php/ms/ttgiab/iab-cawangan/iab-cawangan-utara" itemprop="url">
			<span itemprop="name">
				IAB Cawangan Utara			</span>
		</a>
	</li>
	<li itemscope="" itemtype="https://schema.org/Article">
		<a href="/index.php/ms/pages/menu-ap-web/sp-q-iab" itemprop="url">
			<span itemprop="name">
				Laman Sistem Pengurusan Kualiti (SP-Q)			</span>
		</a>
	</li>
	<li itemscope="" itemtype="https://schema.org/Article">
		<a href="/index.php/ms/sumber/menu-berita-pemberitahuan/9-pengumuman-2018/387-semakan-panggilan-pentaksiran-bagi-program-kelayakan-profesional-pemimpin-pendidikan-kebangsaan-npqel-ambilan-tahun-2019" itemprop="url">
			<span itemprop="name">
				Semakan Panggilan Pentaksiran bagi Program Kelayakan Profesional Pemimpin Pendidikan Kebangsaan (NPQEL) Ambilan Tahun 2019			</span>
		</a>
	</li>
	<li itemscope="" itemtype="https://schema.org/Article">
		<a href="/index.php/ms/ttgiab/carta-organisasi" itemprop="url">
			<span itemprop="name">
				Carta Organisasi IAB			</span>
		</a>
	</li>
</ul>
</div></div></div>
							</div>
					<div class=" col-lg-3 col-md-3 col-sm-3 col-xs-6">
								<div class="t3-module module " id="Mod223"><div class="module-inner"><h3 class="module-title "><span>Informasi Media KPM</span></h3><div class="module-ct"><ul class="nav nav-pills nav-stacked menu">
<li class="item-428"><a href="http://moe.spab.gov.my/eApps/system/index.do" class="" target="_blank" rel="noopener noreferrer">Sistem Pengurusan Aduan Awam (SISPAA) </a></li><li class="item-1258"><a href="https://isd.moe.gov.my" class="" target="_blank" rel="noopener noreferrer">Sistem Meja Bantuan Perkhidmatan ICT </a></li><li class="item-425"><a href="/bahanportal/sumber/Preliminary-Blueprint-BM.pdf" class="" target="_blank" rel="noopener noreferrer">Pelan Pembangunan Pendidikan Malaysia  </a></li><li class="item-642"><a href="https://www.moe.gov.my/korporat/cio" class="" target="_blank" rel="noopener noreferrer">Ketua Pegawai Maklumat (CIO)   </a></li><li class="item-644"><a href="https://www.moe.gov.my/" class="" target="_blank" rel="noopener noreferrer">Portal KPM </a></li><li class="item-704"><a href="https://www.facebook.com/KemPendidikan/" class="" target="_blank" rel="noopener noreferrer">Facebook KPM </a></li><li class="item-1097"><a href="https://www.instagram.com/kempendidikan/" class="" target="_blank" rel="noopener noreferrer">Instagram KPM </a></li><li class="item-705"><a href="https://twitter.com/kempendidikan" class="" target="_blank" rel="noopener noreferrer">Twitter KPM </a></li><li class="item-645"><a href="https://www.youtube.com/results?search_query=kementerian+pendidikan+malaysia" class="" target="_blank" rel="noopener noreferrer">YouTube KPM </a></li></ul>
</div></div></div>
							</div>
			</div>
<!-- SPOTLIGHT -->
		</div>
		<!-- //FOOT NAVIGATION -->
	
	
	<section class="t3-copyright">
		<div class="container">
			<div class="row">
				<div class="col-md-12 copyright ">
					<div class="t3-module module " id="Mod228"><div class="module-inner"><div class="module-ct">

<div class="custom">
	<p></p>
<table style="width: 10%; float: right;">
<tbody>
<tr>
<td>
<p><img src="/images/iab/static_qr_code_iab.png" alt="static qr code iab"></p>
</td>
</tr>
</tbody>
</table>
<p><br>Hak Cipta Terpelihara © 2010 - 2023 INSTITUT AMINUDDIN BAKI, KEMENTERIAN PENDIDIKAN MALAYSIA</p>
<p style="text-align: left;">PENAFIAN:&nbsp;Kerajaan Malaysia dan Institut Aminuddin Baki adalah tidak bertanggungjawab bagi sebarang kehilangan atau kerugian yang diakibatkan oleh penggunaan mana-mana maklumat yang diperolehi daripada laman web ini.&nbsp;Sesuai dipapar menggunakan&nbsp;pelayar popular Internet Explorer 9+, Mozilla Firefox,&nbsp;Google Chrome,&nbsp; <br>Opera versi terkini dengan resolusi 1280 x 800 dan ke atas.</p></div>
</div></div></div><div class="t3-module module " id="Mod454"><div class="module-inner"><div class="module-ct"><div class="mvc_main"><div style="text-align: center;" class="mvc_people"><table align="center" cellpadding="0" cellspacing="0" style="width: 30%;" class="mvc_peopleTable"><tbody><tr align="left" title="09-28-2023"><td><img class="mvc_peopleImg" src="/modules/mod_vvisit_counter/images/tbl/peoples/vtoday.gif" alt="Bil. Pelawat Hari Ini" title="Bil. Pelawat Hari Ini"></td><td>Bil. Pelawat Hari Ini</td><td align="right">90</td></tr><tr align="left" title="09-25-2023 - 10-01-2023"><td><img class="mvc_peopleImg" src="/modules/mod_vvisit_counter/images/tbl/peoples/vweek.gif" alt="Bil. Pelawat Minggu Ini" title="Bil. Pelawat Minggu Ini"></td><td>Bil. Pelawat Minggu Ini</td><td align="right">373</td></tr><tr align="left" title="09-01-2023 - 09-30-2023"><td><img class="mvc_peopleImg" src="/modules/mod_vvisit_counter/images/tbl/peoples/vmonth.gif" alt="Bil. Pelawat Bulan Ini" title="Bil. Pelawat Bulan Ini"></td><td>Bil. Pelawat Bulan Ini</td><td align="right">2640</td></tr><tr align="left" title=""><td><img class="mvc_peopleImg" src="/modules/mod_vvisit_counter/images/tbl/peoples/vall.gif" alt="Jumlah Pelawat Keseluruhan" title="Jumlah Pelawat Keseluruhan"></td><td>Jumlah Pelawat Keseluruhan</td><td align="right">414858</td></tr></tbody></table></div></div><!-- Mod_VVisit_Counter :  http://www.mmajunke.de/ --></div></div></div><div class="t3-module module " id="Mod309"><div class="module-inner"><div class="module-ct"><!-- TWO STEPS TO INSTALL LOADING TIME INDICATOR:

  1.  Copy the coding into the HEAD of your HTML document
  2.  Add the last code into the BODY of your HTML document  -->

<!-- STEP ONE: Paste this code into the HEAD of your HTML document  -->



<script type="text/javascript">
<!-- Begin
/* This script and many more are available free online at
The JavaScript Source!! http://javascript.internet.com
Created by: Abraham Joffe :: http://www.abrahamjoffe.com.au/ */

var startTime=new Date();

function currentTime(){
  var a=Math.floor((new Date()-startTime)/100)/10;
  if (a%1==0) a+=".0";
  document.getElementById("endTime").innerHTML=a;
}

window.onload=function(){
  clearTimeout(loopTime);
}

// End -->
</script>


<!-- STEP TWO: Copy this code into the BODY of your HTML document  -->



<script type="text/javascript">
<!-- Begin
  document.write('Masa Capaian : <span id="endTime">0.0</span> saat');
  var loopTime=setInterval("currentTime()",100);
// End -->
</script>Masa Capaian : <span id="endTime">40.0</span> saat
<p>&nbsp;</p>


</div></div></div>
				</div>
							</div>
		</div>
	</section>

</footer>
<!-- //FOOTER -->

</div>



<div id="cboxOverlay" style="display: none;"></div><div id="colorbox" class="" role="dialog" tabindex="-1" style="display: none;"><div id="cboxWrapper"><div><div id="cboxTopLeft" style="float: left;"></div><div id="cboxTopCenter" style="float: left;"></div><div id="cboxTopRight" style="float: left;"></div></div><div style="clear: left;"><div id="cboxMiddleLeft" style="float: left;"></div><div id="cboxContent" style="float: left;"><div id="cboxTitle" style="float: left;"></div><div id="cboxCurrent" style="float: left;"></div><button type="button" id="cboxPrevious" aria-label="Previous"></button><button type="button" id="cboxNext" aria-label="Next"></button><button type="button" id="cboxSlideshow" aria-label="Slideshow"></button><div id="cboxLoadingOverlay" style="float: left;"></div><div id="cboxLoadingGraphic" style="float: left;"></div></div><div id="cboxMiddleRight" style="float: left;"></div></div><div style="clear: left;"><div id="cboxBottomLeft" style="float: left;"></div><div id="cboxBottomCenter" style="float: left;"></div><div id="cboxBottomRight" style="float: left;"></div></div></div><div style="position: absolute; width: 9999px; visibility: hidden; display: none; max-width: none;"></div></div><div id="sbox-overlay" aria-hidden="true" tabindex="-1" style="z-index: 65555; opacity: 0;"></div><div id="sbox-window" role="dialog" aria-hidden="true" class="shadow" style="z-index: 65557;"><div id="sbox-content" style="opacity: 0;"></div><a id="sbox-btn-close" href="#" role="button" aria-controls="sbox-window"></a></div><div tabindex="-1" role="dialog" class="ui-dialog ui-corner-all ui-widget ui-widget-content ui-front" aria-describedby="popup" aria-labelledby="ui-id-1" style="display: none;"><div class="ui-dialog-titlebar ui-corner-all ui-widget-header ui-helper-clearfix"><span id="ui-id-1" class="ui-dialog-title">Pop Up Title</span><button type="button" class="ui-button ui-corner-all ui-widget ui-button-icon-only ui-dialog-titlebar-close" title="Close"><span class="ui-button-icon ui-icon ui-icon-closethick"></span><span class="ui-button-icon-space"> </span>Close</button></div><div id="popup" class="ui-dialog-content ui-widget-content">
	<div class="header popupjt"><button type="button" class="ui-button ui-corner-all ui-widget ui-button-icon-only ui-dialog-titlebar-close popupjt" title="Close"><span class="ui-button-icon ui-icon ui-icon-closethick popupjt"></span><span class="ui-button-icon-space"> </span>Close</button></div>		<div class="content" style="padding:15px 15px 15px 15px;"> 
		<div id="modpos">
							<div style="clear:both; margin:10px 0;" class="sidescript_block first_block">
															</div>
							</div>
</div>
	</div></div>
@endsection