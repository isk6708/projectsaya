<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama Seminar</th>
      <th scope="col">Kategori Seminar</th>
      <th scope="col">Tarikh Dicipta</th>
      <th scope="col">Tarikh Kemaskini</th>
    </tr>
  </thead>
  <tbody>
        @php 
        $no = 1;
        @endphp
       @foreach($mseminar as $item)
       <tr>
        <td>{{$no++}}</td>
        <td>{{$item['seminar_name']}}</td>
        <td>{{$item['seminar_cat']}}</td>
        <td>{{date('d-m-Y H:i',strtotime($item['created_at']))}}</td>
        <td>{{date('d-m-Y H:i',strtotime($item['updated_at']))}}</td>
       </tr>
       @endforeach
  </tbody>
</table>