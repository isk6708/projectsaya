@extends('layouts.iab')
@section('title', 'Borang Seminar')
@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div>
    @if(isset($mseminar) && !empty($mseminar->id))
        <form action="{{route('seminar.kemaskini',$mseminar->id)}}" method="POST" enctype="multipart/form-data">
        @method('PUT')
    @else
        <form action="{{route('seminar.simpan')}}" method="POST" enctype="multipart/form-data">
    @endif
    @csrf
        <div class="mb-3">
            <label class="form-label wajib">Nama Seminar</label>
            <input type="text" class="form-control" name="seminar_name" value="{{($mseminar->seminar_name??'')}}">
        </div>
        <div class="mb-3">
            <label class="form-label">Lampiran</label>
            <input multiple type="file" class="form-control" name="seminar_attachment[]">
        </div>

        <div class="mb-3">
            <label class="form-label">Tarikh Mula</label>
            <input type="date" class="form-control" 
            name="start_date" value="{{($mseminar->details['start_date']??'')}}">
        </div>
        <div class="mb-3">
            <label class="form-label">Tarikh Tamat</label>
            <input type="date" class="form-control" 
            name="end_date" value="{{($mseminar->details['end_date']??'')}}">
        </div>
        <div class="mb-3">
            <label class="form-label wajib">Kategori Seminar</label>
            <select class="form-select" name="seminar_cat">
                <option value="">-- Sila Pilih --</option>
                @foreach($category as $key=>$value)
                <option value="{{$key}}" 
                {{($mseminar->seminar_cat == $key ? ' selected ':'' )}}>{{$value}}</option>
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Simpan</button>
        <a href="{{route('seminar.daftar')}}" class="btn btn-warning">Set Semula</a>
        <a href="{{route('seminar.cari')}}" class="btn btn-danger">Kembali</a>
    </form>
</div>
@endsection
