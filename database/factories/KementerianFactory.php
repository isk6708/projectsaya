<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Kementerian>
 */
class KementerianFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return[
            'kodkementerian'=>$this->faker->numberBetween(0,1000),
            'acroynm'=>$this->faker->text(maxNbChars:50),
            'namakementerian'=>$this->faker->numberBetween(0,1000),
            'editedby'=>$this->faker->text(maxNbChars:50),
        ];
    }
}
