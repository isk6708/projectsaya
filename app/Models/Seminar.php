<?php

namespace App\Models;


use App\Models\User;
use App\Models\Reference;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Seminar extends Model
{
    use HasFactory;

    protected $casts = [
        'details' => 'json',
    ];
    
    public function kategori()
    {
        return $this->hasOne(Reference::class,'code','seminar_cat')
        ->where('cat','SEMINARCAT');
    }

    public function subCategory()
    {
        return $this->hasOne(Reference::class,
        // DB::Raw("JSON_UNQUOTE(JSON_EXTRACT(details,'$.sub_cat'))"),
        'code','sub_cat')
        ->where('cat','SEMINARSUBCAT');
    }

    public function creator()
    {
        return $this->hasOne(User::class,'created_by','id');
    }

    public function editor()
    {
        return $this->hasOne(User::class,'updated_by','id');
    }


}
