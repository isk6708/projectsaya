<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('kampuses', function (Blueprint $table) {
            $table->id();
            $table->string('IDKampus');
            $table->string('IDKampusOld');
            $table->integer('susunan');
            $table->string('IDOrganisasi');
            $table->string('KodTakwimKampus');
            $table->string('acronymKampus');
            $table->string('kampus');
            $table->integer('hadAsramaPenginapan');
            $table->integer('hadBilikKuliah');
            $table->string('alamat');
            $table->string('poskod');
            $table->string('bandar');
            $table->string('daerah');
            $table->string('negeri');
            $table->string('noTelefon');
            $table->string('status');
            $table->string('editedby');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('kampuses');
    }
};