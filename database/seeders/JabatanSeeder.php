<?php

namespace Database\Seeders;

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class JabatanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        DB::table('jabatans')->insert([
            'kodjabatan'=>Str::random(10),
            'namajabatan'=>Str::random(30),
            'kodkementerian'=>Str::random(10),
            // 'editedby'=>Str::random(30)
        ]);
    }
}
